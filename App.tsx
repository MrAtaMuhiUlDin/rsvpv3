/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Text, View} from 'react-native';
import AddPictureSVG from './src/assets/icons/addPictureSVG';
import AbstractFooterButtonContainer from './src/components/abstract/abstractFooterButtonContainer';
import Navigator from './src/navigation/authNavigator';
import BottomTabNavigator from './src/navigation/providerNavigator';

const App = () => {
  // return (
  //   <View style={{flex: 1}}>
  //     <AbstractFooterButtonContainer>
  //       <AddPictureSVG />
  //     </AbstractFooterButtonContainer>
  //   </View>
  // );
  return <Navigator />;
};
export default App;
