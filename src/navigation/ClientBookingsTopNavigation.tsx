import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { moderateScale } from 'react-native-size-matters';
import ClientsBookingsScreen from '../screens/Client/Client Scheduled Screens/ClientsBookingsScreen';
import ClientsEventsScreen from '../screens/Client/Client Scheduled Screens/ClientsEventsScreen';
import TopBarCustom2Tabs from './CustomTopTabBars/TopBarCustom2TabsBooking';
import TopBarCustom2TabsBooking from './CustomTopTabBars/TopBarCustom2TabsBooking';


const Tab = createMaterialTopTabNavigator();



const ClientBookingsTopNavigation = () => {
    return (
        <Tab.Navigator
        tabBar={(props) => <TopBarCustom2TabsBooking {...props} />}
        screenOptions={{
            // tabBarLabelStyle: { fontSize: moderateScale(14,0.1),color:'#FF5959', fontWeight:'400',fontFamily:'Nunito' },
            // // tabBarItemStyle: { width: 100 },
            // tabBarStyle: { backgroundColor: 'white' },
            tabBarScrollEnabled:false

          }}
        >
        <Tab.Screen name="Events" component={ClientsEventsScreen} />
        <Tab.Screen name="Bookings" component={ClientsBookingsScreen} />
        
      </Tab.Navigator>
    )
}


export default ClientBookingsTopNavigation