import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MessagesScreen from '../screens/Client/Client Notifications Screens/MessagesScreen'
import NotificationsScreen from '../screens/Client/Client Notifications Screens/NotificationScreen';
import { moderateScale } from 'react-native-size-matters';
import TopBarCustom2TabsNotifi from './CustomTopTabBars/TopBarCustom2TabsNotifi';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ClientChatScreen from '../screens/Client/ClientChatScreen/ClientChatScreen';


const Tab = createMaterialTopTabNavigator();



const ClientNotificatonTopNavigationScreen = () => {
    return (
        <Tab.Navigator
        tabBar={(props) => <TopBarCustom2TabsNotifi  {...props} />}
        screenOptions={{
            // tabBarLabelStyle: { fontSize: moderateScale(14,0.1),color:'#FF5959', fontWeight:'400',fontFamily:'Nunito' },
            // // tabBarItemStyle: { width: 100 },
            // tabBarStyle: { backgroundColor: 'white' },
            // tabBarScrollEnabled:false

          }}
        >
        <Tab.Screen name="Message" component={MessagesScreen} />
        <Tab.Screen name="Notifications" component={NotificationsScreen} />
      </Tab.Navigator>
    )
}



export default ClientNotificatonTopNavigationScreen

