import React,{useState,useEffect} from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractTopNavButton from '../../components/abstract/abstractTopNavButton'
import ProviderTopBarButton from '../../components/modules/common/provider/FinancialsCompo/ProviderTopBarButton'


interface Props {
    navigation?:any,
    state: {
        routes: any[];
        index: number;
      };
    descriptors?:any,
}

const TopBarFinancialCustom:React.FC<Props> = ({ state, descriptors, navigation }) => {

    return (
        <View style={{flexDirection: 'row',justifyContent:'center',alignItems:'center',marginTop:20,backgroundColor:'rgba(239, 239, 239, 1)'}}>
       <View style={{flexDirection:'row',width:'90%',height:moderateScale(50,0.1),backgroundColor:'rgba(239, 239, 239, 1)'}}>
        {state.routes.map((route: any, index: any) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;
  
          const isFocused = state.index === index;
          const isFirstIndex = index === 0;
          const isLastIndex = index === state.routes.length - 1;
          const onPress = () => {
            // console.log(`Pressed....`);
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });
  
            if (!isFocused && !event.defaultPrevented) {
              // console.log(`Navigating`);
              // The `merge: true` option makes sure that the params inside the tab screen are preserved
              navigation.navigate({name: route.name, merge: true});
            }
          };
  
          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };
  
          // console.log(route.name);
          switch (route.name) {
            case 'CompletedJobs':
              return (
                <ProviderTopBarButton
                label={'Completed Jobs'}
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}
                />
              );
              case 'PendingClearance':
              return (
                <ProviderTopBarButton
                  label={'Pending Clearance'}
                  isFirstIndex={isFirstIndex}
                  isLastIndex={isLastIndex}
                  onPress={onPress}
                  key={route.key}
                  isFocused={isFocused}
                  />
              );

              case 'Withdraw':
                return (
                  <ProviderTopBarButton
                    label={'Withdraw'}
                    isFirstIndex={isFirstIndex}
                    isLastIndex={isLastIndex}
                    onPress={onPress}
                    key={route.key}
                    isFocused={isFocused}
                    />
                );
              
            default:
              return false;
          }
        })}
        </View>
      </View>
    )
}

export default TopBarFinancialCustom

