import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import SpaceScreen from '../screens/Provider/ProviderServicesScreens/PlansScreen';
import DraftsScreen from '../screens/Provider/ProviderServicesScreens/DraftsScreen';
import ServicesScreen from '../screens/Provider/ProviderServicesScreens/ServicesScreen';
import { moderateScale } from 'react-native-size-matters';
import TopBarCustom3TabsServices from './CustomTopTabBars/TopBarCustom3TabsServices';
import PlansScreen from '../screens/Provider/ProviderServicesScreens/PlansScreen';



const Tab = createMaterialTopTabNavigator();



const TopTabBarNavigationProvideServices = () => {
    return (
        <Tab.Navigator
        tabBar={(props) => <TopBarCustom3TabsServices
          {...props} />
        }
        screenOptions={{
            // tabBarLabelStyle: { fontSize: moderateScale(14,0.1),color:'#FF5959', fontWeight:'400',fontFamily:'Nunito' },
            // // tabBarItemStyle: { width: 100 },
            // tabBarStyle: { backgroundColor: 'white' },
            // tabBarScrollEnabled:false

          }}
        >
        <Tab.Screen name="Plans" component={PlansScreen} />
        <Tab.Screen name="Drafts" component={DraftsScreen} />
      </Tab.Navigator>
    )
}


export default TopTabBarNavigationProvideServices