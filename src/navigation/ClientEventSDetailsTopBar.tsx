import React from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import OverViewScreen from '../screens/Client/ClientEventScreens/OverViewScreen';
import GuestsScreen from '../screens/Client/ClientEventScreens/GuestsScreen';
import { moderateScale } from 'react-native-size-matters';
import OverViewScreenTwo from '../screens/Client/ClientEventScreens/OverViewScreenTwo';


const Tab = createMaterialTopTabNavigator();


const ClientEventSDetailsTopBar = () => {
    return (
        <Tab.Navigator
        screenOptions={{
          tabBarLabelStyle: { fontSize: moderateScale(13,0.1) ,fontWeight:'600',textTransform:'capitalize' },
          // tabBarItemStyle: { width: 100 },
          tabBarStyle: { backgroundColor: 'white' },
          tabBarIndicatorStyle:{backgroundColor:'black'}
        }}
        >
        <Tab.Screen name="OverView" component={OverViewScreenTwo} />
        <Tab.Screen name="Guests" component={GuestsScreen} />
      </Tab.Navigator>
    )
}

export default ClientEventSDetailsTopBar


