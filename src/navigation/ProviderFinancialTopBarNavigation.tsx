import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { moderateScale } from 'react-native-size-matters';
import TopBarCustom2TabsBooking from './CustomTopTabBars/TopBarCustom2TabsBooking';
import CompletedJobsScreen from '../screens/Provider/ProviderFinancialScreens/CompletedJobsScreen';
import PendingClearanceScreen from '../screens/Provider/ProviderFinancialScreens/PendingClearanceScreen';
import WithdrawScreen from '../screens/Provider/ProviderFinancialScreens/WithdrawScreen';
import TopBarFinancialCustom from './CustomProviderTopBarNavigation/TopBarFinancialCustom';


const Tab = createMaterialTopTabNavigator();



const ProviderFinancialTopBarNavigation = () => {
    return (
        <Tab.Navigator
        tabBar={(props) => <TopBarFinancialCustom {...props} />}
        screenOptions={{
            // tabBarLabelStyle: { fontSize: moderateScale(14,0.1),color:'#FF5959', fontWeight:'400',fontFamily:'Nunito' },
            // // tabBarItemStyle: { width: 100 },
            // tabBarStyle: { backgroundColor: 'white' },
            tabBarScrollEnabled:false

          }}
        >
        <Tab.Screen name="CompletedJobs" component={CompletedJobsScreen} />
        <Tab.Screen name="PendingClearance" component={PendingClearanceScreen} />
        <Tab.Screen name="Withdraw" component={WithdrawScreen} />
        
      </Tab.Navigator>
    )
}


export default ProviderFinancialTopBarNavigation