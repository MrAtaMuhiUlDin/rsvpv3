import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ClientSearchScreen from '../screens/Client/ClientSearchScreens/ClientSearchScreen'
import ClientAdvanceSearchScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSearchScreen';
import ClientAdvanceSSFilterOneScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterOneScreen';
import ClientAdvanceSSFilterTwoScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterTwoScreen';
import ClientAdvanceSSFilterThreeScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterThreeScreen';
import ClientSearchMainOneScreen from '../screens/Client/ClientSearchScreens/ClientSearchMainOneScreen';
import ClientSearchMainTwoScreen from '../screens/Client/ClientSearchScreens/ClientSearchMainTwoScreen';
import ClientSupplieMainScreen from '../screens/Client/ClientSearchScreens/ClientSupplierScreen/ClientSupplieMainScreen';



const Stack = createNativeStackNavigator();



const ClientSearchScreenNavigation = () => {
    return (
   <Stack.Navigator screenOptions={{header:()=>false}}>
    <Stack.Screen
        name="IntroScreen"
        component={ClientSearchScreen}
     /> 
      <Stack.Screen
        name="AdvanceSearch"
        component={ClientAdvanceSearchScreen}
     />
      <Stack.Screen
        name="CelebrationType"
        component={ClientAdvanceSSFilterOneScreen}
     />
      <Stack.Screen
        name="PlanTypes"
        component={ClientAdvanceSSFilterTwoScreen}
     />
       <Stack.Screen
        name="Amenities"
        component={ClientAdvanceSSFilterThreeScreen}
     />
       <Stack.Screen
        name="ClientSearchMain"
        component={ClientSearchMainOneScreen}
     />
        {/* <Stack.Screen
        name="IntroScreen"
        component={ClientSearchMainTwoScreen}
     /> */}
      <Stack.Screen
     name="MainSupplier"
     component={ClientSupplieMainScreen}
  />
   </Stack.Navigator>
    )
}



export default ClientSearchScreenNavigation