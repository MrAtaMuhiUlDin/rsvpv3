import * as React from 'react';
import {
  Text,
  Pressable,
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  NavigationHelpersContext,
  useNavigationBuilder,
  TabRouter,
  TabActions,
  createNavigatorFactory,
} from '@react-navigation/native';

interface TabProps {
  initialRouteName?: any;
  children?: any;
  screenOptions?: any;
  tabBarStyle?: any;
  contentStyle?: any;
}

const TabNavigator: React.FC<TabProps> = ({
  initialRouteName,
  children,
  screenOptions,
  tabBarStyle,
  contentStyle,
}) => {
  const {state, navigation, descriptors, NavigationContent} =
    useNavigationBuilder(TabRouter, {
      children,
      screenOptions,
      initialRouteName,
    });

  return (
    <NavigationContent>
      <View style={[{flex: 1}, contentStyle]}>
        {state.routes.map((route, i) => {
          return (
            <View
              key={route.key}
              style={[
                StyleSheet.absoluteFill,
                {display: i === state.index ? 'flex' : 'none'},
              ]}>
              {descriptors[route.key].render()}
            </View>
          );
        })}
      </View>
      <View style={[{flexDirection: 'row'}, tabBarStyle, {height: 70}]}>
        {state.routes.map(route => (
          <TouchableOpacity
            key={route.key}
            onPress={() => {
              // console.log(`Pressed ${route.key}`);
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!event.defaultPrevented) {
                // console.log('NOt revented navigating ');
                navigation.dispatch({
                  ...TabActions.jumpTo(route.name),
                  target: state.key,
                });
              }
            }}
            style={{flex: 1}}>
            <Text>{descriptors[route.key].options.title || route.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </NavigationContent>
  );
};

export const createMyNavigator = createNavigatorFactory(TabNavigator);
