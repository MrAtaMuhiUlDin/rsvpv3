import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MyTabBar from '../components/modules/common/provider/bottomProviderTabBar';
import HomeProviderScreen from '../screens/Provider/homeProviderScreen';
import ScreenHeader from '../components/abstract/screenHeader';
import TopTabBarNavigationProvide from './ProviderTopNavigationAgenda';
import TopTabBarNavigationProvideServices from './ProviderServicesTopNavigation';
import ProviderNotificatonTopNavigationScreen from './ProviderNotificatonTopNavigationScreen';
import Main from '../screens/Main';
import ProviderFinancialScreen from '../screens/Provider/ProviderFinancialScreens/ProviderFinancialScreen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AddBasicDetailsScreen from '../screens/Provider/ProviderStepsScreens/AddBasicDetailsScreen';
import TypesOfEventsScreen from '../screens/Provider/ProviderStepsScreens/TypesOfEventsScreen';
import CharacteristicsScreen from '../screens/Provider/ProviderStepsScreens/CharacteristicsScreen';
import SelectPriceMethodScreen from '../screens/Provider/ProviderStepsScreens/SelectPriceMethodScreen';
import SelectPriceMethodScreenTwo from '../screens/Provider/ProviderStepsScreens/SelectPriceMethodScreenTwo';
import SelectAvailabilityScreen from '../screens/Provider/ProviderStepsScreens/SelectAvailabilityScreen';
import SelectAvailabilityScreenTwo from '../screens/Provider/ProviderStepsScreens/SelectAvailabilityScreenTwo';
import ProviderChatScreen from '../screens/Provider/ProviderChatScreen/ProviderChatScreen';
import ProviderManageMyAccountScreen from '../screens/Provider/ProviderProfileScreens/ProviderManageMyAccountScreen';
import ProfileFinancialScreen from '../screens/Provider/ProviderProfileScreens/ProfileFinancialScreen';
import TopBarCustom2TabsNotifi from './CustomTopTabBars/TopBarCustom2TabsNotifi';
import ProfileProviderScreen from '../screens/Provider/profileProviderScreen';
import ProviderBookingConfirmScreen from '../screens/Provider/ProviderBookingConfirmScreen';


const Stack = createNativeStackNavigator();




const MainProviderStackNavigation = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen name="BottomTabNavi" component={BottomTabNavigator} options={{header:()=>null}} />
    <Stack.Screen name="AddBasic" component={AddBasicDetailsScreen} options={{header:()=>null}} />
    <Stack.Screen name="TypesOfEvents" component={TypesOfEventsScreen} options={{header:()=>null}} />
    <Stack.Screen name="Characteristics" component={CharacteristicsScreen} options={{header:()=>null}} />
    <Stack.Screen name="PriceMethodOne" component={SelectPriceMethodScreen} options={{header:()=>null}} />
    <Stack.Screen name="PriceMethodTwo" component={SelectPriceMethodScreenTwo} options={{header:()=>null}} />
    <Stack.Screen name="AvailabiltyOne" component={SelectAvailabilityScreen} options={{header:()=>null}} />
    <Stack.Screen name="AvailabiltyTwo" component={SelectAvailabilityScreenTwo} options={{header:()=>null}} />
    <Stack.Screen name="ChatScreen" component={ProviderChatScreen} options={{header:()=>null}} />
    <Stack.Screen name="ProviderManageAccount" component={ProviderManageMyAccountScreen} options={{header:()=>null}} />
    <Stack.Screen name="ProfileFinance" component={ProfileFinancialScreen} options={{header:()=>null}} />
    <Stack.Screen name="ProviderFinincial" component={ProviderFinancialScreen} options={{header:()=>null}} />
    <Stack.Screen name="BookingConfirm" component={ProviderBookingConfirmScreen} options={{header:()=>null}} />
    </Stack.Navigator>

  )
}




const My = createBottomTabNavigator();

export const CLIENT_TABS = {
  HOME: 'HomeScreen',
  CALENDAR: 'CalendarScreen',
  Business: 'BusinessScreen',
  NOTIFICATIONS: 'NotificationsScreen',
  PROFILE: 'ProfileScreen',
};

function BottomTabNavigator() {
  return (
    <My.Navigator
      // screenOptions={{header: props => <ScreenHeader />}}
      tabBar={props => <MyTabBar {...props} />}
      initialRouteName={CLIENT_TABS.HOME}>
      <My.Screen name={CLIENT_TABS.HOME} component={HomeProviderScreen} options={{header:()=><ScreenHeader />}} />
      <My.Screen
        name={CLIENT_TABS.CALENDAR}
        component={TopTabBarNavigationProvide}
        options={{header:()=>null}}
      />
      <My.Screen
        name={CLIENT_TABS.Business}
        component={TopTabBarNavigationProvideServices}
        options={{header:()=>null}}
      />
      <My.Screen
        name={CLIENT_TABS.NOTIFICATIONS}
        component={ProviderNotificatonTopNavigationScreen}
        options={{header:()=>null}}
      />
      <My.Screen name={CLIENT_TABS.PROFILE} component={ProfileProviderScreen} options={{header:()=>null}} />


    </My.Navigator>
  );
}
export default MainProviderStackNavigation;
