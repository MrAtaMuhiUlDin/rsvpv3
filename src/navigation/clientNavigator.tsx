import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from '../screens/auth/loginScreen';
import RegisterClientScreen from '../screens/auth/registerClientScreen';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MyTabBar from '../components/modules/common/client/bottomClientTabBar';
import HomeClientScreen from '../screens/Client/homeClientScreen';
import ProfileClientScreen from '../screens/Client/profileClientScreen';
import ScreenHeader from '../components/abstract/screenHeader';
import ClientNotificatonTopNavigationScreen from './ClientNotificatonTopNavigationScreen';
import ClientBookingsTopNavigation from './ClientBookingsTopNavigation';
import ClientSearchScreenNavigation from './ClientSearchScreenNavigation';
import ClientPaymentMethodScreen from '../screens/Client/ClientProfileScreens/ClientPaymentMethodScreen';
import ClientManageMyAccountScreen from '../screens/Client/ClientProfileScreens/ClientManageMyAccountScreen';
import ClientChatScreen from '../screens/Client/ClientChatScreen/ClientChatScreen';
import ClientBookingConfirmationScreen from '../screens/Client/ClientBookingConfirmationScreen';
import CreateEventScreen from '../screens/Client/ClientEventScreens/CreateEventScreen';
import CheckListScreen from '../screens/Client/ClientEventScreens/CheckListScreen';
import AddGuestsScreen from '../screens/Client/ClientEventScreens/AddGuestsScreen';
import EventsDetailsScreen from '../screens/Client/ClientEventScreens/EventsDetailsScreen';
import Main from '../screens/Main';
import ClientAdvanceSearchScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSearchScreen';
import ClientSearchMainOneScreen from '../screens/Client/ClientSearchScreens/ClientSearchMainOneScreen';
import ClientSearchMainTwoScreen from '../screens/Client/ClientSearchScreens/ClientSearchMainTwoScreen';
import ClientSupplieMainScreen from '../screens/Client/ClientSearchScreens/ClientSupplierScreen/ClientSupplieMainScreen';
import ClientSearchScreen from '../screens/Client/ClientSearchScreens/ClientSearchScreen';
import GuestsScreen from '../screens/Client/ClientEventScreens/GuestsScreen';
import InformaciondeInvitadosScreen from '../screens/Client/ClientBookingScrrens/InformaciondeInvitadosScreen';
import DetallesdePagoScreen1 from '../screens/Client/ClientBookingScrrens/DetallesdePagoScreen1';
import DetallesdePagoScreen2 from '../screens/Client/ClientBookingScrrens/DetallesdePagoScreen2';
import RevisarBookingPagoCompo from '../components/modules/common/client/ClientBookingGuestScreensCompo/RevisarBookingPagoCompo';
import ReviewBookingScreen from '../screens/Client/ClientBookingScrrens/ReviewBookingScreen';
import CelebrationTypes from '../components/modules/common/client/ClientAdvancedSearcScreenCompo/CelebrationTypes';
import ClientAdvanceSSFilterOneScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterOneScreen';
import ClientAdvanceSSFilterTwoScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterTwoScreen';
import ClientAdvanceSSFilterThreeScreen from '../screens/Client/ClientSearchScreens/ClientAdvanceSSFilterThreeScreen';

const My = createBottomTabNavigator();
const Stack = createNativeStackNavigator();



const StackForWholeClientNavigation = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen name="BottomTabNavi" component={BottomTabNavigator} options={{header:()=>null}} />
    <Stack.Screen name="ChatScreen" component={ClientChatScreen} options={{header:()=>null}} />
    <Stack.Screen name="BookingConfirm" component={ClientBookingConfirmationScreen} options={{header:()=>null}} />
    <Stack.Screen name="CreateEvent" component={CreateEventScreen} options={{header:()=>null}} />
    <Stack.Screen name="CheckList" component={CheckListScreen} options={{header:()=>null}} />
    <Stack.Screen name="GuestAdd" component={AddGuestsScreen} options={{header:()=>null}} />
    <Stack.Screen name="EventDetail" component={EventsDetailsScreen} options={{header:()=>null}} />
    <Stack.Screen name="PaymentMethod" component={ClientPaymentMethodScreen} options={{header:()=>null}} />
    <Stack.Screen name="ManageMyAccount" component={ClientManageMyAccountScreen} options={{header:()=>null}} />
    <Stack.Screen name="SearchMainOne" component={ClientSearchMainOneScreen} options={{header:()=>null}} />
    <Stack.Screen name="SupplierMain" component={ClientSupplieMainScreen} options={{header:()=>null}} />
    <Stack.Screen name="Informacionde" component={InformaciondeInvitadosScreen} options={{header:()=>null}} />
    <Stack.Screen name="DetallesdePago1" component={DetallesdePagoScreen1} options={{header:()=>null}} />
    <Stack.Screen name="DetallesdePago2" component={DetallesdePagoScreen2} options={{header:()=>null}} />
    <Stack.Screen name="RevisarBooking" component={ReviewBookingScreen} options={{header:()=>null}} />
    <Stack.Screen name="AdvanceSearch" component={ClientAdvanceSearchScreen} options={{header:()=>null}} />
    <Stack.Screen name="CelebrationType" component={ClientAdvanceSSFilterOneScreen} options={{header:()=>null}} />
    <Stack.Screen name="PlanTypes" component={ClientAdvanceSSFilterTwoScreen} options={{header:()=>null}} />
    <Stack.Screen name="Amenities" component={ClientAdvanceSSFilterThreeScreen} options={{header:()=>null}} />

    
  </Stack.Navigator>
  )
}



export const CLIENT_TABS = {
  HOME: 'HomeScreen',
  CALENDAR: 'CalendarScreen',
  SEARCH: 'SearchScree',
  NOTIFICATIONS: 'NotificationsScreen',
  PROFILE: 'ProfileScreen',
};

function BottomTabNavigator() {
  return (
    <My.Navigator
      // screenOptions={{header: props => <ScreenHeader />}}
      tabBar={props => <MyTabBar {...props} />}
      initialRouteName={CLIENT_TABS.HOME} >
      <My.Screen name={CLIENT_TABS.HOME} component={HomeClientScreen} options={{header:()=><ScreenHeader />}}  />
      <My.Screen name={CLIENT_TABS.CALENDAR} component={ClientBookingsTopNavigation}
       options={{header:()=>null}}
      />
      {/* <My.Screen name={CLIENT_TABS.SEARCH} component={ClientSearchScreenNavigation}
       options={{header:()=>null}}
      /> */}
      <My.Screen name={CLIENT_TABS.SEARCH} component={ClientSearchScreen}
       options={{header:()=>null}}
      />
      <My.Screen
        name={CLIENT_TABS.NOTIFICATIONS}
        component={ClientNotificatonTopNavigationScreen}
        options={{header:()=>null}}
      />
      <My.Screen name={CLIENT_TABS.PROFILE} component={ProfileClientScreen}
       options={{header:()=>null}}
      />
    </My.Navigator>
  );
}





export default StackForWholeClientNavigation;
