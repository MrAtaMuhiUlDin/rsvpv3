import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ProviderAgendaScreen from '../screens/Provider/ProviderAgendaScreens/ProviderAgendaScreen';
import ProviderPendingScreen from '../screens/Provider/ProviderAgendaScreens/ProviderPendingScreen'
import ProviderScheduledScreen from '../screens/Provider/ProviderAgendaScreens/ProviderScheduledScreen'
import { moderateScale } from 'react-native-size-matters';
import TopBarCustom3TabsAgenda from './CustomTopTabBars/TopBarCustom3TabsAgenda';




const Tab = createMaterialTopTabNavigator();



const TopTabBarNavigationProvide = () => {
    return (
        <Tab.Navigator
        tabBar={(props) => <TopBarCustom3TabsAgenda
          {...props} />
        }
        screenOptions={{
            // tabBarLabelStyle: { fontSize: moderateScale(14,0.1),color:'#FF5959', fontWeight:'400',fontFamily:'Nunito' },
            // // tabBarItemStyle: { width: 100 },
            // tabBarStyle: { backgroundColor: 'white' },
            // tabBarScrollEnabled:false

          }}
        >
        <Tab.Screen name="Agenda" component={ProviderAgendaScreen}  />
        <Tab.Screen name="Scheduled" component={ProviderScheduledScreen} />
        <Tab.Screen name="Pending" component={ProviderPendingScreen} />
      </Tab.Navigator>
    )
}


export default TopTabBarNavigationProvide