import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import IntroScreen from '../screens/auth/introScreen';
import FocusAwareStatusBar from '../components/abstract/statusbarConfiguration';
import LoginScreen from '../screens/auth/loginScreen';
import RegisterClientScreen from '../screens/auth/registerClientScreen';
import RegisterProviderScreen from '../screens/auth/registerProviderScreen';
import RegisterUserTypesScreen from '../screens/auth/registerUserTypesScreen';
import ConfirmationClientScreen from '../screens/auth/confermationClientScreen';
import ConfirmationProviderScreen from '../screens/auth/confermationProviderScreen';
import ClientNavigator from './clientNavigator';
import ProviderNavigator from './providerNavigator';
import StackForWholeClientNavigation from './clientNavigator';
import MainProviderStackNavigation from '../navigation/providerNavigator'
const AuthStack = createNativeStackNavigator();

export const navigationRef: any = React.createRef();

export function navigate(name: string, params?: string) {
  navigationRef.current?.navigate(name, params);
}

export function clearAndNavigate(name: string, params?: string) {
  navigationRef.current?.reset({
    index: 0,
    routes: [{name}],
    params: params,
  });
}

export function goBack() {
  navigationRef.current?.goBack();
}

export default function Navigator() {
  return (
    <NavigationContainer ref={navigationRef}>
      <AuthStack.Navigator initialRouteName="IntroScreen">
          <AuthStack.Screen
          name="IntroScreen"
          component={IntroScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="LoginScreen"
          component={LoginScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="RegisterClientScreen"
          component={RegisterClientScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="RegisterProviderScreen"
          component={RegisterProviderScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="RegisterUserTypeScreen"
          component={RegisterUserTypesScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="ConformationClientScreen"
          component={ConfirmationClientScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        />
        <AuthStack.Screen
          name="ConfirmationProviderScreen"
          component={ConfirmationProviderScreen}
          options={{
            gestureEnabled: true,
            animation: 'slide_from_right',
            header: () => null,
          }}
        /> 

        <AuthStack.Screen
          name="ClientNavigator"
          component={StackForWholeClientNavigation}
          options={{
            gestureEnabled: false,
            animation: 'slide_from_left',
            header: () => null,
          }}
        /> 
          <AuthStack.Screen
          name="ProviderNavigator"
          component={MainProviderStackNavigation}
          options={{
            gestureEnabled: false,
            animation: 'slide_from_left',
            header: () => null,
          }} 
        /> 
      </AuthStack.Navigator>
    </NavigationContainer>
  );
}

//  options={{
//             cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
//             gestureEnabled: true,
//             gestureDirection: 'horizontal',
//             headerShown: false,
//           }}
