import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import CheckBox from '@react-native-community/checkbox';
import { moderateScale } from 'react-native-size-matters'
import PlusSvg from '../../../assets/icons/EventDetailsScreenSvgs/PlusSvg'
import SimplePlus from '../../../assets/icons/SimplePlus'
import ModalAddTask from './ModalAddTask'
import { goBack } from '../../../navigation/authNavigator'
import ScreenHeader from '../../../components/abstract/screenHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'



const InternalCheckListCompo = () => {

    const [toggleCheckBox, setToggleCheckBox] = useState<boolean>(false)

    return (
   
        
        <View style={{ width: '100%', height: 40, flexDirection: 'row',marginVertical:5 }}>
            <View style={{ width: '10%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <CheckBox
                    tintColors={{ true: '#FF5959', false: 'grey' }}
                    onCheckColor={'#FF5959'}
                    disabled={false}
                    value={toggleCheckBox}
                    onValueChange={(newValue) => setToggleCheckBox(newValue)}
                />
            </View>


            <View style={{ width: '90%', height: '100%',justifyContent:'center' }}>
                <Text style={{fontFamily:'OpenSans',fontSize:moderateScale(11,0.1),fontWeight:'600',color:'#868E96'}}>
                Crear evento en RSVPassadasbaasasas
                </Text>
            </View>
        </View>
 
    )
}






const CreateTaskButton = ({onPress}:{onPress?:()=>void}) => {
    return(
        <TouchableOpacity
        onPress={onPress}
        style={styles.createButtonView}>
             
             <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
             <SimplePlus />
             <Text style={styles.createBtnText}>create task</Text>
             </View>
            
             
        </TouchableOpacity>
    )
}
 


const CheckListScreen: React.FC = () => {

const [ModalVisible,setModalVisible] = useState<boolean>(false)


    return (
        <ScreenContainer>
                <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'#FF5959'} />
            <View style={styles.mainContainer}>

                <AbstractHeader type={'EventScreenHeader2'}  onPressBack={()=>goBack()} />
                 <ScrollView showsVerticalScrollIndicator={false}>
                <ContentContainer>

                     <View style={{marginTop:30}}>
                    <InternalCheckListCompo />
                    <InternalCheckListCompo />
                    <InternalCheckListCompo />
                    <InternalCheckListCompo />
                    <InternalCheckListCompo />
                    <InternalCheckListCompo />
                    </View>


                </ContentContainer>
                </ScrollView>

                <ContentContainer>
                <View style={{marginBottom:25}}>
                        <CreateTaskButton onPress={()=>setModalVisible(true)}  />
                    </View>
                </ContentContainer>

            </View>
         <ModalAddTask visible={ModalVisible} />

        </ScreenContainer>
    )
}

export default CheckListScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    createButtonView:{
        height:moderateScale(45,0.1),
        width:'100%',
        backgroundColor:'white',
        borderRadius:10,
        borderColor:'#FF5959',
        borderWidth:0.5,
        justifyContent:'center',
        alignItems:'center'
    },
    createBtnText:{
        textTransform:'uppercase',
        color:'#FF5959',
        fontFamily:'OpenSans',
        fontSize:moderateScale(14,0.1),
        fontWeight:'700',
        paddingLeft:10
    }
})
