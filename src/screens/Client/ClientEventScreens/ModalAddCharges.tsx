import React from 'react';
import { Dimensions, View, Text, StyleSheet } from 'react-native';
import AbstractButton from '../../../components/abstract/abstractButton';
import AbstractTextInput from '../../../components/abstract/abstractTextInput';
import Modal from 'react-native-modal';
import { useWindowDimensions } from 'react-native';
import { Colors } from '../../../theme/colors';
import { useState } from 'react';
import { useEffect } from 'react';
import { moderateScale } from 'react-native-size-matters';


interface Props {
    visible?: boolean;
    onSubmit?: (email: string) => void;
    onCancel?: () => void;
}

const ModalAddCharges: React.FC<Props> = ({
    visible,
    onSubmit,
    onCancel,
}) => {
    const [isVisible, setVisible] = useState(false);
    const [item, setItem] = useState('paper Towel');
    const [price, setPrice] = useState('$20');
    useEffect(() => {
        setVisible(visible ? true : false);
    }, [visible]);

    const handleOnCancel = () => {
        if (onCancel) onCancel();
        setVisible(false);
        setPrice('');
    };
    const handleSubmit = () => {
        if (onSubmit) onSubmit(price);
        setVisible(false);
        setPrice('');
    };

    // const {height} = useWindowDimensions()
    return (
        <Modal
            isVisible={isVisible}
            onBackButtonPress={() => setVisible(false)}
            backdropOpacity={0.5}
            backdropColor={'black'}
            animationIn={'zoomIn'}
            animationOut={'zoomOut'}
            coverScreen={false}
            backdropTransitionOutTiming={0}>
            <View
                style={{
                    width: '100%',
                    alignSelf: 'center',
                    backgroundColor: 'white',
                    height: moderateScale(289, 0.1),
                    padding: 10,
                    paddingLeft: 20,
                    paddingRight: 20,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                }}>
                <View style={{ width: '100%', height: '90%' }}>

                    <View style={styles.viewOne}>
                        <Text style={styles.textOne} >Add Charge to Others</Text>
                    </View>


                    <View style={{ width: '100%', height: 120 }}>
                        <AbstractTextInput
                            value={item}
                            onChangeText={text => setItem(text)}
                            label="Item"
                        />

                        <AbstractTextInput
                            value={price}
                            onChangeText={text => setPrice(text)}
                            label="Price"
                        />
                    </View>



                    <View style={styles.viewTwo}>
                        <AbstractButton onPress={() => handleSubmit()} title="SAVE" />
                    </View>



                </View>

            </View>
        </Modal>
    );
};
export default ModalAddCharges;




const styles = StyleSheet.create({
    viewOne: {
        width: '100%',
        height: 35,
        // backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: 100,
        // backgroundColor: 'black',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    textOne: {
        fontSize: moderateScale(21, 0.1),
        fontFamily: 'OpenSans',
        fontWeight: '700',
        color: '#FF5959'
    }
})