import React,{useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import EventOptionsBottomSheet from '../../../components/modules/common/client/BottomSheetsClient/EventOptionsBottomSheet'
import { goBack } from '../../../navigation/authNavigator'
import ClientEventSDetailsTopBar from '../../../navigation/ClientEventSDetailsTopBar'


const HeaderCompoCustom = () => {
    return (
    <></>
    )
}



const EventsDetailsScreen:React.FC = () => {

        const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }
    
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
        <View style={styles.mainContainer}>
            <AbstractHeader type={'EventScreenHeader1'}  onPressBack={()=>goBack()} onPressOptions={toggleModal} /> 
            <ClientEventSDetailsTopBar />
      
        </View>


        <AbstractBottomSheet onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom/>}>
            <EventOptionsBottomSheet />
            </AbstractBottomSheet>
        </ScreenContainer>
    )
}

export default EventsDetailsScreen

const styles = StyleSheet.create({
    mainContainer:{
        flex:1
    }
})
