import React,{useState} from 'react'
import { ScrollView, StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import SplitBillBottomSheet from '../../../components/modules/common/client/BottomSheetsClient/SplitBillBottomSheet'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import AddContact from '../../../components/modules/common/client/ClientEventDetailScreenCompo/AddContact'
import BillModule from '../../../components/modules/common/client/ClientEventDetailScreenCompo/BillModule'
import EventAddButton from '../../../components/modules/common/client/ClientEventDetailScreenCompo/EventAddButton'
import Guest from '../../../components/modules/common/client/ClientEventDetailScreenCompo/Guest'
import CreateEventModule from '../../../components/modules/common/client/ClientEventScreensCompo/CreateEventModule'




const HeaderCompoCustom = () => {
    return (
    <></>
    )
}


const GuestsScreen: React.FC = () => {

    const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
            <View style={styles.mainContainer}>
                <ScrollView showsVerticalScrollIndicator={false} >
                <ContentContainer style={{width:'95%'}}>
                   
                   <AddContact />
                   <Guest Simple={false} />
                   <Guest Simple={false} />
                   <Guest Simple={false} />
                   <Guest Simple={false} />
                  

                </ContentContainer>
                </ScrollView>

                <View style={{width:'100%',marginBottom:20}}> 
                <ContentContainer style={{width:'95%'}} >
                  <BillModule onPressSettings={toggleModal} />
                  </ContentContainer>
                 </View>     
            </View>

            <AbstractBottomSheet onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom/>}>
                <SplitBillBottomSheet />
            </AbstractBottomSheet>
        </ScreenContainer>
    )
}

export default GuestsScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    viewOne: {
        width: '100%',
        height: 70,
        // backgroundColor: 'red',
        borderBottomColor:'lightgrey',
        borderBottomWidth: 0.5
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'black'
    },
    textOne:{
        fontSize:moderateScale(15,0.1),
        color:'#868E96',
        fontFamily:'OpenSans',
        fontWeight:'600',
        paddingTop:15
    }
})
