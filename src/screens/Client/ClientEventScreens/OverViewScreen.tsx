import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ClockSvg from '../../../assets/icons/ClockSvg'
import EventDetailCalenderSvgs from '../../../assets/icons/EventDetailsScreenSvgs/EventDetailCalenderSvgs'
import LocationSvg from '../../../assets/icons/LocationSvg'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import BudgetCompo from '../../../components/modules/common/client/ClientEventDetailScreenCompo/BudgetCompo'
import CheckListButton from '../../../components/modules/common/client/ClientEventDetailScreenCompo/CheckListButton'
import ClientEventDetailsCompo from '../../../components/modules/common/client/ClientEventDetailScreenCompo/ClientEventDetailsCompo'
import EventAddButton from '../../../components/modules/common/client/ClientEventDetailScreenCompo/EventAddButton'
import { navigate } from '../../../navigation/authNavigator'



const OverViewScreen:React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar backgroundColor={'white'} barStyle={'dark-content'} />
        <View style={styles.mainContainer}>
            <ScrollView showsVerticalScrollIndicator={false} >
            <ContentContainer style={{ width: '90%' }}>

                <View style={{ width: '100%', height: 60, justifyContent: 'center',marginBottom:'2%' }}>

                    <Text style={styles.headingLarge}>Event Details</Text>

                </View>

                <View style={{ width: '100%', height: 150,marginBottom:'2%' }}>

                    <ClientEventDetailsCompo Svg={<EventDetailCalenderSvgs />} text={'11 May, 2018'} />
                    <ClientEventDetailsCompo Svg={<ClockSvg color={'#979797'} />} text={'3:40 PM - 5:40 PM'} />
                    <ClientEventDetailsCompo Svg={<LocationSvg color={'#979797'} />} text={'Hotel Hilton, Ciudad de Panama, Panama'} />

                </View>

                <View style={{ width: '100%', height: 60,marginBottom:'2%', justifyContent: 'center' }}>

                    <Text style={styles.headingLarge}>Budget Details</Text>

                </View>

                <BudgetCompo height={moderateScale(60, 0.1)} />

                <View style={{ width: '100%', height: 140, marginVertical: 20, justifyContent: 'space-between' }}>

                    <EventAddButton/>
                    <EventAddButton  onlyAdd  />

                </View>

                <View style={{width:'100%',height:100,justifyContent:'center'}}>
                <CheckListButton onPress={()=>navigate('CheckList')} />
                </View>



            </ContentContainer>
            </ScrollView>
        </View>
        </ScreenContainer>
    )
}

export default OverViewScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    headingLarge:{
        fontFamily: 'OpenSans', 
        fontWeight: '700',
         fontSize: moderateScale(17, 0.1)
    }
})
