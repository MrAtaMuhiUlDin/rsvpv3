import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import EventAddButton from '../../../components/modules/common/client/ClientEventDetailScreenCompo/EventAddButton'
import CreateEventModule from '../../../components/modules/common/client/ClientEventScreensCompo/CreateEventModule'
import { navigate } from '../../../navigation/authNavigator'


const CreateEventScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
            <View style={styles.mainContainer}>
                <AbstractHeader type={'CreateEventHeader'} />
                <ScrollView showsVerticalScrollIndicator={false} >
                <ContentContainer>
                    <View style={styles.viewOne}>
                     <Text style={styles.textOne}>Nombre del Evento </Text>
                    </View>


                    <View>
                        <CreateEventModule />
                    </View>

                    <View style={{width:'100%',height:140,marginTop:20,justifyContent:'space-between'}}>

                        <EventAddButton onlyAdd />
                        <EventAddButton  />
                        
                    </View>
                    </ContentContainer>
                </ScrollView>
                      

                      <ContentContainer>
                    <View style={{width:'100%',position:'absolute',bottom:0}}>

                        <View style={{width:'100%',height:60,flexDirection:'row',justifyContent:'space-between'}}>
                            <AbstractButton onPress={()=>navigate('GuestAdd')} textSize={moderateScale(15,0.1)} height={moderateScale(46,0.1)} title={'ADD GUESTS'}  width={'49%'} btnStyle={'transparentTwo'} />
                            <AbstractButton textSize={moderateScale(15,0.1)} height={moderateScale(46,0.1)} title={'CREATE EVENT'} width={'49%'} />
                        </View>

                    </View>

                    </ContentContainer>
             
            </View>
        </ScreenContainer>
    )
}

export default CreateEventScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        position:'relative'
    },
    viewOne: {
        width: '100%',
        height: 70,
        // backgroundColor: 'red',
        borderBottomColor:'lightgrey',
        borderBottomWidth: 0.5
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'black'
    },
    textOne:{
        fontSize:moderateScale(15,0.1),
        color:'#868E96',
        fontFamily:'OpenSans',
        fontWeight:'600',
        paddingTop:15
    }
})
