import React from 'react';
import {View, Text,StatusBar, StyleSheet} from 'react-native';
import AbstractProfileNavigation from '../../components/abstract/AbstractProfileNavigation';
import ContentContainer from '../../components/abstract/contentContainer';
import ProfileScreenModule from '../../components/modules/common/provider/ProfileScreenModule';
import { moderateScale } from 'react-native-size-matters';


import {clearAndNavigate, navigate} from '../../navigation/authNavigator';

const ProfileClientScreen: React.FC = props => {
  return (
    <View style={{ flex: 1, backgroundColor: 'white' }}>
          <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />

    <View style={styles.viewOne}>
      <ProfileScreenModule image imgUrl={require('../../assets/images/ppic.png')} />
    </View>


    <View>
      <ContentContainer style={{width:'80%'}}>
        <AbstractProfileNavigation text={'Payment Methods'}  onPress={()=>navigate('PaymentMethod')} />
        <AbstractProfileNavigation text={'Terms of Service'} />
        <AbstractProfileNavigation text={'Manage My Account'} onPress={()=>navigate('ManageMyAccount')} />
        <AbstractProfileNavigation logout onPress={()=> clearAndNavigate('IntroScreen')} />


      </ContentContainer>
    </View>

  </View>
  );
};
export default ProfileClientScreen;



const styles = StyleSheet.create({
  viewOne:{
    width: '100%',
     alignItems: 'center',
      backgroundColor: 'white',
       height: moderateScale(180, 0.1),
        justifyContent: 'center',

  }
})