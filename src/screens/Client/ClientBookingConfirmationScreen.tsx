import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../components/abstract/contentContainer'
import AbstractHeader from '../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import { goBack } from '../../navigation/authNavigator'


const ClientBookingConfirmationScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
            <View style={styles.mainContainer}>
                <AbstractHeader type={'SimpleBackHeader'}  onPressBack={()=>goBack()} />
              <ScrollView showsVerticalScrollIndicator={false}>
                <ContentContainer>
                    <View style={{width:'100%',height:60,justifyContent:'center'}}>
                     <Text style={styles.LargeHeading}>Booking Confirmation</Text>
                    </View>

                 <View style={{width:'100%',alignItems:'center'}}>

                <ClientBookingConfirmation />

                 </View>


                </ContentContainer>
                </ScrollView>

            </View>
        </ScreenContainer>
    )
}

export default ClientBookingConfirmationScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'black'
    }
})
