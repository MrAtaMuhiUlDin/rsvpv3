import React from 'react';
import { View, Text, ScrollView,StatusBar, StyleSheet } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../../components/abstract/contentContainer'
import NotificationsModule from '../../../components/modules/common/provider/NotificationsModule';
import { navigate } from '../../../navigation/authNavigator';



const NotificationsScreen: React.FC = props => {
  return (
    <ScreenContainer>
    <View style={{ flex: 1 }}>
        <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />
      <ScrollView showsVerticalScrollIndicator={false}>
       <ContentContainer style={{width:'95%'}}>
      <View style={{ width: '100%', marginTop: 25 }}>
        <ScrollView showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>
           <View style={{ width: '100%'}}>
            <Text style={styles.headingStyle}>Today</Text>
          </View>
   
          <NotificationsModule today onPress={()=>navigate('BookingConfirm')} />   

          <View style={{ width: '100%'}}>
            <Text style={styles.headingStyle}>Yesterday</Text>
          </View>
   
          <NotificationsModule onPress={()=>navigate('BookingConfirm')} />


          <View style={{ width: '100%'}}>
            <Text style={styles.headingStyle}>Mon 25, June</Text>
          </View>
   
          <NotificationsModule onPress={()=>navigate('BookingConfirm')} />
      
         

        </ScrollView>
      </View>
      </ContentContainer>
      </ScrollView>
    </View>
    </ScreenContainer>
  );
};
export default NotificationsScreen;




const styles = StyleSheet.create({
  headingStyle:{
    fontSize: moderateScale(14, 0.1),
     fontWeight: '400',
     fontFamily: 'Nunito',
     color: '#777777'
  }
})