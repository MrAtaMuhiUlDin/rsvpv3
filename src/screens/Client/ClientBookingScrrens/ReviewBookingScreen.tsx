import React, { useState } from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import DetallesdePagoCompo from '../../../components/modules/common/client/ClientBookingGuestScreensCompo/DetallesdePagoCompo'
import CheckBox from '@react-native-community/checkbox';
import RevisarBookingPagoCompo from '../../../components/modules/common/client/ClientBookingGuestScreensCompo/RevisarBookingPagoCompo'
import { goBack, navigate } from '../../../navigation/authNavigator'



const ReviewBookingScreen: React.FC = () => {

    const [toggleCheckBox, setToggleCheckBox] = useState<boolean>(false)


    return (
        <ScreenContainer>
            <FocusAwareStatusBar
                backgroundColor={'#f5f5f6'}
                barStyle={'dark-content'}
            />
            <View style={styles.mainContainer}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ width: '100%', height: moderateScale(435, 0.1) }}>
                        <ImageBackground source={require('../../../assets/images/Rectangle.png')} resizeMode={'stretch'} style={{ width: '100%', height: '100%' }}>
                            <AbstractHeader type={'SimpleBackHeader'} bgColor={'transparent'} onPressBack={()=>goBack()} />
                            <ContentContainer style={{ width: '85%' }} >

                                <View style={{ width: '100%', height: 60, justifyContent: 'center' }}>
                                    <Text style={styles.LargeHeading}>Revisar Booking</Text>
                                </View>

                                <View style={{ width: '100%', height: moderateScale(515, 0.1), justifyContent: 'center', alignItems: 'center' }}>
                                    <RevisarBookingPagoCompo />
                                </View>



                            </ContentContainer>
                        </ImageBackground>
                    </View>

                    <ContentContainer>
                        <View style={{ width: '100%', height: moderateScale(30, 0.1), marginTop: 250,justifyContent:'center',alignItems:'center' }}>
                               
                                    <View>
                                        <Text style={styles.textOne}>
                                            Agregar a Evento +
                                        </Text>
                                    </View>
                        </View>


                        <View style={{ width: '100%', height: 100, justifyContent: 'center' }}>
                            <AbstractButton title={'Confirm & Pay'} textSize={moderateScale(14, 0.1)} onPress={()=>navigate('BookingConfirm')} />
                        </View>

                    </ContentContainer>


                </ScrollView>
            </View>
        </ScreenContainer>
    )
}

export default ReviewBookingScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        position: 'relative'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textOne: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#868E96'
    }
})
