import React from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import InformaciondeInvitadosCompo from '../../../components/modules/common/client/ClientBookingGuestScreensCompo/InformaciondeInvitadosCompo'
import ChatBar from '../../../components/modules/common/client/ClientChatScreenSCompo/ChatBar'
import ReceiverCompo from '../../../components/modules/common/client/ClientChatScreenSCompo/ReceiverCompo'
import SenderCompo from '../../../components/modules/common/client/ClientChatScreenSCompo/SenderCompo'
import { goBack, navigate } from '../../../navigation/authNavigator'








const InformaciondeInvitadosScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar
               backgroundColor={'#f5f5f6'}
                barStyle={'dark-content'}
            />
            <View style={styles.mainContainer}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ width: '100%', height: moderateScale(435, 0.1) }}>
                        <ImageBackground source={require('../../../assets/images/Rectangle.png')} resizeMode={'stretch'} style={{ width: '100%', height: '100%' }}>
                            <AbstractHeader type={'SimpleBackHeader'} bgColor={'transparent'} onPressBack={()=>goBack()} />
                            <ContentContainer style={{ width: '85%' }} >

                                <View style={{ width: '100%', height: 55, justifyContent: 'center' }}>
                                    <Text style={styles.LargeHeading}>Informacion de Invitados</Text>
                                </View>

                                <View style={{ width: '100%', height: 290, justifyContent: 'center', alignItems: 'center' }}>
                                    <InformaciondeInvitadosCompo />
                                </View>



                            </ContentContainer>
                        </ImageBackground>
                    </View>

                    <ContentContainer>
                        <View style={{ width: '100%', height: moderateScale(300, 0.1) }}>
                            <View style={{ width: '100%' }}>
                                <Text style={styles.smallHeading}>Guest Details</Text>
                            </View>
                            <View style={{ width: '100%', height: moderateScale(230, 0.1) }}>
                                <AbstractTextInput  Fonts={'OpenSans'} label={'Guest Name'} placeholderText={'hamza'} />
                                <AbstractTextInput  Fonts={'OpenSans'} label={'Guest Phone Number'} placeholderText={'93888383838'} />
                                <AbstractTextInput  Fonts={'OpenSans'} label={'Guest Email Address'} placeholderText={'123"gmail.com'} />
                            </View>
                        </View>


                        <View style={{ width: '100%', height: 100, justifyContent: 'center' }}>
                            <AbstractButton title={'Proceder a Pagos'} textSize={moderateScale(14, 0.1)} onPress={()=>navigate('DetallesdePago1')} />
                        </View>

                    </ContentContainer>


                </ScrollView>
            </View>
        </ScreenContainer>
    )
}

export default InformaciondeInvitadosScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        position: 'relative'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    }
})
