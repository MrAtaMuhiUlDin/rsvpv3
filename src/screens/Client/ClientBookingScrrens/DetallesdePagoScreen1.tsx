import React, { useState } from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import DetallesdePagoCompo from '../../../components/modules/common/client/ClientBookingGuestScreensCompo/DetallesdePagoCompo'
import CheckBox from '@react-native-community/checkbox';
import { goBack, navigate } from '../../../navigation/authNavigator'







const DetallesdePagoScreen1: React.FC = () => {

    const [toggleCheckBox, setToggleCheckBox] = useState<boolean>(false)


    return (
        <ScreenContainer>
            <FocusAwareStatusBar
                backgroundColor={'#f5f5f6'}
                barStyle={'dark-content'}
            />
            <View style={styles.mainContainer}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ width: '100%', height: moderateScale(435, 0.1) }}>
                        <ImageBackground source={require('../../../assets/images/Rectangle.png')} resizeMode={'stretch'} style={{ width: '100%', height: '100%' }}>
                            <AbstractHeader type={'SimpleBackHeader'} bgColor={'transparent'} onPressBack={()=>goBack()} />
                            <ContentContainer style={{ width: '85%' }} >

                                <View style={{ width: '100%', height: 60, justifyContent: 'center' }}>
                                    <Text style={styles.LargeHeading}>Detalles de Pago</Text>
                                </View>

                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    <DetallesdePagoCompo />
                                </View>



                            </ContentContainer>
                        </ImageBackground>
                    </View>

                    <ContentContainer>
                        <View style={{ width: '100%', height: moderateScale(300, 0.1), marginTop: 60 }}>
                            <View style={{ width: '100%' }}>
                                <Text style={styles.smallHeading}>Guest Details</Text>
                            </View>
                            <View style={{ width: '100%', height: moderateScale(230, 0.1) }}>
                                <AbstractTextInput Fonts={'OpenSans'} label={'Card Holder'} placeholderText={'hamza'} />
                                <AbstractTextInput Fonts={'OpenSans'} label={'Card Number'} placeholderText={'93888383838'} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <View style={{ width: '45%' }}>
                                        <AbstractTextInput Fonts={'OpenSans'} label={'Expiration Date'} placeholderText={'93888383838'} />

                                    </View>
                                    <View style={{ width: '45%' }}>
                                        <AbstractTextInput Fonts={'OpenSans'} label={'CCV'} placeholderText={'93888383838'} />

                                    </View>
                                </View>

                                <View style={{ width: '100%', height: 70, flexDirection: 'row', alignItems: 'center' }}>
                                    
                                    <View style={{ width: '10%', height: '40%',justifyContent:'center' }}>
                                        <CheckBox
                                            tintColors={{ true: '#FF5959', false: 'grey' }}
                                            onCheckColor={'#FF5959'}
                                            disabled={false}
                                            value={toggleCheckBox}
                                            onValueChange={(newValue) => setToggleCheckBox(newValue)}
                                        />
                                    </View>
                                    <View style={{alignItems:'center',height:'26%',justifyContent:'center'}}> 
                                        <Text style={styles.textOne}>
                                            Pagar 50% hoy, y 50% el día antes
                                        </Text>
                                    </View>

                                </View>
                            </View>

                        </View>


                        <View style={{ width: '100%', height: 100, justifyContent: 'center' }}>
                            <AbstractButton title={'Revisar Booking'} textSize={moderateScale(14, 0.1)} onPress={() => navigate('RevisarBooking')} />
                        </View>

                    </ContentContainer>


                </ScrollView>
            </View>
        </ScreenContainer>
    )
}

export default DetallesdePagoScreen1

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        position: 'relative'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textOne: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#868E96'
    }
})
