import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import MasterCardSvg from '../../../assets/icons/PaymentSvgs/MasterCardSvg'
import VisaCardSvg from '../../../assets/icons/PaymentSvgs/VisaCardSvg'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import CVC from '../../../components/modules/common/client/ClientPaymentScreenCompo/CVC'
import Expiry from '../../../components/modules/common/client/ClientPaymentScreenCompo/Expiry'
import PaymentModuleCard from '../../../components/modules/common/client/ClientPaymentScreenCompo/PaymentModuleCard'
import TextInputVisa from '../../../components/modules/common/client/ClientPaymentScreenCompo/TextInputVisa'

interface Props {

}

const ClientPaymentMethodScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar backgroundColor={'white'} barStyle={'dark-content'} />
            <View style={styles.mainContainer}>
                <AbstractHeader type={'SimpleRsvp'} />


                <ScrollView showsVerticalScrollIndicator={false}>

                    <ContentContainer>
                        <View style={{ width: '100%', height: 80, justifyContent: 'center' }}>
                            <Text style={styles.LargeHeading}>Payment Method</Text>
                        </View>

                        <View style={{ width: '100%', height: 170, justifyContent: 'center', alignItems: 'center' }}>
                            <PaymentModuleCard />
                        </View>

                        <View style={{ width: '100%', height: 50 }}>
                            <Text style={styles.smallHeading}>Available Options</Text>

                            <View style={{ flexDirection: 'row', width: '25%', justifyContent: 'space-between', marginTop: 10 }}>
                                <MasterCardSvg />
                                <VisaCardSvg />
                            </View>
                        </View>

                        <View style={{ width: '100%', height: 240, justifyContent: 'space-between', marginTop: 10,marginBottom:10 }}>

                            <AbstractTextInput Fonts={'OpenSans'} label={'Name'} placeholderText={'Card Holder'} />

                            <TextInputVisa />

                            <View style={{ width: '100%', height: 75, flexDirection: 'row',marginTop:-10}}>
                                <View style={{ width: '50%', height: '100%' }}>
                                    <Expiry />
                                </View>

                                <View style={{ width: '50%',height: '100%',justifyContent:'flex-end',alignItems:'flex-end' }}>
                                    <CVC />
                                </View>

                            </View>
                        </View>


                        <View style={{ width: '100%', height: 100, justifyContent: 'center', alignItems: 'center' }}>
                            <AbstractButton title={'CONFIRM AUTO LAYOUT'} textSize={moderateScale(15, 0.1)} />

                        </View>


                    </ContentContainer>
                </ScrollView>

            </View>
        </ScreenContainer>
    )
}

export default ClientPaymentMethodScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F8F8F8'
    },
    LargeHeading: {
        fontSize: moderateScale(16, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'black'

    },
    smallHeading: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'black'
    }
})
