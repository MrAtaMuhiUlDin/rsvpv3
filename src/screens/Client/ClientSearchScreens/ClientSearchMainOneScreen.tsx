import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../components/abstract/contentContainer'
import CelebrationCompo from '../../../components/modules/common/client/ClientSearchScreenCompo/CelebrationCompo'
import ClientSearchHeader from '../../../components/modules/common/client/ClientSearchScreenCompo/ClientSearchHeader'
import YourServicesCard from '../../../components/modules/common/provider/YourServicesCard'
import SearchClientScreen from '../searchClientScreen'
import FilterBtnSpecialnormal from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/FilterButtons/FilterBtnSpecialnormal'
import FilterBtnSpecialDate from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/FilterButtons/FilterBtnSpecialDate'
import { navigate } from '../../../navigation/authNavigator'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'




const ClientSearchMainOneScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
        <View style={styles.mainContainer}>
            <ClientSearchHeader
                icon1={<FilterBtnSpecialnormal text={'Espacios'} />}
                icon2={<FilterBtnSpecialnormal text={'10:30 AM'} />}
                icon3={<FilterBtnSpecialDate text={'Abril 22'} />}
            />
            <ContentContainer style={{ width: '90%' }}>


                <View style={styles.headingView}>
                    <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(12, 0.1), fontWeight: '600', color: 'black' }}>Resultados: Yates</Text>
                </View>

                </ContentContainer>
              
                    <ScrollView showsVerticalScrollIndicator={false} style={{width:'90%',alignSelf:'center'}} >

                        <YourServicesCard onPress={() => navigate('SupplierMain')} tag tagtext={'YATES'} title={'Luxury Yacht Rental '} imageUrl={require('../../../assets/images/yacht.png')} />
                        <YourServicesCard onPress={() => navigate('SupplierMain')} tag tagtext={'CATERING'} title={'Chef Privado'} imageUrl={require('../../../assets/images/chef.png')} />


                    </ScrollView>

                    
               

         
        </View>
        </ScreenContainer>
    )
}

export default ClientSearchMainOneScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F8F8F8'
    },
    viewOne: {
        width: '100%',
        // backgroundColor: 'red',
        height: 1000,
        // marginBottom:200

    },
    headingView: {
        width: '100%',
        height: moderateScale(20, 0.1),
        //  backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'center'
    }
})
