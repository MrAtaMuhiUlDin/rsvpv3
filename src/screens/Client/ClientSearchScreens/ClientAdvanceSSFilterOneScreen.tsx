import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import CelebrationTypes from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/CelebrationTypes'
import Dropdown from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/Dropdown'
import HeaderAdvanceFilter from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/HeaderAdvanceFilter'
import HeaderAdvanceSearch from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/HeaderAdvanceSearch'
import CelebrationCompo from '../../../components/modules/common/client/ClientSearchScreenCompo/CelebrationCompo'
import ClientSearchHeader from '../../../components/modules/common/client/ClientSearchScreenCompo/ClientSearchHeader'
import { goBack } from '../../../navigation/authNavigator'
import SearchClientScreen from '../searchClientScreen'


const data = [
    'Fiestas de Cumpleaños',
    'Celebraciones ',
    'Fiestas Infantiles',
    'Despedida de Solteros y Solteras',
    'Baby Showers',
    'Aniversarios ',
    'Gender Reveal',
    'Salidas Grupales',
]





const ClientAdvanceSSFilterOneScreen: React.FC = () => {

    return (
        <ScreenContainer>
            <FocusAwareStatusBar
            backgroundColor={'#F6F7F9'}
            barStyle={'dark-content'}
            />
        <View style={styles.mainContainer}>
            <ContentContainer style={{ width: '90%' }}>
             <HeaderAdvanceFilter onCancel={()=>goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            
             <View style={styles.headingViewLarge}>
               <Text style={styles.headingLarge}>
                   Celebration Type
               </Text>
             </View>

             <View style={{width:'100%',height:500,marginTop:20}}>

                  {data.map((val,index)=>{
                      return <CelebrationTypes text={val} key={index} />
                  })}
             </View>
          


             </ScrollView>
            </ContentContainer>
        </View>
        </ScreenContainer>
    )
}

export default ClientAdvanceSSFilterOneScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F6F7F9'
    },
    viewOne: {
        width: '100%',
        height:250,
        // backgroundColor:'yellow',
        justifyContent:'space-between',
        marginTop:35
         
    },
    headingViewLarge:{
        width: '100%', 
        height: moderateScale(48, 0.1),
        //  backgroundColor: 'green',
          marginVertical: 10,
           justifyContent: 'center' 
    },
    headingLarge:{
        fontFamily:'OpenSans',
        fontWeight:'400',
        fontSize:moderateScale(36,0.1),
        color:'#273D52'
    },
    headingViewSmall:{
        width: '100%', 
        height: moderateScale(20, 0.1),
         backgroundColor: 'green',
          marginVertical: 10,
           justifyContent: 'center' 
    },
    headingSmall:{
        fontFamily: 'OpenSans', fontSize: moderateScale(11, 0.1), fontWeight: '700', color: '#273D52'
    }
})
