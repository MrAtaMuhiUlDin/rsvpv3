import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import Dropdown from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/Dropdown'
import FilterBars from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/FilterBars'
import HeaderAdvanceSearch from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/HeaderAdvanceSearch'
import PriceRange from '../../../components/modules/common/client/ClientAdvancedSearcScreenCompo/PriceRange'
import CelebrationCompo from '../../../components/modules/common/client/ClientSearchScreenCompo/CelebrationCompo'
import ClientSearchHeader from '../../../components/modules/common/client/ClientSearchScreenCompo/ClientSearchHeader'
import SearchClientScreen from '../searchClientScreen'
import { navigate, goBack } from '../../../navigation/authNavigator'
import CalendarStrip from 'react-native-calendar-strip';
import MultiSlider from '@ptomasroos/react-native-multi-slider'
import TimeBtnWithDownArrow from '../../../components/modules/common/client/TimeBtnWithDownArrow'
import { Calendar, CalendarList, Agenda } from 'react-native-calendars';
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet'
import ArrowDownSVG from '../../../assets/icons/ArrowDownSVG'
import LeftSvg from '../../../assets/icons/CalendarSvgs/LeftSvg'
import RightSvg from '../../../assets/icons/CalendarSvgs/RightSvg'




const HeaderCompoCustom = () => {
    return (

        <View style={{ width: '85%',alignSelf:'center' ,height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderColor: '#E0E0E1' }}>
            <TouchableOpacity>
                <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600' }}>Cancel</Text>
            </TouchableOpacity>

            <TouchableOpacity>
                <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600', color: '#EF4339' }}>Done</Text>
            </TouchableOpacity>
        </View>

    )
}



const ClientAdvanceSearchScreen: React.FC = () => {

    const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }
    return (
        <ScreenContainer>
            <FocusAwareStatusBar
                barStyle="dark-content"
                backgroundColor={'#F6F7F9'}
            />
            <View style={styles.mainContainer}>
                <ContentContainer style={{ width: '90%' }}>
                    <HeaderAdvanceSearch onCancel={() => goBack()} />
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={styles.headingViewLarge}>
                            <Text style={styles.headingLarge}>
                                Advanced Search
                            </Text>
                        </View>

                        <View style={styles.viewOne}>

                            <FilterBars label={'Celebration Type'} onPress={() => navigate('CelebrationType')} />
                            <FilterBars label={'Plan Types'} onPress={() => navigate('PlanTypes')} />
                            <FilterBars label={'Amenities'} onPress={() => navigate('Amenities')} />


                        </View>

                        {/* dateandTime */}
                        <View style={{ width: '100%', height: 170, marginTop: 20, }}>
                            <View style={styles.headingViewSmall}>
                                <Text style={styles.headingSmall}>Date & Time</Text>
                            </View>
                            <View style={{ width: '100%', height: moderateScale(117, 0.1), backgroundColor: 'white' }}>
                                <View style={styles.viewTwo}>
                                    <View style={[styles.viewThree]}>
                                        <Text style={styles.textOne}>September 2018</Text>
                                        <View style={{ paddingRight: 10 }}>
                                            <ArrowDownSVG color={'#A9B1BA'} />
                                        </View>
                                    </View>
                                </View>


                                <CalendarStrip
                                    scrollable
                                    style={{ height: moderateScale(83, 0.1) }}
                                    calendarColor={'white'}
                                    calendarHeaderStyle={{ color: 'black' }}

                                    dateNumberStyle={{ color: '#273D52' }}
                                    dateNameStyle={{ color: '#273D52' }}
                                    iconContainer={{ flex: 0 }}
                                    iconRight={require('../../../assets/icons/ArrowDownSVG')}
                                    iconLeft={require('../../../assets/icons/ArrowDownSVG')}
                                    calendarHeaderContainerStyle={{
                                        // backgroundColor: 'blue',
                                        width: '100%',
                                        height: 0,
                                        alignItems: 'center',
                                        justifyContent: 'space-between'
                                    }}

                                />
                            </View>
                        </View>


                        {/* timeRange */}
                        <View style={{ width: '100%', height: 70, marginTop: 20, flexDirection: 'row' }}>
                            <View style={{ width: '30%', height: '100%', justifyContent: 'center' }}>
                                <Text style={styles.headingSmall}>Time Range</Text>
                            </View>
                            <View style={{ width: '70%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>

                                <View style={{ width: '70%', height: '80%', alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' }}>

                                    <View style={{marginHorizontal:5}}>
                                    <TimeBtnWithDownArrow onPress={toggleModal} />
                                    </View>

                                    <View style={{marginHorizontal:5}}>
                                    <TimeBtnWithDownArrow hide />
                                    </View>


                                </View>
                            </View>

                        </View>


                        {/* PriceRang */}
                        <View style={{ width: '100%', height: 100, marginTop: 20, }}>
                            <PriceRange />
                        </View>

                        <View style={{ width: '100%', height: 70 }}>

                        </View>


                        <AbstractBottomSheet onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom />} >
                            <View style={{ width: '85%', height: 310, backgroundColor: 'red', alignSelf: 'center' }}>
                                <Calendar
                                    renderArrow={(direction) => direction === 'left' ? (<LeftSvg />) : (<RightSvg />)}
                                    markedDates={{
                                        '2021-10-09': { selected: true, marked: true, selectedColor: '#EF4339' },
                                    }}
                                    theme={{
                                        arrowColor: '#EF4339',
                                        textSectionTitleColor: '#EF4339',
                                    }}
                                    current={'2021-10-09'}
                                    enableSwipeMonths={true}
                                />
                            </View>
                        </AbstractBottomSheet>



                    </ScrollView>
                </ContentContainer>
            </View>
        </ScreenContainer>
    )
}

export default ClientAdvanceSearchScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F6F7F9'
    },
    viewOne: {
        width: '100%',
        height: 250,
        // backgroundColor:'yellow',
        justifyContent: 'space-between',
        marginTop: 35

    },
    headingViewLarge: {
        width: '100%',
        height: moderateScale(31, 0.1),
        //  backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'center'
    },
    headingLarge: {
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(22, 0.1),
        color: '#273D52'
    },
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'space-between'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        color: '#273D52'
    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#273D52'
    },
    viewTwo: {
        width: '100%',
        height: moderateScale(40, 0.1),
        // backgroundColor:'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: '85%',
        height: '80%',
        // backgroundColor:'green',
        justifyContent: 'space-between'
        , alignItems: 'flex-end',
        flexDirection: 'row'
    }
})
