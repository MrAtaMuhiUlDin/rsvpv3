import React from 'react'
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../../components/abstract/abstractButton'
import ContentContainer from '../../../../components/abstract/contentContainer'
import GalleryModuleSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/GalleryModuleSupplier'
import AmenitiesSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/AmenitiesSupplier'
import NuestroMenuButton from '../../../../components/modules/common/client/ClientSupllierScreenCompo/NuestroMenuButton'
import MapSupplierScreen from '../../../../components/modules/common/client/ClientSupllierScreenCompo/MapSupplierScreen'
import CardAddedBtn from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardAddedBtn'
import CardSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardSupplier'
import CardExtraServicesSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardExtraServicesSupplier'


const OverViewCompo:React.FC = () => {
    return (
        <>
        <ContentContainer>
                    <View style={styles.viewSeven}>
                        <AbstractButton title={'VIEW PRICES'} textSize={moderateScale(14, 0.1)} height={moderateScale(45, 0.1)} />
                    </View>
        </ContentContainer>

             
           
                    <View style={{ width: '100%', height: 150, }}>
                    <ContentContainer>
                        <View style={styles.headingViewSmall}>
                            <Text style={styles.headingSmall}>
                                Galeria
                            </Text>
                        </View>
                    </ContentContainer>

                        <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                            <GalleryModuleSupplier imgUrl={require('../../../../assets/images/image3.png')} />
                            <GalleryModuleSupplier imgUrl={require('../../../../assets/images/image3.png')} />
                            <GalleryModuleSupplier imgUrl={require('../../../../assets/images/image3.png')} />
                            <GalleryModuleSupplier imgUrl={require('../../../../assets/images/image3.png')} />
                            <GalleryModuleSupplier imgUrl={require('../../../../assets/images/image3.png')} />
                        </ScrollView>

                    </View>
                 
                    

             <ContentContainer>
                    <View style={{ width: '100%', marginTop: 10 }}>
                        <View style={styles.headingViewSmall}>
                            <Text style={styles.headingSmall}>
                                Description
                            </Text>
                        </View>

                        <Text numberOfLines={7} style={{ fontFamily: 'OpenSans', fontWeight: '400', color: 'rgba(50, 54, 67, 0.5)' }}>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum tempora voluptates amet excepturi expedita ratione corrupti! Veniam soluta facere neque totam omnis! Libero quisquam omnis minima pariatur magni, neque amet!
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi nobis non vitae! Reiciendis non rem laboriosam consequatur corrupti a illo officiis! Nisi nobis provident vel error maxime porro optio ratione.
                        </Text>
                    </View>


                    <View style={{ width: '100%', height: 150, marginTop: 20 }}>
                        <View style={styles.headingViewSmall}>
                            <Text style={styles.headingSmall}>
                                Amenities
                            </Text>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <AmenitiesSupplier />
                        </View>
                    </View>


                    <View style={{ width: '100%', alignItems: 'center', marginVertical: 10 }}>
                        <NuestroMenuButton />
                    </View>


                    <View style={{ width: '100%', height: 350, marginTop: 10 }}>
                        <View style={styles.headingViewSmall}>
                            <Text style={styles.headingSmall}>
                                Ubicacion
                            </Text>
                        </View>

                        <View>
                            <MapSupplierScreen />
                        </View>

                    </View>



                    <View style={{ width: '100%', marginTop: 10 }}>
                        <View style={styles.headingViewSmall}>
                            <Text style={styles.headingSmall}>
                                Ubicacion
                            </Text>
                        </View>

                        <ScrollView showsVerticalScrollIndicator={false}>

                            <CardSupplier />

                            <CardExtraServicesSupplier />


                        </ScrollView>

                    </View>

                </ContentContainer>
                </>
    )
}

export default OverViewCompo

const styles = StyleSheet.create({
    viewSeven: {
        width: '100%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10
    },
    textOne: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textTwo: {
        fontSize: moderateScale(7.5, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50,54,67,0.5)'
    },
    textFive: {
        width: '30%',
        height: '100%',
        flexDirection: 'row'
    },
    textSix: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.25)'
    },
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'center'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    }
})
