import React from 'react'
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../../components/abstract/abstractButton'
import ContentContainer from '../../../../components/abstract/contentContainer'
import CardExtraServicesSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardExtraServicesSupplier'
import CardSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardSupplier'
import OtherModuleSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/OtherModuleSupplier'
import ReviewSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/ReviewSupplier'

const OthersCompo: React.FC = () => {
    return (
        <View>
            <ContentContainer>
                <View style={{width:'100%'}}>

                  <ScrollView>
                       <OtherModuleSupplier />
                       <OtherModuleSupplier />
                       <OtherModuleSupplier />
                       <OtherModuleSupplier />
                  </ScrollView>

                </View>
            </ContentContainer>

        </View>
    )
}

export default OthersCompo

const styles = StyleSheet.create({
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    },
    viewOne: {
        width: '100%', height: moderateScale(85, 0.1), backgroundColor: 'white', marginTop: 50, flexDirection: 'row'
    },
    viewTwo: {
        width: '25%', height: '100%', justifyContent: 'center', alignItems: 'center'
    },
    viewThree: {
        width: '75%', height: '100%', justifyContent: 'center', alignItems: 'center'
    }

})
