import React from 'react'
import { ScrollView, StyleSheet, Text, View, Image, FlatList } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../../components/abstract/contentContainer'
import GalleryModuleSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/GalleryModuleSupplier'
import AutoHeightImage from 'react-native-auto-height-image';



const PicsData = [
    {
        id: 0,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
    {
        id: 1,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
    {
        id: 2,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
    {
        id: 3,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
    {
        id: 4,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },
    {
        id: 5,
        imgUrl: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    },

]






const GalleryCompo: React.FC = () => {
    return (
        <View>
            <ContentContainer>

                <View style={{ width: '100%', height: 40, justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View style={styles.headingViewSmall}>
                        <Text style={styles.headingSmall}>
                            Galeria
                        </Text>
                        <Text style={{ fontSize: moderateScale(11, 0.1), fontWeight: '400', fontFamily: 'OpenSans' }}>
                            34 Pictures
                        </Text>
                    </View>
                </View>
            </ContentContainer>

            <View style={{ width: '100%', marginTop: 10 }}>

                
                <ScrollView showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>
                <View style={{ height: 170,flexDirection: 'row', width: '90%', alignSelf: 'center' }} >
                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />

                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />
                </View>

                <View style={{ height: 170,flexDirection: 'row', width: '90%', alignSelf: 'center' }} >
                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />

                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />
                </View>



                <View style={{ height: 170,flexDirection: 'row', width: '90%', alignSelf: 'center' }} >
                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />

                    <AutoHeightImage
                        width={165}
                        height={150}
                        style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                        source={{ uri: 'https://images.pexels.com/photos/813011/pexels-photo-813011.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500' }}
                    />
                </View>





                     </ScrollView>









                {/* <ScrollView  nestedScrollEnabled={true} style={{width:'85%',backgroundColor:'green',alignSelf:'center',flexDirection:'row'}} showsVerticalScrollIndicator={false}>
                
                    {PicsData.map((value,index)=>{
                        return (
                            <AutoHeightImage
                            key={index}
                            width={165}
                            height={150}
                            style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                            source={{ uri: value.imgUrl }}
                        />
                    
                        )
                    })}

                </ScrollView> */}






                {/* <FlatList
                    data={PicsData}
                    style={{ width: '85%', alignSelf: 'center' }}
                    showsVerticalScrollIndicator={false}
                    nestedScrollEnabled={true}
                    numColumns={2}
                    keyExtractor={(item: any) => item.id}
                    renderItem={({ item }) => {
                        return (
                            <View>
                                <AutoHeightImage
                                    width={165}
                                    height={150}
                                    style={{ borderRadius: 10, marginVertical: 10, marginHorizontal: 5 }}
                                    source={{ uri: item.imgUrl }}
                                />
                            </View>
                        )
                    }}
                /> */}

            </View>

        </View>
    )
}

export default GalleryCompo

const styles = StyleSheet.create({
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    }
})
