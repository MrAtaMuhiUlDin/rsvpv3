import React from 'react'
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../../components/abstract/abstractButton'
import ContentContainer from '../../../../components/abstract/contentContainer'
import CardExtraServicesSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardExtraServicesSupplier'
import CardSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardSupplier'
import ReviewSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/ReviewSupplier'

const ReviewCompo: React.FC = () => {
    return (
        <View>
            <ContentContainer>
                <View style={{ width: '100%', height: moderateScale(294, 0.1), marginTop: 25 }}>
                    <ReviewSupplier />
                </View>
            </ContentContainer>
            <View >
            </View>
            <ContentContainer>
                <View style={styles.viewOne}>

                    <View style={styles.viewTwo}>
                        <Text style={{ fontSize: moderateScale(18, 0.1), fontWeight: '600', fontFamily: 'OpenSans' }}>
                            $950
                        </Text>
                    </View>
                    <View style={styles.viewThree}>
                        <AbstractButton
                            title={'Continuar'}
                            width={moderateScale(240, 0.1)}
                            textSize={moderateScale(15, 0.1)}
                            height={moderateScale(45, 0.1)} />
                    </View>
                </View>
            </ContentContainer>

        </View>
    )
}

export default ReviewCompo

const styles = StyleSheet.create({
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    },
    viewOne: {
        width: '100%', height: moderateScale(85, 0.1), backgroundColor: 'white', marginTop: 50, flexDirection: 'row'
    },
    viewTwo: {
        width: '25%', height: '100%', justifyContent: 'center', alignItems: 'center'
    },
    viewThree: {
        width: '75%', height: '100%', justifyContent: 'center', alignItems: 'center'
    }

})
