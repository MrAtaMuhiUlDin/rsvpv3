import React from 'react'
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../../components/abstract/contentContainer'
import CardExtraServicesSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardExtraServicesSupplier'
import CardSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/CardSupplier'
import { navigate } from '../../../../navigation/authNavigator'

const PreciosCompo: React.FC = () => {
    return (
        <View>
            <ContentContainer>

                <View style={{ width: '100%', height: 40, justifyContent: 'space-between', flexDirection: 'row' }}>
                    <View style={styles.headingViewSmall}>
                        <Text style={styles.headingSmall}>
                            Precios
                        </Text>
                    </View>
                </View>
            </ContentContainer>

            <View style={styles.viewOne}>
                <View style={styles.viewTwo} />
                <Text style={styles.textOne}>
                    Yates de Lujo
                </Text>
            </View>

            <ContentContainer>

                <ScrollView showsVerticalScrollIndicator={false}>

                    <CardSupplier />
                    <CardSupplier />
                    <CardExtraServicesSupplier onPressNavi={()=>navigate('Informacionde')} />

                </ScrollView>

            </ContentContainer>

        </View>
    )
}

export default PreciosCompo

const styles = StyleSheet.create({
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    },
    viewOne: {
        width: '100%',
        height: 30,
        position: 'relative',
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center'
    },
    viewTwo: {
        width: 22,
        height: 22,
        borderRadius: 11,
        backgroundColor: '#FF5959',
        position: 'absolute',
        left: -10
    },
    textOne: {
        color: '#FF5959',
        fontWeight: '600',
        fontSize: moderateScale(14, 0.1),
        paddingLeft: 32
    }
})
