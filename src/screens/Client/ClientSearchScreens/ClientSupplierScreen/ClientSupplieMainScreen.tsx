import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../../components/abstract/contentContainer'
import HeaderSupplierScreen from '../../../../components/modules/common/client/ClientSupllierScreenCompo/HeaderSupplierScreen'
import StarRating from '../../../../components/modules/common/provider/StarRating'
import AbstractButton from '../../../../components/abstract/abstractButton'
import TouchableBtnsSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/TouchableBtnsSupplier'
import OverViewCompo from './OverViewCompo'
import GalleryCompo from './GalleryCompo'
import PreciosCompo from './PreciosCompo'
import ReviewCompo from './ReviewCompo'
import OthersCompo from './OthersCompo'
import ReviewSupplier from '../../../../components/modules/common/client/ClientSupllierScreenCompo/ReviewSupplier'
import ScreenContainer from '../../../../components/abstract/abstractScreenContainer'
import FocusAwareStatusBar from '../../../../components/abstract/statusbarConfiguration'
import { goBack } from '../../../../navigation/authNavigator'


interface Props {
    type?: string
}



const SwitchingBetweenFunction = ({ type }: Props) => {
    switch (type) {
        case 'Overview':
            return <OverViewCompo />
            break;
        case 'Galeria':
            return <GalleryCompo />
            break;
        case 'Precios':
            return <PreciosCompo />
            break;
        case 'Reviews':
            return <ReviewCompo />
            break;
        case 'Others':
            return <OthersCompo />
        default: return <OverViewCompo />
    }
}


const ClientSupplieMainScreen: React.FC = () => {

    const [nameOfScreen, setnameOfScreen] = useState<string>('Overview')
    const [overViewActive, setoverViewActive] = useState<boolean>(true)
    const [PreciosActive, setPreciosActive] = useState<boolean>(false)
    const [ReviewsActive, setReviewsActive] = useState<boolean>(false)
    const [OthersActive, setOthersActive] = useState<boolean>(false)
    const [GaleriaActive, setGaleriaActive] = useState<boolean>(false)



    return (
        <ScreenContainer>
            <FocusAwareStatusBar 
               barStyle="dark-content"
               translucent={true}
               backgroundColor={'transparent'}
            />
        <View style={styles.mainContainer}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <HeaderSupplierScreen  onPress={()=>goBack()} />

                <ContentContainer style={{ width: '85%' }}>

                    <View style={styles.viewOne}>

                        <View style={styles.headingView}>
                            <Text style={styles.headingStyle}>
                                Luxury Yacht Service
                            </Text>
                        </View>

                        <View style={styles.viewTwo}>
                            <View style={styles.viewThree}>
                                <View style={styles.viewFour}>
                                    <View style={{ width: moderateScale(24, 0.1), height: moderateScale(24, 0.1) }}>
                                        <Image source={require('../../../../assets/images/logo.png')} style={{ width: '100%', height: '100%' }} />
                                    </View>
                                </View>
                                <View style={{ width: '90%', paddingLeft: 8, justifyContent: 'center' }}>
                                    <Text style={styles.textOne}>Company</Text>
                                    <Text style={styles.textTwo}>San Cristobal, Panama  </Text>
                                </View>
                            </View>
                        </View>


                        <View style={styles.viewFive}>
                            <View style={styles.textFive}>
                                <View style={{ width: '65%', justifyContent: 'center', alignItems: 'center' }}>
                                    <StarRating rating={5} color={'#FF7373'} />
                                </View>
                                <View style={{ width: '100%',justifyContent: 'center', alignItems: 'flex-start' }}>
                                    <Text style={styles.textSix}>243 Reviews</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </ContentContainer>


                <View style={styles.viewSix}>
                    <ScrollView horizontal showsHorizontalScrollIndicator={false} >
                        <TouchableBtnsSupplier text={'Overview'} active={overViewActive}
                            onPress={() => {
                                setnameOfScreen('Overview')
                                setoverViewActive(true)
                                setGaleriaActive(false)
                                setPreciosActive(false)
                                setReviewsActive(false)
                                setOthersActive(false)
                            }}
                        />
                        <TouchableBtnsSupplier text={'Galeria'}
                            active={GaleriaActive}
                            onPress={() => {
                                setnameOfScreen('Galeria')
                                setoverViewActive(false)
                                setGaleriaActive(true)
                                setPreciosActive(false)
                                setReviewsActive(false)
                                setOthersActive(false)
                            }}
                        />
                        <TouchableBtnsSupplier text={'Precios'}
                            active={PreciosActive}
                            onPress={() => {
                                setnameOfScreen('Precios')
                                setoverViewActive(false)
                                setGaleriaActive(false)
                                setPreciosActive(true)
                                setReviewsActive(false)
                                setOthersActive(false)
                            }}
                        />
                        <TouchableBtnsSupplier text={'Reviews'}
                            active={ReviewsActive}
                            onPress={() => {
                                setnameOfScreen('Reviews')
                                setoverViewActive(false)
                                setGaleriaActive(false)
                                setPreciosActive(false)
                                setReviewsActive(true)
                                setOthersActive(false)
                            }}
                        />
                        <TouchableBtnsSupplier text={'Others'}
                            active={OthersActive}
                            onPress={() => {
                                setnameOfScreen('Others')
                                setoverViewActive(false)
                                setGaleriaActive(false)
                                setPreciosActive(false)
                                setReviewsActive(false)
                                setOthersActive(true)
                            }}
                        />
                    </ScrollView>
                </View>

                <SwitchingBetweenFunction type={nameOfScreen} />

            </ScrollView>
        </View>
        </ScreenContainer>
    )
}

export default ClientSupplieMainScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    viewOne: {
        width: '100%',
        // backgroundColor: 'red',
        height: 120,
    },
    headingView: {
        width: '100%',
        height: 30,
        // backgroundColor:'orange',
        justifyContent: 'center',
        marginVertical: 10
    },
    headingStyle: {
        fontSize: moderateScale(18, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'black'
    },
    viewTwo: {
        width: '100%',
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        // backgroundColor:'grey'
    },
    viewFour: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFive: {
        width: '100%',
        height: 25,
        // backgroundColor:'green',
        marginTop: 5,
        justifyContent: 'center'
    },
    viewSix: {
        width: '100%',
        height: 30,
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: 5
    },
    viewSeven: {
        width: '100%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 10
    },
    textOne: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textTwo: {
        fontSize: moderateScale(7.5, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50,54,67,0.5)'
    },
    textFive: {
        width: '30%',
        height: '100%',
        flexDirection: 'row'
    },
    textSix: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.25)'
    },
    headingViewSmall: {
        width: '100%',
        height: moderateScale(20, 0.1),
        // backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'center'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#323643'
    }

})
