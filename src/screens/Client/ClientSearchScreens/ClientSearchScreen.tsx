import React, { useState } from 'react'
import { FlatList, ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import BookMarkBottomSheet from '../../../components/modules/common/client/BottomSheetsClient/BookMarkBottomSheet'
import CelebrationCompo from '../../../components/modules/common/client/ClientSearchScreenCompo/CelebrationCompo'
import ClientSearchHeader from '../../../components/modules/common/client/ClientSearchScreenCompo/ClientSearchHeader'
import { navigate } from '../../../navigation/authNavigator'
import SearchClientScreen from '../searchClientScreen'



const Data = [
    {
        id: 1,
        imgUrl: require('../../../assets/images/cheers.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 2,
        imgUrl: require('../../../assets/images/baloons.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 3,
        imgUrl: require('../../../assets/images/cheers.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 4,
        imgUrl: require('../../../assets/images/cheers.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 5,
        imgUrl: require('../../../assets/images/baloons.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 6,
        imgUrl: require('../../../assets/images/cheers.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 7,
        imgUrl: require('../../../assets/images/baloons.png'),
        text: 'Celebrating Among Friends'
    },
    {
        id: 8,
        imgUrl: require('../../../assets/images/cheers.png'),
        text: 'Celebrating Among Friends'
    },

]



const HeaderCompoCustom = () => {
    return (
        <></>
    )
}


const ClientSearchScreen: React.FC = () => {
    const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }
    return (
        <ScreenContainer>
            <FocusAwareStatusBar
                barStyle="dark-content"
                backgroundColor={'white'}
            />
            <View style={styles.mainContainer}>
                <ClientSearchHeader onPressBookmark={toggleModal} />
                <ContentContainer style={{ width: '90%' }}>


                    <View style={styles.headingView}>
                        <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600', color: 'black' }}>Tipo de Clebracion</Text>
                    </View>

                    </ContentContainer>

                  
                    <FlatList
                        style={{alignSelf:'center',width:'91.5%'}}
                        data={Data}
                        showsVerticalScrollIndicator={false}
                        numColumns={2}
                        keyExtractor={(item: any) => item.id}
                        renderItem={({ item }) => {
                            return (
                                <View>
                                    <CelebrationCompo imgUrl={item.imgUrl} text={item.text} onPress={() => navigate('SearchMainOne')} />
                                </View>
                            )
                        }}
                  />
                 
             
               

                <AbstractBottomSheet onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom />} >
                    <BookMarkBottomSheet />
                </AbstractBottomSheet>

            </View>
        </ScreenContainer>
    )
}

export default ClientSearchScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F8F8F8'
    },
    viewOne: {
        width: '100%',


    },
    headingView: {
        width: '100%',
        height: moderateScale(20, 0.1),
        //  backgroundColor: 'green',
        marginVertical: 10,
        justifyContent: 'center'
    }
})
