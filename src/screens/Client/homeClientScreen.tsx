import React from 'react';
import { View, Text, ScrollView, StyleSheet, TouchableOpacity, StatusBar } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import Event1SVG from '../../assets/icons/ProviderSVG/Event1SVG';
import Event2Svg from '../../assets/icons/ProviderSVG/Event2Svg';
import ContentContainer from '../../components/abstract/contentContainer';
import PlacesToCelebrate from '../../components/Composite/PlacesToCelebrate';
import EventModule from '../../components/modules/common/client/EventModule';
import EventsDismisserModule from '../../components/modules/common/client/EventsDismisserModule';
import LiveExperienceModule from '../../components/modules/common/client/LiveExperienceModule';
import YourServicesCard from '../../components/modules/common/provider/YourServicesCard';
import Ionicons from 'react-native-vector-icons/Ionicons';
import CollectionCard from '../../components/modules/common/client/CollectionCard';


const HomeClientScreen: React.FC = props => {
  return (
    <View style={styles.mainContainer}>
      <StatusBar
        backgroundColor={'white'}
        barStyle="dark-content"
      />
      <ScrollView showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>

        <View style={styles.viewOne}>
          <ContentContainer style={{ width: '95%' }}>
            <View style={styles.viewTwo}>

              <View style={styles.viewThree}>
                <Text style={styles.textOne}>Live The Experience With Us</Text>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.viewFour}>
                  <Text style={styles.textTwo}>View All</Text>
                </TouchableOpacity>
              </View>

            </View>
          </ContentContainer>


          <ScrollView horizontal nestedScrollEnabled={true} showsHorizontalScrollIndicator={false}>
            <LiveExperienceModule imgUrl={require('../../assets/images/cheers.png')} text={'Celebrating Among Friends'} />
            <LiveExperienceModule imgUrl={require('../../assets/images/baloons.png')} text={'Birthday Parties'} />
            <LiveExperienceModule imgUrl={require('../../assets/images/cheers.png')} text={'Weddings'} />
          </ScrollView>
        </View>


        <ContentContainer style={{ width: '95%' }}>

          <View style={{ width: '100%', marginTop: 15 }}>
            <EventsDismisserModule />
          </View>



          <View style={styles.viewFive}>
            <View style={styles.viewSix}>
              <Event2Svg />
              <Text style={styles.textThree}>Events</Text>
            </View>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.viewSeven}>
              <Ionicons name={'add'} color={'white'} size={15} />
            </TouchableOpacity>
          </View>

          <EventModule type={'editOrDel'} />

          <View style={styles.viewEight}>
            <Text style={styles.textFour}>Collection Title</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.viewNine}>
              <Text style={styles.textFive}>See All (3)</Text>
            </TouchableOpacity>
          </View>

          <View style={{ width: '100%', height: moderateScale(208, 0.1) }}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false} >
              <CollectionCard imageUrl={require('../../assets/images/image3.png')} title={'Isla Contadora'} />
              <CollectionCard imageUrl={require('../../assets/images/image3.png')} title={'Isla Contadora'} />

            </ScrollView>
          </View>

        </ContentContainer>

        <View style={styles.viewTen}>
          <Text style={styles.textSix}>Places to Celebrate</Text>
          <PlacesToCelebrate />
        </View>




        <ContentContainer style={{ width: '95%' }}>
          <View style={styles.viewEleven}>
            <Text style={styles.textSeven}>Collection Title</Text>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.viewTwelve}>
              <Text style={styles.textEight}>See All (3)</Text>
            </TouchableOpacity>
          </View>

          <View style={{ width: '100%', height: moderateScale(208, 0.1), marginBottom: 20 }}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false} >
              <CollectionCard imageUrl={require('../../assets/images/image3.png')} title={'Isla Contadora'} />
              <CollectionCard imageUrl={require('../../assets/images/image3.png')} title={'Isla Contadora'} />
            </ScrollView>
          </View>

        </ContentContainer>




      </ScrollView>
    </View>
  );
};
export default HomeClientScreen;




const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#F8F8F8'
  },
  viewOne: {
    width: '100%',
    height: moderateScale(207, 0.1),
    backgroundColor: 'white',
    marginTop: 10
  },
  viewTwo: {
    width: '100%',
    height: moderateScale(60, 0.1),
    justifyContent: 'center'
  },
  viewThree: {
    width: '100%',
    height: moderateScale(20, 0.1),
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  viewFour: {
    width: moderateScale(65, 0.1),
    height: '100%',
    backgroundColor: 'rgba(255, 99, 89, 0.1)',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewFive: {
    width: '100%',
    height: moderateScale(30, 0.1),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    marginBottom: 20
  },
  viewSix: {
    width: '50%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  viewSeven: {
    height: moderateScale(20, 0.1),
    zIndex: 1,
    width: moderateScale(20, 0.1),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#FF5959'
  },
  viewEight: {
    width: '100%',
    height: moderateScale(20, 0.1),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20
  },
  viewNine: {
    width: moderateScale(65, 0.1),
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewTen: {
    width: '100%',
    height: moderateScale(266, 0.1),
    backgroundColor: 'white',
    marginTop: 24
  },
  viewEleven: {
    width: '100%',
    height: moderateScale(20, 0.1),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 50
  },
  viewTwelve: {
    width: moderateScale(65, 0.1),
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  textOne: {
    fontFamily: 'OpenSans',
    fontSize: moderateScale(14, 0.1),
    fontWeight: '400',
    color: '#FF5959'

  },
  textTwo: {
    fontSize: moderateScale(10, 0.1),
    color: '#FF5959',
    fontWeight: '700',
    fontFamily: 'OpenSans'
  },
  textThree: {
    fontFamily: 'OpenSans',
    fontSize: moderateScale(17, 0.1),
    fontWeight: '600',
    color: 'black',
    paddingLeft: 10
  },
  textFour: {
    fontFamily: 'OpenSans',
    fontSize: moderateScale(14, 0.1),
    fontWeight: '600',
    color: 'black'
  },
  textFive: {
    fontSize: moderateScale(11, 0.1),
    color: '#273D52',
    fontWeight: '700',
    fontFamily: 'OpenSans'
  },
  textSix: {
    fontFamily: 'OpenSans',
    fontSize: moderateScale(14, 0.1),
    fontWeight: '400',
    color: '#FF5959',
    paddingLeft: 15,
    height: 40,
    paddingTop: 15
  },
  textSeven: {
    fontFamily: 'OpenSans',
    fontSize: moderateScale(14, 0.1),
    fontWeight: '600',
    color: 'black'
  },
  textEight: {
    fontSize: moderateScale(11, 0.1),
    color: '#273D52',
    fontWeight: '700',
    fontFamily: 'OpenSans'
  },
})