import React from 'react';
import { View, Text, StyleSheet, ScrollView, Clipboard } from 'react-native';
import ContentContainer from '../../components/abstract/contentContainer';
import AbstractSearchBarCompo from '../../components/abstract/SearchCompo/AbstractSearchBarCompo';
import CelebrationTypes from '../../components/modules/common/client/ClientAdvancedSearcScreenCompo/CelebrationTypes';
import Dropdown from '../../components/modules/common/client/ClientAdvancedSearcScreenCompo/Dropdown';
import HeaderAdvanceFilter from '../../components/modules/common/client/ClientAdvancedSearcScreenCompo/HeaderAdvanceFilter';
import HeaderAdvanceSearch from '../../components/modules/common/client/ClientAdvancedSearcScreenCompo/HeaderAdvanceSearch';
import PriceRange from '../../components/modules/common/client/ClientAdvancedSearcScreenCompo/PriceRange';
import ReceiverCompo from '../../components/modules/common/client/ClientChatScreenSCompo/ReceiverCompo';
import SenderCompo from '../../components/modules/common/client/ClientChatScreenSCompo/SenderCompo';
import CreateEventModule from '../../components/modules/common/client/ClientEventScreensCompo/CreateEventModule';
import HeaderCreateEvent from '../../components/modules/common/client/ClientEventScreensCompo/HeaderCreateEvent';
import CelebrationCompo from '../../components/modules/common/client/ClientSearchScreenCompo/CelebrationCompo';
import ClientSearchHeader from '../../components/modules/common/client/ClientSearchScreenCompo/ClientSearchHeader';
import AmenitiesSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/AmenitiesSupplier';
import CardExtraServicesSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/CardExtraServicesSupplier';
import CardSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/CardSupplier';
import GalleryModuleSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/GalleryModuleSupplier';
import HeaderSupplierScreen from '../../components/modules/common/client/ClientSupllierScreenCompo/HeaderSupplierScreen';
import MapSupplierScreen from '../../components/modules/common/client/ClientSupllierScreenCompo/MapSupplierScreen';
import NuestroMenuButton from '../../components/modules/common/client/ClientSupllierScreenCompo/NuestroMenuButton';
import OtherModuleSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/OtherModuleSupplier';
import ReviewSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/ReviewSupplier';
import TouchableBtnsSupplier from '../../components/modules/common/client/ClientSupllierScreenCompo/TouchableBtnsSupplier';
import ChatBar from '../../components/modules/common/client/ClientChatScreenSCompo/ChatBar'
import PaymentModuleCard from '../../components/modules/common/client/ClientPaymentScreenCompo/PaymentModuleCard';
import AvailableOptions from '../../components/modules/common/client/ClientPaymentScreenCompo/AvailableOptions';
import AbstractTextInput from '../../components/abstract/abstractTextInput';
import TextInputVisa from '../../components/modules/common/client/ClientPaymentScreenCompo/TextInputVisa';
import CVC from '../../components/modules/common/client/ClientPaymentScreenCompo/CVC';
import Expiry from '../../components/modules/common/client/ClientPaymentScreenCompo/Expiry';
import ChangeProfilePhoto from '../../components/modules/common/client/ClientManageAccountScreensCompo/ChangeProfilePhoto';
import ClientBookingConfirmation from '../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation';
import { moderateScale } from 'react-native-size-matters';
import ClientEventDetailsCompo from '../../components/modules/common/client/ClientEventDetailScreenCompo/ClientEventDetailsCompo';
import ChatSvg from '../../assets/icons/ConfirmBookingScreenSvgs/ChatSvg';
import CalendarSVG from '../../assets/icons/calendarSVG';
import LocationSvg from '../../assets/icons/LocationSvg';
import ClockSvg from '../../assets/icons/ClockSvg';
import BudgetCompo from '../../components/modules/common/client/ClientEventDetailScreenCompo/BudgetCompo';
import EventAddButton from '../../components/modules/common/client/ClientEventDetailScreenCompo/EventAddButton';
import CheckListButton from '../../components/modules/common/client/ClientEventDetailScreenCompo/CheckListButton';
import AddContact from '../../components/modules/common/client/ClientEventDetailScreenCompo/AddContact';
import Guest from '../../components/modules/common/client/ClientEventDetailScreenCompo/Guest';
import BillModule from '../../components/modules/common/client/ClientEventDetailScreenCompo/BillModule';
import SplitBillBottomSheet from '../../components/modules/common/client/BottomSheetsClient/SplitBillBottomSheet';
import EventOptionsBottomSheet from '../../components/modules/common/client/BottomSheetsClient/EventOptionsBottomSheet';
import Servicious from '../../components/modules/common/client/ClientEventDetailScreenCompo/Servicious';
import Otros from '../../components/modules/common/client/ClientEventDetailScreenCompo/Otros';
import InformaciondeInvitadosCompo from '../../components/modules/common/client/ClientBookingGuestScreensCompo/InformaciondeInvitadosCompo';
import DetallesdePagoCompo from '../../components/modules/common/client/ClientBookingGuestScreensCompo/DetallesdePagoCompo';
import RevisarBookingPagoCompo from '../../components/modules/common/client/ClientBookingGuestScreensCompo/RevisarBookingPagoCompo';
import AbstractHeader from '../../components/abstract/SearchCompo/AbstractHeader';
import { goBack } from '../../navigation/authNavigator';
import ClientPaymentMethodScreen from './ClientProfileScreens/ClientPaymentMethodScreen'
import ClientManageMyAccountScreen from './ClientProfileScreens/ClientManageMyAccountScreen';
import ClientBookingConfirmationScreen from './ClientBookingConfirmationScreen';
import ClientChatScreen from './ClientChatScreen/ClientChatScreen';
import InformaciondeInvitadosScreen from './ClientBookingScrrens/InformaciondeInvitadosScreen';
import DetallesdePagoScreen1 from './ClientBookingScrrens/DetallesdePagoScreen1';
import DetallesdePagoScreen2 from './ClientBookingScrrens/DetallesdePagoScreen2';
import ReviewBookingScreen from './ClientBookingScrrens/ReviewBookingScreen';
import CreateEventScreen from './ClientEventScreens/CreateEventScreen';
import AddGuestsScreen from './ClientEventScreens/AddGuestsScreen';
import EventsDetailsScreen from './ClientEventScreens/EventsDetailsScreen';
import CancelBookingBottomSheet from '../../components/modules/common/client/BottomSheetsClient/CancelBookingBottomSheet';
import CheckListScreen from './ClientEventScreens/CheckListScreen';



const SearchClientScreen: React.FC = props => {
  return (
    <View style={styles.mainContainer}>
    
      

    </View>
  );
};
export default SearchClientScreen;


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center'
  },
 
})