import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ClientBookingConfirmation from '../../../components/modules/common/client/ClientBookingConfirmationCompo/ClientBookingConfirmation'
import ChatBar from '../../../components/modules/common/client/ClientChatScreenSCompo/ChatBar'
import ReceiverCompo from '../../../components/modules/common/client/ClientChatScreenSCompo/ReceiverCompo'
import SenderCompo from '../../../components/modules/common/client/ClientChatScreenSCompo/SenderCompo'
import SendingImagesCompo from '../../../components/modules/common/client/ClientChatScreenSCompo/SendingImagesCompo'
import { goBack } from '../../../navigation/authNavigator'



const ChatDate = () => {
    return(
        <View style={{width:'100%',height:30,justifyContent:'center',alignItems:'center'}}>
            <View style={{width:moderateScale(58,0.1),justifyContent:'center',alignItems:'center',height:moderateScale(16,0.1),borderRadius:20,backgroundColor:'rgba(0, 0, 0, 0.6)'}}>
                <Text style={styles.smallHeading}>May 7</Text>
            </View>
        </View>
    )
}







const ClientChatScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
            <View style={styles.mainContainer}>
                <AbstractHeader type={'ChatHeader'} onPressBack={()=>goBack()} />
                <ScrollView showsVerticalScrollIndicator={false}>
                <ContentContainer style={{width:'90%',flex:1}} >
                      
                   <View style={{width:'100%'}}>
                     <ChatDate />

                    <SenderCompo text={'hai My name is hamza'} />
                    <ReceiverCompo text={'Right Whats your hobbies'} />
                    <SenderCompo text={'Cricket , games '} />
                    <SendingImagesCompo />
                    <ReceiverCompo text={'Right Whats your hobbies'} />
                    <SenderCompo text={'Cricket , games '} />
                    <ReceiverCompo text={'Right Whats your hobbies'} />
                    <SenderCompo text={'Cricket , games '} />

                    </View>
                  
                </ContentContainer>
                </ScrollView>
                <View style={{width:'100%',height:moderateScale(55,0.1)}}>

                </View>

              <View style={{position:'absolute',bottom:0}}>
           <ChatBar />
              </View>
              
            </View>
        </ScreenContainer>
    )
}

export default ClientChatScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#F6F7F9',
        position:'relative'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'white'
    }
})
