import React from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity,StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ContentContainer from '../../../components/abstract/contentContainer'
import YourServicesCard from '../../../components/modules/common/provider/YourServicesCard'
import Ionicons from 'react-native-vector-icons/Ionicons';
import EventModule from '../../../components/modules/common/client/EventModule';
import { navigate } from '../../../navigation/authNavigator';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';


interface Props {
    onPress?: ()=>void
}




const ClientsEventsScreen:React.FC<Props> = ({onPress}) => {
    return (
        <ScreenContainer>
        <View style={{ flex: 1, position: 'relative' }}>
             <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />

            <View style={styles.viewOne} >
                <TouchableOpacity
                   onPress={()=>navigate('CreateEvent')}
                    activeOpacity={0.8}
                    style={styles.viewTwo}>
                    <Ionicons name={'add'} color={'white'} size={20} />
                </TouchableOpacity>
            </View>


            <ContentContainer>


                <View style={{ width: '100%', marginTop: 20 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>

                    <EventModule type={'Options'} bookings time onPress={()=>navigate('EventDetail')} onPressOptions={()=>navigate('')} />
                    <EventModule type={'Options'} bookings time onPressOptions={()=>navigate('CreateEvent')} />
                    <EventModule type={'Options'} ProfilePic time onPressOptions={()=>navigate('CreateEvent')} />
                    <EventModule type={'Request'} ProfilePic time onPressOptions={()=>navigate('CreateEvent')} />




                    </ScrollView>
                </View>

            </ContentContainer>

        </View>
        </ScreenContainer>
    )
}

export default ClientsEventsScreen

const styles = StyleSheet.create({
    viewOne:{
        width: '15%', 
        height: moderateScale(60, 0.1),
         position: 'absolute',right:'7.5%', 
         bottom: 20,
    },
    viewTwo:{
        height: moderateScale(58, 0.1),
         zIndex: 1,
          width: moderateScale(58, 0.1),
           justifyContent: 'center',
            alignItems: 'center',
             borderRadius: 30,
              backgroundColor: '#FF5959'
    }
})
