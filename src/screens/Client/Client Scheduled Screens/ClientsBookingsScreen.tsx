import React,{useState} from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ContentContainer from '../../../components/abstract/contentContainer'
import YourServicesCard from '../../../components/modules/common/provider/YourServicesCard'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Bookings from '../../../components/modules/common/client/Bookings';
import { navigate } from '../../../navigation/authNavigator';
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet';
import CancelBookingBottomSheet from '../../../components/modules/common/client/BottomSheetsClient/CancelBookingBottomSheet';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';





const HeaderCompoCustom = () => {
    return (
       <></> 
    )
}


const ClientsBookingsScreen: React.FC = () => {

    const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }

    return (
        <ScreenContainer>
        <View style={{ flex: 1, position: 'relative' }}>
            <StatusBar
                backgroundColor={'white'}
                barStyle="dark-content"
            />
            <AbstractBottomSheet onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom />} >
                <View style={{ width: '85%', height: 226,alignSelf: 'center' }}>
                   <CancelBookingBottomSheet />
                </View>
            </AbstractBottomSheet>
            <ContentContainer>
                <View style={{ width: '100%', marginTop: 20 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>

                        <Bookings Person onPress={() => navigate('BookingConfirm')} onPressOptions={()=>toggleModal()} />
                        <Bookings onPress={() => navigate('BookingConfirm')} onPressOptions={()=>toggleModal()} />
                        <Bookings onPress={() => navigate('BookingConfirm')} onPressOptions={()=>toggleModal()} />


                    </ScrollView>
                </View>

            </ContentContainer>

        </View>
        </ScreenContainer>
    )
}

export default ClientsBookingsScreen

const styles = StyleSheet.create({})
