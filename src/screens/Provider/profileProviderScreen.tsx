import React from 'react';
import { View, Text,StatusBar } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import LogoutSVG from '../../assets/icons/logoutSVG';
import AbstractButton from '../../components/abstract/abstractButton';
import AbstractIconButton from '../../components/abstract/abstractIconButton';
import AbstractProfileNavigation from '../../components/abstract/AbstractProfileNavigation';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../components/abstract/contentContainer';
import ProfileScreenModule from '../../components/modules/common/provider/ProfileScreenModule';
import { clearAndNavigate, navigate } from '../../navigation/authNavigator';

const ProfileProviderScreen: React.FC = props => {
  return (
    <ScreenContainer>
    <View style={{ flex: 1, backgroundColor: 'white' }}>
          <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />

      <View style={{ width: '100%', alignItems: 'center', backgroundColor: 'white', height: moderateScale(160, 0.1), justifyContent: 'center' }}>
        <ProfileScreenModule />
      </View>


      <View>
        <ContentContainer style={{width:'80%'}}>
          <AbstractProfileNavigation text={'Finance Settings'} onPress={()=>navigate('ProfileFinance')} />
          <AbstractProfileNavigation text={'Financials'} onPress={()=>navigate('ProviderFinincial')} />
          <AbstractProfileNavigation text={'Terms of Service'} />
          <AbstractProfileNavigation text={'Manage My Account'} onPress={()=>navigate('ProviderManageAccount')} />
          <AbstractProfileNavigation logout onPress={()=> clearAndNavigate('IntroScreen')} />


        </ContentContainer>
      </View>

    </View>
    </ScreenContainer>
  );
};
export default ProfileProviderScreen;
