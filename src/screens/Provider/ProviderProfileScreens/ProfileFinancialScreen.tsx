import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'


const ProfileFinancialScreen:React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
        <View style={styles.mainContainer}>

              <AbstractHeader type={'ProviderFinancialSetting'}  />

            <ContentContainer>

               <View style={{marginVertical:20}}>
                   <Text style={styles.textOne}>Bank Details</Text>
               </View>    

              <View style={{ width: '100%', height: 300,}}>
                        <AbstractTextInput Fonts={'OpenSans'} label={'Name'} placeholderText={'Jennifer'}  />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Account Number'} placeholderText={'...........'} />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Routing Number '} placeholderText={'2984651218910'} />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Bank'} placeholderText={'Banco General'} />
                    </View>
           </ContentContainer>
        </View>
        </ScreenContainer>
    )
}

export default ProfileFinancialScreen

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        backgroundColor:'white'
    },
    textOne:{
        fontWeight:'600',
        fontFamily:'OpenSans',
        fontSize:moderateScale(16,0.1),
        color:'black'
    }
})
