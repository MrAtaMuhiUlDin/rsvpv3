import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import AbstractTextInput from '../../../components/abstract/abstractTextInput'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import ChangeProfilePhoto from '../../../components/modules/common/client/ClientManageAccountScreensCompo/ChangeProfilePhoto'
import { goBack } from '../../../navigation/authNavigator'


const ProviderManageMyAccountScreen: React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar backgroundColor={'white'} barStyle={'dark-content'} />
            <View style={styles.mainContainer}>
                <AbstractHeader type={'HeaderForManageAccount'} onPressBack={()=>goBack()} />

                <ContentContainer>

                    <View style={{ width: '100%', height: 200, justifyContent: 'center', alignItems: 'center' }}>
                        <ChangeProfilePhoto   />
                    </View>


                    <View style={{ width: '100%', height: 300,}}>
                        <AbstractTextInput Fonts={'OpenSans'} label={'First Name'} placeholderText={'Jennifer'}  />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Last Name'} placeholderText={'Anniston'} />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Email'} placeholderText={'j.anniston@gmail.com'} />
                        <AbstractTextInput Fonts={'OpenSans'} label={'Phone'} placeholderText={'+1- 2383-989899'} />
                    </View>


                </ContentContainer>


            </View>
        </ScreenContainer>
    )
}

export default ProviderManageMyAccountScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white'
    },
    LargeHeading: {
        fontSize: moderateScale(16, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'black'

    },
    smallHeading: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'black'
    }
})
