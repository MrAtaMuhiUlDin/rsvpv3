import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ContentContainer from '../../../components/abstract/contentContainer'
import Agenda from '../../../components/modules/common/provider/Agenda';
import ProviderSpaceModule from '../../../components/modules/common/provider/ProviderSpaceModule';
import YourServicesCard from '../../../components/modules/common/provider/YourServicesCard'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AbstractBottomSheet from '../../../components/abstract/AbstractBottomSheet';
import BottomSheetOptions from './BottomSheetOptions';


interface Props {
    navigation?: any
}



const HeaderCompoCustom = () => {
    return (
        <ContentContainer>
        <View style={{ width: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: 1, borderColor: '#E0E0E1' }}>
            <TouchableOpacity>
                <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600' }}>Cancel</Text>
            </TouchableOpacity>

            <TouchableOpacity>
                <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600', color: '#EF4339' }}>Done</Text>
            </TouchableOpacity>
        </View>

    </ContentContainer>
    )
}


// Screen1
const SpaceScreen1 = ({ navi }: any) => {
    return (
        <ContentContainer>
            <View style={{ width: '100%', marginTop: 20 }}>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <ProviderSpaceModule onPress={navi} />

                </ScrollView>
            </View>
        </ContentContainer>
    )
}



//Screen2
const SpaceScreen2 = () => {
    const [isModalVisible, setVisible] = useState<Boolean>(false)
    const toggleModal = () => {
        setVisible(true)
    }
    return (
        <View style={{ flex: 1, position: 'relative' }}>
            <StatusBar
                backgroundColor={'white'}
                barStyle="dark-content"
            />

            <View style={styles.viewOne} >
                <TouchableOpacity
                   onPress={toggleModal}
                    activeOpacity={0.8}
                    style={styles.viewTwo}>
                    <Ionicons name={'add'} color={'white'} size={17} />
                </TouchableOpacity>
            </View>

            <ContentContainer>

                <View style={{ width: '100%', marginTop: 20 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>

                        <YourServicesCard imageUrl={require('../../../assets/images/yacht.png')} title={'Luxury Yacht Rental '} />
                        <YourServicesCard imageUrl={require('../../../assets/images/yacht.png')} title={'Luxury Yacht Rental '} />

                    </ScrollView>
                </View>


                <AbstractBottomSheet  onClose={() => setVisible(false)} isVisible={isModalVisible} MyHeader={<HeaderCompoCustom/>} >
                    <BottomSheetOptions />
                </AbstractBottomSheet>


            </ContentContainer>

        </View>

    )
}




const PlansScreen = ({ navigation }: Props) => {

    const [navigationState, setnavigationState] = useState<boolean>(false)
    return (
        <React.Fragment>
            {
                navigationState ? <SpaceScreen2 /> : <SpaceScreen1 navi={() => setnavigationState(true)} />

            }
        </React.Fragment>
    )
}

export default PlansScreen

const styles = StyleSheet.create({
    MainContainer: {

    },
    viewOne: {
        width: '100%',
        height: moderateScale(38, 0.1),
        position: 'absolute',
        bottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        height: moderateScale(38, 0.1),
        zIndex: 1,
        width: moderateScale(105, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        backgroundColor: '#FF5959'
    },
})
