import React from 'react'
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ContentContainer from '../../../components/abstract/contentContainer'
import YourServicesCard from '../../../components/modules/common/provider/YourServicesCard'
import Ionicons from 'react-native-vector-icons/Ionicons';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';




const ServicesScreen: React.FC = () => {
    return (
        <ScreenContainer>
        <View style={{ flex: 1, position: 'relative' }}>
            <StatusBar
                backgroundColor={'white'}
                barStyle="dark-content"
            />

            <View style={styles.viewOne} >
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.viewTwo}>
                    <Ionicons name={'add'} color={'white'} size={17} />
                </TouchableOpacity>
            </View>


            <ContentContainer>


                <View style={{ width: '100%', marginTop: 20 }}>
                    <ScrollView showsVerticalScrollIndicator={false}>

                        <YourServicesCard tag tagtext={'SERVICIO'} imageUrl={require('../../../assets/images/chef.png')} title={'Chef Privado'} />


                    </ScrollView>
                </View>

            </ContentContainer>

        </View>
        </ScreenContainer>
    )
}

export default ServicesScreen

const styles = StyleSheet.create({
    viewOne: {
        width: '100%',
        height: moderateScale(38, 0.1),
        position: 'absolute',
        bottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        height: moderateScale(38, 0.1),
        zIndex: 1,
        width: moderateScale(105, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 35,
        backgroundColor: '#FF5959'
    }
})
