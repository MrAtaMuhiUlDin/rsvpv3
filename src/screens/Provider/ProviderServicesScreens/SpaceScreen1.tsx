import React from 'react'
import { StyleSheet, Text, View,ScrollView,StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ContentContainer from '../../../components/abstract/contentContainer'
import Agenda from '../../../components/modules/common/provider/Agenda';
import ProviderSpaceModule from '../../../components/modules/common/provider/ProviderSpaceModule';


interface Props {
    navigation?:any
}

const SpaceScreen = ({navigation}:Props) => {
 

    return (
        <View style={{flex:1}}>
           <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />
            <ContentContainer>
           
           <ProviderSpaceModule onPress={()=>navigation.navigate('SpaceScreen2')} />
             
            </ContentContainer>
           
        </View>
    )
}

export default SpaceScreen

const styles = StyleSheet.create({})
