import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import DeleteSvg from '../../../assets/icons/EventDetailsScreenSvgs/DeleteSvg';
import EditSvgs from '../../../assets/icons/EventDetailsScreenSvgs/EditSvgs';
import ContentContainer from '../../../components/abstract/contentContainer';
import { navigate } from '../../../navigation/authNavigator';



interface Props {
    
}

const BottomSheetOptions = ({ onPress }: { onPress?: () => void }) => {
    return (
        <ContentContainer>
        <View style={{ width: '100%', height: 150 }}>

        <TouchableOpacity
        onPress ={()=>navigate('PriceMethodTwo')}
            activeOpacity={0.8}
            style={styles.viewTwo}>
            <Text>
            Edit Listing
            </Text>
            <EditSvgs />
        </TouchableOpacity>

        <TouchableOpacity
            activeOpacity={0.8}
            onPress ={()=>navigate('AddBasic')}
            style={styles.viewTwo}>
            <Text>
            Edit Packages
            </Text>
            <EditSvgs />
        </TouchableOpacity>


        <TouchableOpacity
            activeOpacity={0.8}
            style={styles.viewTwo}>
            <Text style={{color:'rgba(255, 0, 0, 1)'}}>
            Delete Listing
            </Text>
            <DeleteSvg />
        </TouchableOpacity>

    </View>
    </ContentContainer>
    )
}

export default BottomSheetOptions

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#13204D'
    },
    viewTwo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        justifyContent: 'space-between',
        height: 50,
        // backgroundColor: 'pink'
    },
    viewThree: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },

    textTwo: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },

})
