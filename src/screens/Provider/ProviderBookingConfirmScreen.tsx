import React from 'react'
import { ImageBackground, ScrollView, StyleSheet, Text, View } from 'react-native'
import { ScreenContainer } from 'react-native-screens'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../components/abstract/abstractButton'
import ContentContainer from '../../components/abstract/contentContainer'
import AbstractHeader from '../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration'
import DetallesdePagoCompo from '../../components/modules/common/client/ClientBookingGuestScreensCompo/DetallesdePagoCompo'
import AdditionalPriceBtn from '../../components/modules/common/provider/ProviderAllStepsCompo/AdditionalPriceBtn'
import { navigate } from '../../navigation/authNavigator'


const ProviderBookingConfirmScreen:React.FC= () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar  barStyle={'dark-content'} backgroundColor={'white'} />
            <View style={styles.mainContainer}>
                  <ScrollView showsVerticalScrollIndicator={false}>

                  <View style={{ width: '100%', height: moderateScale(435, 0.1) }}>
                        <ImageBackground source={require('../../assets/images/Rectangle.png')} resizeMode={'stretch'} style={{ width: '100%', height: '100%' }}>
                            <AbstractHeader type={'SimpleBackHeader'} bgColor={'transparent'} />
                            <ContentContainer style={{ width: '85%' }} >

                                <View style={{ width: '100%', height: 60, justifyContent: 'center' }}>
                                    <Text style={styles.LargeHeading}>Confirm Booking</Text>
                                </View>

                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                                    <DetallesdePagoCompo />
                                </View>



                            </ContentContainer>
                        </ImageBackground>
                    </View>
                 
                    <ContentContainer>
                    <View style={{height:200}}>

                    </View>


 
                    <View style={{ height: moderateScale(115, 0.1), width: '100%', marginTop: 50, justifyContent: 'space-between' }}>
                            <AdditionalPriceBtn label={'Decline Booking'} />
                            <AbstractButton title={'CONFIRM BOOKING'} textSize={moderateScale(14, 0.1)} onPress={()=>navigate('PriceMethodTwo')} />
                        </View>






                    </ContentContainer>
                      
                  </ScrollView>

            </View>
            </ScreenContainer>
    )
}

export default ProviderBookingConfirmScreen


const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: 'white',
        position: 'relative'
    },
    LargeHeading: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    smallHeading: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textOne: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#868E96'
    }
})
