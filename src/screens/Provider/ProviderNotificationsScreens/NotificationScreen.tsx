import React from 'react';
import { View, Text, ScrollView, StatusBar, StyleSheet } from 'react-native';
import { moderateScale, s } from 'react-native-size-matters';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../../components/abstract/contentContainer'
import NotificationsModule from '../../../components/modules/common/provider/NotificationsModule';



const NotificationsScreen: React.FC = props => {
  return (
    <ScreenContainer>
    <View style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'white'}
        barStyle="dark-content"
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <ContentContainer style={{ width: '95%' }}>
          <View style={{ width: '100%', marginTop: 25 }}>
            <ScrollView showsVerticalScrollIndicator={false} nestedScrollEnabled={true}>
              <View style={{ width: '100%' }}>
                <Text style={styles.textOne}>Today</Text>
              </View>

              <NotificationsModule today />

              <View style={{ width: '100%' }}>
                <Text style={styles.textOne}>Yesterday</Text>
              </View>

              <NotificationsModule />


              <View style={{ width: '100%' }}>
                <Text style={styles.textOne}>Mon 25, June</Text>
              </View>

              <NotificationsModule />



            </ScrollView>
          </View>
        </ContentContainer>
      </ScrollView>
    </View>
    </ScreenContainer>
  );
};
export default NotificationsScreen;


const styles = StyleSheet.create({
  textOne: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '400',
    fontFamily: 'Nunito',
    color: '#777777'
  }
})