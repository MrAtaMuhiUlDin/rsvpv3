import { isTemplateElement } from '@babel/types';
import React from 'react';
import { View, Text, ScrollView, FlatList, StatusBar } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../../components/abstract/contentContainer'
import ChatModule from '../../../components/modules/common/provider/ChatModule';
import { navigate } from '../../../navigation/authNavigator';




const MessagesScreen: React.FC = props => {

    interface Data {
        arrayofData: { id: number, active: boolean, notseen: boolean, notseenmorethanMonth: boolean }[]
    }

    const arrayData: Data['arrayofData'] = [
        {
            id: 1,
            active: true,
            notseen: false,
            notseenmorethanMonth: false
        },
        {
            id: 2,
            active: false,
            notseen: false,
            notseenmorethanMonth: false
        },
        {
            id: 3,
            active: false,
            notseen: false,
            notseenmorethanMonth: true
        },
        {
            id: 4,
            active: false,
            notseen: true,
            notseenmorethanMonth: false
        },
        {
            id: 5,
            active: false,
            notseen: false,
            notseenmorethanMonth: true
        },

    ]



    return (
        <ScreenContainer>
        <View style={{ flex: 1 }}>
            <StatusBar
                backgroundColor={'white'}
                barStyle="dark-content"
            />

            <ContentContainer style={{ width: '100%' }}>
                <View style={{ width: '100%', marginTop: 5 }}>


                    <FlatList

                        showsVerticalScrollIndicator={false}
                        data={arrayData}
                        renderItem={({ item }) => {
                            return <ChatModule onPress={()=>navigate('ChatScreen')}  active={item.active} notseen={item.notseen} notseenmorethanMonth={item.notseenmorethanMonth} />
                        }}
                        keyExtractor={(item: any) => item.id}
                        ItemSeparatorComponent={() => {
                            return <View style={{ height: 1, width: '100%', backgroundColor: 'lightgrey' }} />
                        }}

                        ListFooterComponent={() => {
                            return <View style={{ height: 1, width: '100%', backgroundColor: 'lightgrey' }} />
                        }}

                    />



                </View>
            </ContentContainer>

        </View>
        </ScreenContainer>
    );
};
export default MessagesScreen;
