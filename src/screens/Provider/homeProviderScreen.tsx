import React from 'react';
import { View, Text, ScrollView, StatusBar, StyleSheet } from 'react-native';
import ContentContainer from '../../components/abstract/contentContainer';
import Agenda from '../../components/modules/common/provider/Agenda';
import YourServicesCard from '../../components/modules/common/provider/YourServicesCard';

import { moderateScale } from 'react-native-size-matters';
import AbstractTopNavButton from '../../components/abstract/abstractTopNavButton';


const HomeProviderScreen: React.FC = props => {

  const stringsArray = {
    chef: require('../../assets/images/chef.png'),
    yacht: require('../../assets/images/yacht.png')
  }



  return (

    <View style={{
      flex: 1,
      backgroundColor: '#F8F8F8',
    }}>
      <StatusBar
        backgroundColor={'white'}
        barStyle="dark-content"
      />

      <ScrollView showsVerticalScrollIndicator={false} >
        <ContentContainer>

          <View style={styles.viewOne}>
            <View style={styles.viewTwo}>
              <Text style={styles.textOne}>Upcoming Bookings</Text>
              <Text style={styles.textTwo}>SEE ALL</Text>
            </View>
            <Agenda />

          </View>


          <View style={{ width: '100%' }}>
            <View style={styles.viewThree}>
              <Text style={styles.textThree}>Your Services</Text>

            </View>
            <YourServicesCard imageUrl={require('../../assets/images/yacht.png')} title={'Luxury Yacht Rental '} />

          </View>



        </ContentContainer>
      </ScrollView>
    </View>
  );
};
export default HomeProviderScreen;



const styles = StyleSheet.create({
  viewOne: {
    height: moderateScale(164, 0.1),
    width: '100%',
    marginTop: 50,
    marginBottom: 40
  },
  viewTwo: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewThree: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  textOne: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#FF5959',
    paddingBottom: 10
  },
  textTwo: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#5890FF',
    paddingBottom: 10
  },
  textThree: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#FF5959',
    paddingBottom: 30
  },
})