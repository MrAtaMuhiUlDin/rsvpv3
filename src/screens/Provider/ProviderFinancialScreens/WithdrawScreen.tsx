import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../components/abstract/contentContainer'


const WithdrawModule = () => {
    return (
        <View style={{width:'100%',height:40,flexDirection:'row'}}>
              <View style={{width:'50%',height:'100%',justifyContent:'center'}}>
                  <Text style={styles.textTwo}>Weekly Billing</Text>
              </View>
              <View style={{width:'30%',height:'100%',justifyContent:'center'}}>
              <Text style={styles.textTwo}>00/00/0000</Text>
              </View>
              <View style={{width:'20%',height:'100%',justifyContent:'center',alignItems:'center'}}>
              <Text style={styles.textTwo}>$400</Text>
              </View>

        </View>
    )
}







const WithdrawScreen:React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <ScrollView showsVerticalScrollIndicator={false}>
        <ContentContainer>

          <View style={styles.viewOne}>
              <View style={{width:'50%',height:'100%',justifyContent:'center'}}>
                  <Text style={styles.textone}>Paid</Text>
              </View>
              <View style={{width:'30%',height:'100%',justifyContent:'center'}}>
              <Text style={styles.textone}>Date</Text>
              </View>
              <View style={{width:'20%',height:'100%',justifyContent:'center',alignItems:'center'}}>
              <Text style={styles.textone}>Billing</Text>
              </View>

          </View>


          <View>

          <WithdrawModule />
          <WithdrawModule />
          <WithdrawModule />
          <WithdrawModule />
          <WithdrawModule />
          <WithdrawModule />


          </View>




        </ContentContainer>
        </ScrollView>
    </View>
    )
}

export default WithdrawScreen

const styles = StyleSheet.create({
    mainContainer:{
        paddingTop:20,
        backgroundColor:'white',
        flex:1
    },
    viewOne:{
        width:'100%',
        height:40,
        // backgroundColor:'red',
        flexDirection:'row'

    },
    textone:{
        fontFamily:'OpenSans',
        fontWeight:'900',
        fontSize:moderateScale(13,0.1),
        color:'rgba(50, 50, 50, 1)'
    },
    textTwo:{
        fontFamily:'OpenSans',
        fontWeight:'400',
        fontSize:moderateScale(13,0.1),
        color:'rgba(50, 50, 50, 1)'
    }
    })