import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import BudgetCompo from '../../../components/modules/common/client/ClientEventDetailScreenCompo/BudgetCompo'
import CompletedJobsCompo from '../../../components/modules/common/provider/FinancialsCompo/CompletedJobsCompo'
import { goBack } from '../../../navigation/authNavigator'
import ProviderFinancialTopBarNavigation from '../../../navigation/ProviderFinancialTopBarNavigation'

interface Props {

}

const ProviderFinancialScreen = (props: Props) => {



    return (
        <ScreenContainer>
            <View style={styles.mainContainer}>
                <AbstractHeader type={'FinancialHeader'} onPressBack={() => goBack()} />

                <ContentContainer style={{ width: '90%' }}>
                    <View style={styles.viewOne}>
                        <BudgetCompo bgColor={'#FF5959'} height={70} financial />
                    </View>
                </ContentContainer>
                 
              
                <ProviderFinancialTopBarNavigation />
           
              
            </View>
        </ScreenContainer>
    )
}

export default ProviderFinancialScreen

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    viewOne: {
        width: '100%',
        height: moderateScale(99, 0.1),
        backgroundColor: '#FF5959',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop:10
    },
    viewTwo: {
        width: '100%',
        height: moderateScale(50, 0.1),
        backgroundColor: '#EFEFEF',
        marginVertical: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: '90%',
        height: '100%',
        // backgroundColor:'green',
        flexDirection: 'row'
    },
    viewFour: {
        width: '33.4%',
        height: '100%',
        // backgroundColor:'pink',
        justifyContent: 'center',
        alignItems: 'center'

    },
    viewFive: {
        width: 26,
        height: 5,
        borderRadius: 30,
        backgroundColor: '#FF5959',
        marginTop: 5
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(13, 0.1),
        color: '#FF5959',
        fontWeight: '400'
    }

})
