import React from 'react'
import { StyleSheet, Text, View,ScrollView } from 'react-native'
import CompletedJobsCompo from '../../../components/modules/common/provider/FinancialsCompo/CompletedJobsCompo'



const CompletedJobsScreen:React.FC = () => {


    const CompletedTasksCompo = () => {
        return (

            <CompletedJobsCompo />

        )
    }



    return (
        <ScrollView style={{backgroundColor:"white"}} showsVerticalScrollIndicator={false}>

        {[0, 0, 0, 0].map((val, index) => {
            return (
                <React.Fragment>
                <View key={index} style={{width:'85%',alignSelf:'center'}}>
                <CompletedTasksCompo />
                </View>
                <View style={{width:'100%',height:1,backgroundColor:'lightgrey',marginVertical:8}} />
                </React.Fragment>
            )
        })}




    </ScrollView>
    )
}

export default CompletedJobsScreen

const styles = StyleSheet.create({})
