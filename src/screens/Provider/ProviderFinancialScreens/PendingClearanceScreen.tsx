import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import ContentContainer from '../../../components/abstract/contentContainer'

interface Props {
    
}

const PendingClearanceScreen:React.FC = () => {
    return (
   
        <View style={styles.mainContainer}>
            <ContentContainer>
            <Text>PendingClearanceScreen</Text>
            </ContentContainer>
        </View>

    )
}

export default PendingClearanceScreen

const styles = StyleSheet.create({
mainContainer:{
    paddingTop:20,
    backgroundColor:'white',
    flex:1
}
})
