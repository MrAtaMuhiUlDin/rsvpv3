import React from 'react'
import { StyleSheet, Text, View, ScrollView, StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../../components/abstract/contentContainer'
import Agenda from '../../../components/modules/common/provider/Agenda';



const ProviderAgendaScreen:React.FC = () => {


  return (
    <ScreenContainer>
    <View style={{ flex: 1 }}>
      <StatusBar
        backgroundColor={'white'}
        barStyle="dark-content"
      />
      <ContentContainer>
        <ScrollView showsVerticalScrollIndicator={false}>

          {/* Schedule View */}
          <View style={{ width: '100%', marginTop: 25 }}>
            <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
              <View style={styles.viewOne}>
                <Text style={styles.textOne}>Scheduled</Text>
              </View>

              <Agenda money />
              <Agenda money />

            </ScrollView>
          </View>

          {/* Pending View */}
          <View style={{ width: '100%', marginTop: 20 }}>
            <ScrollView nestedScrollEnabled={true} showsVerticalScrollIndicator={false}>
              <View style={styles.viewTwo}>
                <Text style={styles.textTwo}>Pending</Text>
              </View>

              <Agenda money />

            </ScrollView>
          </View>



        </ScrollView>
      </ContentContainer>

    </View>
    </ScreenContainer>
  )
}

export default ProviderAgendaScreen



const styles = StyleSheet.create({
  viewOne: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  viewTwo: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  textOne: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#FF5959'
  },
  textTwo: {
    fontSize: moderateScale(14, 0.1),
    fontWeight: '700',
    fontFamily: 'Nunito',
    color: '#FF5959',
    paddingBottom: 5
  },
})