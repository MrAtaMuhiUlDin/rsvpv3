import React from 'react'
import { StyleSheet, Text, View,ScrollView,StatusBar } from 'react-native'
import { moderateScale } from 'react-native-size-matters';
import ScreenContainer from '../../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../../components/abstract/contentContainer'
import Agenda from '../../../components/modules/common/provider/Agenda';


const ProviderScheduledScreen:React.FC = () => {

    return (
      <ScreenContainer>
        <View style={{flex:1}}>
            <StatusBar
            backgroundColor={'white'}
            barStyle="dark-content"
            />
            <ContentContainer>


          <View style={{width:'100%',marginTop:20}}>
            <ScrollView showsVerticalScrollIndicator={false}>

          <Agenda  />
      
          </ScrollView>
        </View>


        </ContentContainer>
           
        </View>
        </ScreenContainer>
    )
}

export default ProviderScheduledScreen

const styles = StyleSheet.create({})
