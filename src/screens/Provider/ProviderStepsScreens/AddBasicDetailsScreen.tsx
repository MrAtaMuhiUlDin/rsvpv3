import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../components/abstract/abstractButton'
import ScreenContainer from '../../../components/abstract/abstractScreenContainer'
import ContentContainer from '../../../components/abstract/contentContainer'
import AbstractHeader from '../../../components/abstract/SearchCompo/AbstractHeader'
import FocusAwareStatusBar from '../../../components/abstract/statusbarConfiguration'
import HeadingSteps from '../../../components/modules/common/provider/ProviderAllStepsCompo/HeadingSteps'
import LinkTextInput from '../../../components/modules/common/provider/ProviderAllStepsCompo/LinkTextInput'
import SimpleTextInput from '../../../components/modules/common/provider/ProviderAllStepsCompo/SimpleTextInput'
import UploadYourSpaceImage from '../../../components/modules/common/provider/ProviderAllStepsCompo/UploadYourSpaceImage'
import { goBack, navigate } from '../../../navigation/authNavigator'



const AddBasicDetailsScreen:React.FC = () => {
    return (
        <ScreenContainer>
            <FocusAwareStatusBar barStyle={'dark-content'} backgroundColor={'white'} />
        <View style={styles.mainContainer}>

            <AbstractHeader type={'ProviderStepsHeader'} onPressBack={()=>goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <ContentContainer style={{width:'85%'}}>
            
   
            <View>        
           <HeadingSteps step={1} heading={'Service Details'} innerViewWidth={'80%'} />
             </View>

           <View>
               <UploadYourSpaceImage />
           </View>

           
           <View style={{width:'100%',height:380,marginTop:20,justifyContent:'space-between'}}>
            <SimpleTextInput label={'Name'} />
            <SimpleTextInput label={'Description'} />
            <SimpleTextInput label={'Hyperlink'} placeHolder={'Title'} />
            <SimpleTextInput label={'Name'}  />
            <LinkTextInput placeHolder={'Link'} />
           </View>



           <View style={{height:moderateScale(50,0.1),width:'100%',marginTop:50}}>
             <AbstractButton  title={'NEXT'}  textSize={moderateScale(14,0.1)} onPress={()=>navigate('TypesOfEvents')} />
           </View>


            </ContentContainer>
            <View style={{width:'100%',height:10}}  />

            </ScrollView>

        </View>
        </ScreenContainer>
    )
}

export default AddBasicDetailsScreen

const styles = StyleSheet.create({
    mainContainer:{
       flex:1,
       backgroundColor:'white'
    }
})
