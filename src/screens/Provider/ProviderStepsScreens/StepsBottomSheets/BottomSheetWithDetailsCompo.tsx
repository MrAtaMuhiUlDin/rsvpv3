import React,{useState} from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../../components/abstract/contentContainer'
import { CheckBoxCompo } from '../../../../components/modules/common/client/BottomSheetsClient/SplitBillBottomSheet'
import SelectOptionsTextInput from '../../../../components/modules/common/provider/ProviderAllStepsCompo/SelectOptionsTextInput'
import SimpleTextInput from '../../../../components/modules/common/provider/ProviderAllStepsCompo/SimpleTextInput'
import UploadAnImageSteps from '../../../../components/modules/common/provider/ProviderAllStepsCompo/UploadAnImageSteps'
import CheckBox from '@react-native-community/checkbox';
import ExtraSevicesSimple from '../../../../components/modules/common/provider/ProviderAllStepsCompo/ExtraSevicesSimple'
import ExtraSevicesSimpleWithDetails from '../../../../components/modules/common/provider/ProviderAllStepsCompo/ExtraSevicesSimpleWithDetails'
import AbstractButton from '../../../../components/abstract/abstractButton'



interface Props {
    
}

const BottomSheetWithDetailsCompo = (props: Props) => {

    const [toggleCheckBox, setToggleCheckBox] = useState<boolean>(false)



    return (
        <View style={styles.mainContainer}>
              
          <ContentContainer style={{width:'90%'}}>

             <View style={{width:'100%',height:130,justifyContent:'center',alignItems:'center',marginVertical:10}}>
                 <UploadAnImageSteps />
             </View>

             <View style={{width:'100%',height:350,justifyContent:'space-between'}}>
                  <SimpleTextInput label={'Title'} />
                  <SimpleTextInput label={'Price'} />
                  <SimpleTextInput label={'What It includes'} />
                  <SelectOptionsTextInput label={'Cancellation Policy'} />

                  <View style={{width:'100%',height:30,flexDirection:'row',alignItems:'center'}}>
                  <CheckBox
                    tintColors={{ true: '#FF5959', false: 'grey' }}
                    onCheckColor={'#FF5959'}
                    disabled={false}
                    value={toggleCheckBox}
                    onValueChange={(newValue) => setToggleCheckBox(newValue)}
                />
                <Text style={styles.textOne}>Accept 50% Upfront Payment</Text>
                   </View>  

                  <SelectOptionsTextInput label={'Group Section'} />
             </View>
            


            <View>
                 <ExtraSevicesSimpleWithDetails />
            </View>

            
           <View style={{height:moderateScale(50,0.1),width:'100%',marginTop:50,marginBottom:30}}>
             <AbstractButton title={'SAVE'}  textSize={moderateScale(14,0.1)} />
           </View>

            
            
          </ContentContainer>
        </View>
    )
}

export default BottomSheetWithDetailsCompo

const styles = StyleSheet.create({
    mainContainer:{
        // height:moderateScale(600,0.1)
    },
    textOne:{
        fontFamily:'OpenSans',
        fontWeight:'600',
        fontSize:moderateScale(11,0.1),
        color:'#868E96'
    }
})
