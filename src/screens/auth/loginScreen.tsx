import React from 'react';
import {useState} from 'react';
import {TouchableOpacity, View, TextInput} from 'react-native';
import AbstractButton from '../../components/abstract/abstractButton';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import AbstractTextInput from '../../components/abstract/abstractTextInput';
import AbstractTextOverLine from '../../components/abstract/abstractTextOverLine';
import MyText from '../../components/abstract/myText';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import AuthHeader from '../../components/modules/auth/authHeader';
import EmailConfirmationBox from '../../components/modules/auth/emailConfirmationBox';
import {navigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const LoginScreen: React.FC = props => {
  const [emailModalVisible, setEmailVisible] = useState(false);
  const [email, setEmail] = useState<string>('rsvp@rsvp.com');
  const [password, setPassword] = useState<string>('123456789');

  return (
    <ScreenContainer>
      <FocusAwareStatusBar backgroundColor={Colors.SECONDRY} />
      <AuthHeader title="Bienvenido de vuelta" />
      <View style={{flex: 1, justifyContent: 'space-between'}}>
        <View style={{width: '85%', alignSelf: 'center'}}>
          <AbstractTextInput
            value={email}
            onChangeText={text => setEmail(text)}
            keyboardType="email-address"
            label="EMAIL"
          />
          <AbstractTextInput
            value={password}
            onChangeText={text => setPassword(text)}
            secureTextEntry
            label="CONTRASENA"
          />
          <View style={{height: 10}} />
          <TouchableOpacity onPress={() => setEmailVisible(true)}>
            <MyText style={{alignSelf: 'flex-end', color: '#2050D5'}}>
              Olvide mi contraseña
            </MyText>
          </TouchableOpacity>
        </View>
        <View style={{width: '80%', alignSelf: 'center'}}>
          <AbstractButton title="ACCEDER" />
          <View style={{height: 15}} />
          <AbstractTextOverLine text="No tienes una cuenta?" />
          <View style={{height: 15}} />
          <AbstractButton
            onPress={() => navigate('RegisterUserTypeScreen')}
            btnStyle="secondry"
            title="REGISTRATE"
          />
          <View style={{height: 15}} />
        </View>
      </View>
      <EmailConfirmationBox
        visible={emailModalVisible}
        onSubmit={email => {
          setEmailVisible(false);
        }}
        onCancel={() => setEmailVisible(false)}
      />
    </ScreenContainer>
  );
};
export default LoginScreen;
