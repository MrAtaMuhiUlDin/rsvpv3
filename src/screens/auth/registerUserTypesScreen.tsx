import React from 'react';
import {useState} from 'react';
import {View} from 'react-native';
import AbstractButton from '../../components/abstract/abstractButton';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import ContentContainer from '../../components/abstract/contentContainer';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import AuthHeader from '../../components/modules/auth/authHeader';
import {USER_TYPES} from '../../components/modules/auth/userTypes';
import UserTypesSelector from '../../components/modules/auth/userTypesSelector';
import {navigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const RegisterUserTypesScreen: React.FC = props => {
  const [selectedItemId, setItemId] = useState<string>('');
  const handleNavigate = () => {
    if (selectedItemId === '123456789') {
      navigate('RegisterClientScreen');
    } else {
      navigate('RegisterProviderScreen');
    }
  };
  return (
    <ScreenContainer>
      <FocusAwareStatusBar backgroundColor={Colors.SECONDRY} />

      <AuthHeader title="Tipo de Cuenta" />
      <ContentContainer style={{flex: 1}}>
        <View style={{height: 10}} />
        <View style={{flex: 1, justifyContent: 'space-between'}}>
          <UserTypesSelector
            onChange={typeId => setItemId(typeId)}
            userTypes={USER_TYPES}
          />
          <AbstractButton
            onPress={() => handleNavigate()}
            center
            width="95%"
            title="COMPLETAR REGISTRO"
          />
        </View>
        <View style={{height: 20}} />
      </ContentContainer>
    </ScreenContainer>
  );
};
export default RegisterUserTypesScreen;
