import React from 'react';
import {useState} from 'react';
import {StyleSheet} from 'react-native';
import {ScrollView, View, KeyboardAvoidingView} from 'react-native';
import AbstractButton from '../../components/abstract/abstractButton';
import AbstractImageBox from '../../components/abstract/abstractImageBox';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import AbstractTextInput from '../../components/abstract/abstractTextInput';
import ContentContainer from '../../components/abstract/contentContainer';
import MyText from '../../components/abstract/myText';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import AuthHeader from '../../components/modules/auth/authHeader';
import {USER_TYPES} from '../../components/modules/auth/userTypes';
import UserTypesSelector from '../../components/modules/auth/userTypesSelector';
import {navigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const RegisterProviderScreen: React.FC = props => {
  const [businessName, setBusinessName] = useState<string>('Kudro’s Venture');
  const [rucId, setRucId] = useState<string>('856528');
  const [ownerName, setOwnerName] = useState<string>('Elizabeth Kudro');
  const [email, setEmail] = useState<string>('j.anniston@gmail.com');
  const [phoneNumber, setPhoneNumber] = useState<string>('+1- 2383-989899');
  const [noOfEmployees, setNumberOfEmployees] = useState<string>('5');

  return (
    <ScreenContainer>
      <FocusAwareStatusBar
        barStyle="light-content"
        backgroundColor={Colors.SECONDRY}
      />

      <AuthHeader title="Register as a Provider" />
      <View style={styles.container}>
        <ContentContainer style={{flex: 1}}>
          <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
            <View style={{height: 15}} />
            <AbstractImageBox center />
            <MyText style={{color: Colors.SECONDRY, alignSelf: 'center'}}>
              Upload Your Profile Picture
            </MyText>
            <AbstractTextInput
              value={businessName}
              onChangeText={text => setBusinessName(text)}
              label="Business Name"
            />
            <AbstractTextInput
              value={rucId}
              onChangeText={text => setRucId(text)}
              label="RUC (Business ID)"
            />
            <AbstractTextInput
              value={ownerName}
              onChangeText={text => setOwnerName(text)}
              label="Owner Name"
            />
            <AbstractTextInput
              value={email}
              onChangeText={text => setEmail(text)}
              keyboardType="email-address"
              label="Owner Email"
            />
            <AbstractTextInput
              value={phoneNumber}
              onChangeText={text => setPhoneNumber(text)}
              label="Telephone"
            />
            <AbstractTextInput
              value={noOfEmployees}
              onChangeText={text => setNumberOfEmployees(text)}
              keyboardType="number-pad"
              label="No. of Employees"
            />
            <View style={{height: 10}} />
            <AbstractButton
              onPress={() => navigate('ConfirmationProviderScreen')}
              center
              width="95%"
              title="REGISTER"
            />
          </ScrollView>
        </ContentContainer>
      </View>
    </ScreenContainer>
  );
};
export default RegisterProviderScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    width: '100%',
    alignSelf: 'center',
  },
});
