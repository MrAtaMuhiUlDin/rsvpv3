import React from 'react';
import {StyleSheet} from 'react-native';
import {View} from 'react-native';
import RegistractionConfirmSVG from '../../assets/icons/registrationConfirmSVG';
import AbstractButton from '../../components/abstract/abstractButton';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import MyText from '../../components/abstract/myText';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import {clearAndNavigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const ConfirmationProviderScreen: React.FC = props => {
  return (
    <ScreenContainer>
      <FocusAwareStatusBar
        barStyle="dark-content"
        backgroundColor={Colors.PRIMARY}
      />

      <View style={styles.container}>
        <View style={styles.innerContainer}>
          <RegistractionConfirmSVG size={270} />
        </View>
        <View style={{height: 0}} />
        <MyText style={styles.textStyle}>
          Congradulations, You're a member of RSVP now
        </MyText>
        <View style={{height: 70}} />
        <AbstractButton
          onPress={() => clearAndNavigate('ProviderNavigator')}
          title="OFFER YOUR SPACE/SERVICE"
        />
      </View>
    </ScreenContainer>
  );
};
export default ConfirmationProviderScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '85%',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 25,
    textAlign: 'center',
    color: '#13204D',
    fontWeight: 'bold',
  },
});
