import React from 'react';
import {ImageBackground, Image, StyleSheet, View} from 'react-native';
import AppIconSVG from '../../assets/icons/appIconSVG';
import AbstractButton from '../../components/abstract/abstractButton';
import MyText from '../../components/abstract/myText';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import {navigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const IntroScreen: React.FC = props => {
  return (
    <View style={{flex: 1}}>
      <FocusAwareStatusBar translucent backgroundColor="transparent" />
      <ImageBackground
        style={{width: '100%', height: '100%'}}
        source={require('../../assets/images/introbg.png')}></ImageBackground>
      <View
        style={[
          StyleSheet.absoluteFillObject,
          {backgroundColor: 'rgba(0,0,0,0.6)'},
        ]}>
        <Image source={require('../../assets/images/intromapbg.png')} />
      </View>
      <View style={[StyleSheet.absoluteFillObject]}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 45,
          }}>
          <AppIconSVG size={400} />
        </View>
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <View
            style={{
              width: '80%',
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <AbstractButton
              onPress={() => navigate('LoginScreen')}
              title="ACCEDER"
              width="47%"
            />
            <AbstractButton
              onPress={() => navigate('RegisterUserTypeScreen')}
              btnStyle="transparent"
              title="REGISTRATE"
              width="47%"
            />
          </View>
          <View style={{height: 15}} />
          <MyText
            style={{
              color: Colors.FONT_SECONDRY,
              alignSelf: 'center',
              textDecorationLine: 'underline',
            }}>
            ENTRAR COMO VISITANTE
          </MyText>
          <View style={{height: 20}} />
        </View>
      </View>
    </View>
  );
};
export default IntroScreen;
