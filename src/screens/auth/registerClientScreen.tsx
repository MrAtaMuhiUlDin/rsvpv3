import React from 'react';
import {useState} from 'react';
import {StyleSheet} from 'react-native';
import {ScrollView, View} from 'react-native';
import AbstractButton from '../../components/abstract/abstractButton';
import AbstractImageBox from '../../components/abstract/abstractImageBox';
import ScreenContainer from '../../components/abstract/abstractScreenContainer';
import AbstractTextInput from '../../components/abstract/abstractTextInput';
import ContentContainer from '../../components/abstract/contentContainer';
import MyText from '../../components/abstract/myText';
import FocusAwareStatusBar from '../../components/abstract/statusbarConfiguration';
import AuthHeader from '../../components/modules/auth/authHeader';
import {USER_TYPES} from '../../components/modules/auth/userTypes';
import UserTypesSelector from '../../components/modules/auth/userTypesSelector';
import {navigate} from '../../navigation/authNavigator';
import {Colors} from '../../theme/colors';

const RegisterClientScreen: React.FC = props => {
  const [firstName, setFirstName] = useState<string>('Jennifer');
  const [lastName, setLastName] = useState<string>('Anniston');
  const [email, setEmail] = useState<string>('j.anniston@gmail.com');
  const [phoneNumber, setPhoneNumber] = useState<string>('+1- 2383-989899');

  return (
    <ScreenContainer>
      <FocusAwareStatusBar backgroundColor={Colors.SECONDRY} />
      <AuthHeader title="Tipo de Cuenta" />
      <View style={styles.container}>
        <ContentContainer style={{flex: 1}}>
          <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
            <View style={{height: 15}} />
            <AbstractImageBox center />
            <MyText style={{color: Colors.SECONDRY, alignSelf: 'center'}}>
              Upload Your Profile Picture
            </MyText>
            <AbstractTextInput
              value={firstName}
              onChangeText={text => setFirstName(text)}
              label="First Name"
            />
            <AbstractTextInput
              value={lastName}
              onChangeText={text => setLastName(text)}
              label="Last Name"
            />
            <AbstractTextInput
              value={email}
              onChangeText={text => setEmail(text)}
              keyboardType="email-address"
              label="Email"
            />
            <AbstractTextInput
              value={phoneNumber}
              keyboardType="name-phone-pad"
              onChangeText={text => setPhoneNumber(text)}
              label="Phone"
            />
            <View style={{height: 10}} />
            <AbstractButton
              onPress={() => navigate('ConformationClientScreen')}
              center
              width="95%"
              title="REGISTRO"
            />
          </ScrollView>
        </ContentContainer>
      </View>
    </ScreenContainer>
  );
};
export default RegisterClientScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    width: '100%',
    alignSelf: 'center',
  },
});
