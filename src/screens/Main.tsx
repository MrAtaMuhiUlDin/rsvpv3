import React, { useState } from 'react'
import { StyleSheet,Text, Button, View, TouchableOpacity } from 'react-native'
import AbstractBottomSheet from '../components/abstract/AbstractBottomSheet'
import {Calendar, CalendarList, Agenda} from 'react-native-calendars';
import LeftSvg from '../assets/icons/CalendarSvgs/LeftSvg';
import RightSvg from '../assets/icons/CalendarSvgs/RightSvg';
import ContentContainer from '../components/abstract/contentContainer';
import { moderateScale } from 'react-native-size-matters';
import CompletedJobsCompo from '../components/modules/common/provider/FinancialsCompo/CompletedJobsCompo';
import UploadYourSpaceImage from '../components/modules/common/provider/ProviderAllStepsCompo/UploadYourSpaceImage';
import LinkTextInput from '../components/modules/common/provider/ProviderAllStepsCompo/LinkTextInput';
import SelectOptionsTextInput from '../components/modules/common/provider/ProviderAllStepsCompo/SelectOptionsTextInput';
import CategoryItem from '../components/modules/common/provider/ProviderAllStepsCompo/CategoryItem';
import AbstractHeader from '../components/abstract/SearchCompo/AbstractHeader';
import AddPackagesButton from '../components/modules/common/provider/ProviderAllStepsCompo/AddPackagesButton';
import SimpleTextInput from '../components/modules/common/provider/ProviderAllStepsCompo/SimpleTextInput';
import UploadAnImageSteps from '../components/modules/common/provider/ProviderAllStepsCompo/UploadAnImageSteps';
import ExtraSevicesSimple from '../components/modules/common/provider/ProviderAllStepsCompo/ExtraSevicesSimple';
import ExtraSevicesSimpleWithDetails from '../components/modules/common/provider/ProviderAllStepsCompo/ExtraSevicesSimpleWithDetails';
import TripToIslaCompo from '../components/modules/common/provider/ProviderAllStepsCompo/TripToIslaCompo';
import SelectCategory from '../components/modules/common/provider/ProviderAllStepsCompo/SelectCategory';
import AdditionalFileBtn from '../components/modules/common/provider/ProviderAllStepsCompo/AdditionalPriceBtn';
import SelectAvailability from '../components/modules/common/provider/ProviderAllStepsCompo/SelectAvailability';
import HeadingSteps from '../components/modules/common/provider/ProviderAllStepsCompo/HeadingSteps';
import Banner from '../components/modules/common/provider/ProviderAllStepsCompo/Banner';
import RectangleWithArrowDown from '../components/modules/common/provider/ProviderAllStepsCompo/RectangleWithArrowDown';
import PlaceDetailsCompo from '../components/modules/common/provider/ProviderAllStepsCompo/PlaceDetailsCompo';
import PriceDetailCompo from '../components/modules/common/provider/ProviderAllStepsCompo/PriceDetailCompo';
import CancellationAndPymentCompo from '../components/modules/common/provider/ProviderAllStepsCompo/CancellationAndPymentCompo';
import AddBasicDetailsScreen from './Provider/ProviderStepsScreens/AddBasicDetailsScreen';
import TypesOfEventsScreen from './Provider/ProviderStepsScreens/TypesOfEventsScreen';
import CharacteristicsScreen from './Provider/ProviderStepsScreens/CharacteristicsScreen';
import SelectPriceMethodScreen from './Provider/ProviderStepsScreens/SelectPriceMethodScreen';
import SelectPriceMethodScreenTwo from './Provider/ProviderStepsScreens/SelectPriceMethodScreenTwo';
import SelectAvailabilityScreen from './Provider/ProviderStepsScreens/SelectAvailabilityScreen';
import SelectAvailabilityScreenTwo from './Provider/ProviderStepsScreens/SelectAvailabilityScreenTwo';
import ProviderFinancialScreen from './Provider/ProviderFinancialScreens/ProviderFinancialScreen';
import ProfileFinancialScreen from './Provider/ProviderProfileScreens/ProfileFinancialScreen';
import ProviderBookingConfirmScreen from './Provider/ProviderBookingConfirmScreen';




const Main = () => {
   
    return (
     <View style={styles.mainContainer}>
        
        <ProviderBookingConfirmScreen />

        </View>
    )
}

export default Main

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        // justifyContent:'center',
        // alignItems:'center',
        // backgroundColor:'green'
    }
})
