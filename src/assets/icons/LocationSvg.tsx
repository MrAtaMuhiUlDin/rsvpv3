import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const LocationSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : '#FF5959';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="16" height="22" viewBox="0 0 16 22" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M8 22C13.3333 15.6122 16 10.9455 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 10.9455 2.66667 15.6122 8 22ZM2 8C2 4.68629 4.68629 2 8 2C11.3137 2 14 4.68629 14 8C14 10.0669 12.0122 13.7673 8 18.8323C3.98775 13.7673 2 10.0669 2 8ZM8 9C9.10457 9 10 8.10457 10 7C10 5.89543 9.10457 5 8 5C6.89543 5 6 5.89543 6 7C6 8.10457 6.89543 9 8 9Z" fill="${color}"/>
      </svg>
	  
`}
    />
  );
};
export default LocationSvg;
