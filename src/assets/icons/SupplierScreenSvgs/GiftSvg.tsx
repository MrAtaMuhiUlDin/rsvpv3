import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const GiftSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="19" height="20" viewBox="0 0 19 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15.6428 6.2738H3.47613C2.51621 6.2738 1.73804 7.05198 1.73804 8.0119V16.7024C1.73804 17.6623 2.51621 18.4405 3.47613 18.4405H15.6428C16.6027 18.4405 17.3809 17.6623 17.3809 16.7024V8.0119C17.3809 7.05198 16.6027 6.2738 15.6428 6.2738Z" stroke="#323643" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M9.5595 3.66671C9.5595 3.15107 9.40659 2.64701 9.12012 2.21826C8.83364 1.78952 8.42646 1.45536 7.95007 1.25803C7.47368 1.0607 6.94947 1.00907 6.44373 1.10967C5.93799 1.21026 5.47345 1.45857 5.10883 1.82319C4.74421 2.1878 4.49591 2.65235 4.39531 3.15809C4.29471 3.66382 4.34634 4.18803 4.54367 4.66442C4.741 5.14082 5.07517 5.548 5.50391 5.83447C5.93265 6.12095 6.43671 6.27386 6.95236 6.27386" stroke="#323643" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M12.1667 6.27386C12.6824 6.27386 13.1864 6.12095 13.6152 5.83447C14.0439 5.548 14.3781 5.14082 14.5754 4.66442C14.7727 4.18803 14.8244 3.66382 14.7238 3.15809C14.6232 2.65235 14.3749 2.1878 14.0102 1.82319C13.6456 1.45857 13.1811 1.21026 12.6753 1.10967C12.1696 1.00907 11.6454 1.0607 11.169 1.25803C10.6926 1.45536 10.2854 1.78952 9.99895 2.21826C9.71248 2.64701 9.55957 3.15107 9.55957 3.66671" stroke="#323643" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M9.55957 3.66669V18.4405" stroke="#323643" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M17.3809 12.3572H1.73804" stroke="#323643" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
`}
    />
  );
};
export default GiftSvg;
