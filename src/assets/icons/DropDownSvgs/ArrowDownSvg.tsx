import React from 'react';
import {SvgXml} from 'react-native-svg';
import {View} from 'react-native'

interface Props {
  size?: number;
  color?: string;
}

const ArrowDownSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <View style={{width:50,height:50,justifyContent:'center',alignItems:'center'}}>
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="10" height="7" viewBox="0 0 10 7" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M5 5.88722L5.40625 5.50094L8.90625 2.16761L8.09375 1.11292L5 4.06L1.90625 1.11292L1.09375 2.16761L4.59375 5.50094L5 5.88722Z" fill="#A9B1BA" stroke="#A9B1BA" stroke-width="0.3"/>
      </svg>
          
`}
    />
    </View>
  );
};
export default ArrowDownSvg;
