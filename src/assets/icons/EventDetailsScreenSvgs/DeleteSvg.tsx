import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const DeleteSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="11" height="15" viewBox="0 0 11 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0.964286 12.6964C0.964286 13.552 1.60714 14.252 2.39286 14.252H8.10714C8.89286 14.252 9.53571 13.552 9.53571 12.6964V3.36306H0.964286V12.6964ZM10.25 1.02973H7.75L7.03571 0.251953H3.46429L2.75 1.02973H0.25V2.58529H10.25V1.02973Z" fill="#FF2424"/>
      </svg>
                      
`}
    />
  );
};
export default DeleteSvg;
