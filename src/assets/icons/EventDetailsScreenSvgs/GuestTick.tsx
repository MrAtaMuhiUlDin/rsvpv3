import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const GuestTick: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="13" height="7" viewBox="0 0 13 7" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M9.83527 0.736018L9.04688 0L5.50194 3.30947L6.29032 4.04549L9.83527 0.736018ZM12.206 0L6.29032 5.52274L3.95312 3.34601L3.16473 4.08203L6.29032 7L13 0.736018L12.206 0ZM0 4.08203L3.12559 7L3.91398 6.26398L0.793979 3.34601L0 4.08203Z" fill="white"/>
      </svg>             
`}
    />
  );
};
export default GuestTick;
