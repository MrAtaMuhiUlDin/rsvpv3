import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const EditSvgs: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0 8.31396V10.5015H2.1875L8.63917 4.04979L6.45167 1.86229L0 8.31396ZM10.3308 2.35812C10.5583 2.13063 10.5583 1.76313 10.3308 1.53563L8.96583 0.170625C8.73833 -0.056875 8.37083 -0.056875 8.14333 0.170625L7.07583 1.23812L9.26333 3.42562L10.3308 2.35812Z" fill="black"/>
      </svg>
      
`}
    />
  );
};
export default EditSvgs;
