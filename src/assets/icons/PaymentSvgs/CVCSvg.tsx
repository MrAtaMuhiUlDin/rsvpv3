import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const CVCSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="26" height="19" viewBox="0 0 26 19" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M22.5265 1H3.26779C2.08598 1 1.12793 1.95139 1.12793 3.125V15.875C1.12793 17.0486 2.08598 18 3.26779 18H22.5265C23.7083 18 24.6664 17.0486 24.6664 15.875V3.125C24.6664 1.95139 23.7083 1 22.5265 1Z" stroke="#D9D0E3" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M1.12805 5.57715H24.6665" stroke="#D9D0E3" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
      <rect x="11.1537" y="8.41016" width="9.15385" height="3.92308" rx="1.96154" stroke="#F24E1E" stroke-width="2"/>
      </svg>
              
`}
    />
  );
};
export default CVCSvg;
