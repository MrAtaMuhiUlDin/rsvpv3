import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const SimplePlus: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M11.5871 6.79183H6.83712V11.5418H5.25378V6.79183H0.503784V5.2085H5.25378V0.458496H6.83712V5.2085H11.5871V6.79183Z" fill="#FF5959"/>
      </svg>
      
  `}
    />
  );
};
export default SimplePlus;
