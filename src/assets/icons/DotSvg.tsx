import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const DotSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : '#13204D';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="5" height="5" viewBox="0 0 5 5" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="2.5" cy="2.5" r="2.5" fill="${color}"/>
      </svg>          
      
  `}
    />
  );
};
export default DotSvg;
