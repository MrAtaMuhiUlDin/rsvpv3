import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const ArrowDownSVG: React.FC<Props> = props => {
  const size = props.size ? props.size : undefined;
  const color = props.color ? props.color : 'black';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="9" height="6" viewBox="0 0 9 6" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1.02493 0.00534717L0.0228125 0.996746L4.24153 5.26109L8.50588 1.04237L7.51448 0.0402503L4.25229 3.2605L1.02493 0.00534717Z" fill="${color}"/>
      </svg>      
      
  `}
    />
  );
};
export default ArrowDownSVG;
