import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const SeenTick: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.53284 8.02876C1.79392 7.76841 2.2172 7.76841 2.47828 8.02876L5.15237 10.6954C5.41344 10.9558 5.41344 11.3779 5.15237 11.6382C4.89129 11.8986 4.46801 11.8986 4.20693 11.6382L1.53284 8.97157C1.27177 8.71122 1.27177 8.28911 1.53284 8.02876Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.21253 8.69526C6.4736 8.43491 6.89689 8.43491 7.15796 8.69526L9.16353 10.6953C9.42461 10.9556 9.42461 11.3777 9.16353 11.6381C8.90246 11.8984 8.47917 11.8984 8.2181 11.6381L6.21253 9.63807C5.95146 9.37772 5.95146 8.95561 6.21253 8.69526Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5005 5.36177C10.7616 5.62212 10.7616 6.04423 10.5005 6.30457L5.15235 11.6379C4.89127 11.8983 4.46799 11.8983 4.20691 11.6379C3.94584 11.3776 3.94584 10.9554 4.20691 10.6951L9.5551 5.36177C9.81617 5.10142 10.2395 5.10142 10.5005 5.36177Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.5117 5.36177C14.7728 5.62212 14.7728 6.04423 14.5117 6.30457L9.16352 11.6379C8.90244 11.8983 8.47916 11.8983 8.21808 11.6379C7.95701 11.3776 7.95701 10.9554 8.21808 10.6951L13.5663 5.36177C13.8273 5.10142 14.2506 5.10142 14.5117 5.36177Z" fill="#005FFF"/>
      </svg>         
`}
    />
  );
};
export default SeenTick;
