import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const AddPictureSVG: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      width={size}
      height={size}
      xml={`<svg width="39" height="40" viewBox="0 0 39 40" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M6.50098 8.77734H27.626V20.1523H30.876V8.77734C30.876 6.98497 29.4184 5.52734 27.626 5.52734H6.50098C4.7086 5.52734 3.25098 6.98497 3.25098 8.77734V28.2773C3.25098 30.0697 4.7086 31.5273 6.50098 31.5273H19.501V28.2773H6.50098V8.77734Z" fill="${color}"/>
  <path d="M13.001 18.5273L8.12598 25.0273H26.001L19.501 15.2773L14.626 21.7773L13.001 18.5273Z" fill="${color}"/>
  <path d="M30.876 23.4023H27.626V28.2773H22.751V31.5273H27.626V36.4023H30.876V31.5273H35.751V28.2773H30.876V23.4023Z" fill="${color}"/>
  </svg>
  `}
    />
  );
};
export default AddPictureSVG;
