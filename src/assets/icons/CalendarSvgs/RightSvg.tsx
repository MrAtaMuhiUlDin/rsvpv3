import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const RightSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M2.2834 0L0 2.2834L7.417 9.7166L0 17.1498L2.2834 19.4332L12 9.7166L2.2834 0Z" fill="#EF4339"/>
      </svg>
          
`}
    />
  );
};
export default RightSvg;
