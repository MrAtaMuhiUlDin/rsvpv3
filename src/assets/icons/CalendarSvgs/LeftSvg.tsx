import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const LeftSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="12" height="20" viewBox="0 0 12 20" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M9.7166 0L12 2.2834L4.583 9.7166L12 17.1498L9.7166 19.4332L0 9.7166L9.7166 0Z" fill="#EF4339"/>
      </svg>        
`}
    />
  );
};
export default LeftSvg;
