import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const SearchBarSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="18" height="17" viewBox="0 0 18 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M15.1474 7.32217C15.1474 3.28311 11.8624 0 7.82369 0C3.78384 0 0.5 3.28429 0.5 7.32217C0.5 11.3589 3.7862 14.6443 7.82369 14.6443L8.1381 14.6377L8.45017 14.6178C9.79691 14.5031 11.0684 14.0205 12.1442 13.2324L12.262 13.141L15.8339 16.7117L15.9313 16.797C16.1018 16.9277 16.3114 17 16.5245 17C16.7839 17 17.0368 16.8969 17.2217 16.705L17.3012 16.6147L17.3675 16.5169C17.586 16.1464 17.5365 15.6518 17.2151 15.3304L13.644 11.76L13.735 11.6419L13.9115 11.3903C14.7094 10.2003 15.1474 8.79411 15.1474 7.32217ZM2.45453 7.32217C2.45453 4.36413 4.86233 1.95787 7.82013 1.95787C10.7789 1.95787 13.1857 4.36514 13.1857 7.32217C13.1857 10.28 10.7782 12.69 7.82013 12.69C4.8631 12.69 2.45453 10.281 2.45453 7.32217Z" fill="#848897"/>
      </svg>
      
`}
    />
  );
};
export default SearchBarSvg;
