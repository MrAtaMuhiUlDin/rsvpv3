import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const TwoArrows: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="9" height="9" viewBox="0 0 9 9" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M8.99963 4V0H4.99963L6.64463 1.645L1.64463 6.645L-0.000366211 5V9H3.99963L2.35463 7.355L7.35463 2.355L8.99963 4Z" fill="white"/>
      </svg>          
`}
    />
  );
};
export default TwoArrows;
