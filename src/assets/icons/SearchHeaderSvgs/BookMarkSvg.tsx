import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const BookMarkSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M12.4444 13.9091L14 14.7273V1.63636C14 0.736364 13.3 0 12.4444 0H4.65889C3.80333 0 3.11111 0.736364 3.11111 1.63636H10.8889C11.7444 1.63636 12.4444 2.37273 12.4444 3.27273V13.9091ZM9.33333 3.27273H1.55556C0.7 3.27273 0 4.00909 0 4.90909V18L5.44444 15.5455L10.8889 18V4.90909C10.8889 4.00909 10.1889 3.27273 9.33333 3.27273Z" fill="#E6E6E7"/>
      </svg>      
`}
    />
  );
};
export default BookMarkSvg;
