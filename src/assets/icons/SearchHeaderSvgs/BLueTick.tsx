import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  width?: number;
  height?:number
  color?: string;
}

const BLueTick: React.FC<Props> = props => {
  const defaultHeight = props.height ? props.height : 14;
  const defaultWidth = props.width ? props.width : 18
  const color = props.color ? props.color : '#3389EE';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="${defaultWidth}" height="${defaultHeight}" viewBox="0 0 18 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M5.59 10.58L1.42 6.41L0 7.82L5.59 13.41L17.59 1.41L16.18 0L5.59 10.58Z" fill="${color}"/>
      </svg>
          
`}
    />
  );
};
export default BLueTick;
