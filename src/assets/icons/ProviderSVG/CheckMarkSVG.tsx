import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const CheckMark: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M3.53846 7.52827C3.79953 7.26792 4.22282 7.26792 4.48389 7.52827L7.15799 10.1949C7.41906 10.4553 7.41906 10.8774 7.15799 11.1377C6.89691 11.3981 6.47363 11.3981 6.21255 11.1377L3.53846 8.47108C3.27738 8.21073 3.27738 7.78862 3.53846 7.52827Z" fill="#7A7A7A"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M12.5061 4.86225C12.7672 5.1226 12.7672 5.54471 12.5061 5.80506L7.15796 11.1384C6.89689 11.3987 6.4736 11.3987 6.21253 11.1384C5.95146 10.878 5.95146 10.4559 6.21253 10.1956L11.5607 4.86225C11.8218 4.6019 12.2451 4.6019 12.5061 4.86225Z" fill="#7A7A7A"/>
      </svg>
  `}
    />
  );
};
export default CheckMark;
