import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const PencilSvg: React.FC<Props> = props => {
  // const size = props.size ? props.size : 12;
  // const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M0 11.8754V15H2.91626L11.5173 5.78461L8.60103 2.66005L0 11.8754ZM13.7725 3.36828C14.0758 3.04333 14.0758 2.5184 13.7725 2.19345L11.9528 0.243716C11.6495 -0.0812387 11.1596 -0.0812387 10.8563 0.243716L9.43314 1.7685L12.3494 4.89307L13.7725 3.36828Z" fill="#FF5959"/>
      </svg>       
  `}
    />
  );
};
export default PencilSvg;
