import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const ArrowRightWhite: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M7.91879 6.28711L1.77551 0.71954C1.36596 0.348369 0.751629 0.348369 0.342076 0.71954C-0.067476 1.09071 -0.067476 1.64747 0.342076 2.01864L5.76864 6.93666L0.342076 11.8547C-0.067476 12.2258 -0.067476 12.7826 0.342076 13.1538C0.546852 13.3394 0.854017 13.4322 1.05879 13.4322C1.26357 13.4322 1.57073 13.3394 1.77551 13.1538L7.91879 7.58621C8.32835 7.21504 8.32835 6.65828 7.91879 6.28711Z" fill="white"/>
      </svg>
  `}
    />
  );
};
export default ArrowRightWhite;
