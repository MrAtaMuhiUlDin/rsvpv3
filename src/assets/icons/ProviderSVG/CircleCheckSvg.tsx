import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const CircleCheckSvg: React.FC<Props> = props => {
  // const size = props.size ? props.size : 12;
  // const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <mask id="mask0" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="16" height="16">
      <rect width="16" height="16" fill="white"/>
      </mask>
      <g mask="url(#mask0)">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.33325 7.99992C1.33325 4.31992 4.31992 1.33325 7.99992 1.33325C11.6799 1.33325 14.6666 4.31992 14.6666 7.99992C14.6666 11.6799 11.6799 14.6666 7.99992 14.6666C4.31992 14.6666 1.33325 11.6799 1.33325 7.99992C1.33325 7.99992 1.33325 4.31992 1.33325 7.99992ZM6.66659 9.4466L11.0599 5.05326L11.9999 5.99993L6.66659 11.3333L3.99992 8.6666L4.93992 7.7266L6.66659 9.4466Z" fill="#323643"/>
      </g>
      </svg>      
  `}
    />
  );
};
export default CircleCheckSvg;
