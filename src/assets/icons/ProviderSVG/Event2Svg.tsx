import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const Event2Svg: React.FC<Props> = props => {
  const size = props.size ? props.size : 26;
  const color = props.color ? props.color : 'black';
  return (
    <SvgXml
      width={size}
      height={size}
      color={color}
      xml={`<svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M3.33333 7.5H5V9.16667H3.33333V7.5ZM15 3.33333V15C15 15.9167 14.25 16.6667 13.3333 16.6667H1.66667C0.741667 16.6667 0 15.9167 0 15L0.00833333 3.33333C0.00833333 2.41667 0.741667 1.66667 1.66667 1.66667H2.5V0H4.16667V1.66667H10.8333V0H12.5V1.66667H13.3333C14.25 1.66667 15 2.41667 15 3.33333ZM1.66667 5H13.3333V3.33333H1.66667V5ZM13.3333 15V6.66667H1.66667V15H13.3333ZM10 9.16667H11.6667V7.5H10V9.16667ZM6.66667 9.16667H8.33333V7.5H6.66667V9.16667Z" fill="black"/>
      </svg>      
  `}
    />
  );
};
export default Event2Svg;
