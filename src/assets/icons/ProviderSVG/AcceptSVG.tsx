import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const Accept: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.5 16.1194L7.875 13.2985L7 14.2388L10.5 18L18 9.9403L17.125 9L10.5 16.1194Z" fill="#26BA1A"/>
      <path d="M23.5 12.5C23.5 19.1469 18.3322 24.5 12 24.5C5.66777 24.5 0.5 19.1469 0.5 12.5C0.5 5.85315 5.66777 0.5 12 0.5C18.3322 0.5 23.5 5.85315 23.5 12.5Z" stroke="#26BA1A"/>
      </svg>
  `}
    />
  );
};
export default Accept;
