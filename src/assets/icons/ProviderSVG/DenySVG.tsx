import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const DenySVG: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M17 9.00714L15.9929 8L12 11.9929L8.00714 8L7 9.00714L10.9929 13L7 16.9929L8.00714 18L12 14.0071L15.9929 18L17 16.9929L13.0071 13L17 9.00714Z" fill="#D60000"/>
      <path d="M0.499999 12.5C0.5 5.85315 5.66777 0.5 12 0.500001C18.3322 0.500001 23.5 5.85315 23.5 12.5C23.5 19.1469 18.3322 24.5 12 24.5C5.66776 24.5 0.499999 19.1468 0.499999 12.5Z" stroke="#D60000"/>
      </svg>      
  `}
    />
  );
};
export default DenySVG;
