import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const DustBinSVg: React.FC<Props> = props => {
  // const size = props.size ? props.size : 12;
  const color = props.color ? props.color : '#FF5959';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="14" height="19" viewBox="0 0 14 19" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M1 16.499C1 17.599 1.9 18.499 3 18.499H11C12.1 18.499 13 17.599 13 16.499V4.49902H1V16.499ZM3 6.49902H11V16.499H3V6.49902ZM10.5 1.49902L9.5 0.499023H4.5L3.5 1.49902H0V3.49902H14V1.49902H10.5Z" fill="${color}"/>
      </svg>
  `}
    />
  );
};
export default DustBinSVg
;
