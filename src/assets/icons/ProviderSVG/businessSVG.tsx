import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const BusinessSVG: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="30" height="26" viewBox="0 0 30 26" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M15 5.91667V0.25H0.833374V25.75H29.1667V5.91667H15ZM6.50004 22.9167H3.66671V20.0833H6.50004V22.9167ZM6.50004 17.25H3.66671V14.4167H6.50004V17.25ZM6.50004 11.5833H3.66671V8.75H6.50004V11.5833ZM6.50004 5.91667H3.66671V3.08333H6.50004V5.91667ZM12.1667 22.9167H9.33337V20.0833H12.1667V22.9167ZM12.1667 17.25H9.33337V14.4167H12.1667V17.25ZM12.1667 11.5833H9.33337V8.75H12.1667V11.5833ZM12.1667 5.91667H9.33337V3.08333H12.1667V5.91667ZM26.3334 22.9167H15V20.0833H17.8334V17.25H15V14.4167H17.8334V11.5833H15V8.75H26.3334V22.9167ZM23.5 11.5833H20.6667V14.4167H23.5V11.5833ZM23.5 17.25H20.6667V20.0833H23.5V17.25Z" fill="black"/>
      </svg>
  `}
    />
  );
};
export default BusinessSVG;
