import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const Event1SVG: React.FC<Props> = props => {
  const size = props.size ? props.size : 26;
  const color = props.color ? props.color : 'black';
  return (
    <SvgXml
      width={size}
      height={size}
      color={color}
      xml={`<svg width="23" height="26" viewBox="0 0 23 26" fill="${color}" xmlns="http://www.w3.org/2000/svg">
      <path d="M5.11111 11.7H7.66667V14.3H5.11111V11.7ZM23 5.2V23.4C23 24.83 21.85 26 20.4444 26H2.55556C1.13722 26 0 24.83 0 23.4L0.0127778 5.2C0.0127778 3.77 1.13722 2.6 2.55556 2.6H3.83333V0H6.38889V2.6H16.6111V0H19.1667V2.6H20.4444C21.85 2.6 23 3.77 23 5.2ZM2.55556 7.8H20.4444V5.2H2.55556V7.8ZM20.4444 23.4V10.4H2.55556V23.4H20.4444ZM15.3333 14.3H17.8889V11.7H15.3333V14.3ZM10.2222 14.3H12.7778V11.7H10.2222V14.3Z" fill="white"/>
      </svg>   
  `}
    />
  );
};
export default Event1SVG;
