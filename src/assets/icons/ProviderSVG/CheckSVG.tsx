import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const AddPictureSVG: React.FC<Props> = props => {
  // const size = props.size ? props.size : 12;
  // const color = props.color ? props.color : 'white';
  return (
    <SvgXml
      // width={size}
      // height={size}
      xml={`<svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M1.53717 7.52827C1.79897 7.26792 2.22344 7.26792 2.48524 7.52827L5.16678 10.1949C5.42858 10.4553 5.42858 10.8774 5.16678 11.1377C4.90498 11.3981 4.48051 11.3981 4.21871 11.1377L1.53717 8.47108C1.27537 8.21073 1.27537 7.78862 1.53717 7.52827Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M6.2298 8.19526C6.4916 7.93491 6.91606 7.93491 7.17787 8.19526L9.18902 10.1953C9.45082 10.4556 9.45082 10.8777 9.18902 11.1381C8.92722 11.3984 8.50275 11.3984 8.24095 11.1381L6.2298 9.13807C5.968 8.87772 5.968 8.45561 6.2298 8.19526Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5298 4.86225C10.7916 5.1226 10.7916 5.54471 10.5298 5.80506L5.16676 11.1384C4.90496 11.3987 4.48049 11.3987 4.21869 11.1384C3.95689 10.878 3.95689 10.4559 4.21869 10.1956L9.58177 4.86225C9.84357 4.6019 10.268 4.6019 10.5298 4.86225Z" fill="#005FFF"/>
      <path fill-rule="evenodd" clip-rule="evenodd" d="M14.5522 4.86225C14.814 5.1226 14.814 5.54471 14.5522 5.80506L9.1891 11.1384C8.9273 11.3987 8.50283 11.3987 8.24103 11.1384C7.97923 10.878 7.97923 10.4559 8.24103 10.1956L13.6041 4.86225C13.8659 4.6019 14.2904 4.6019 14.5522 4.86225Z" fill="#005FFF"/>
      </svg>
  `}
    />
  );
};
export default AddPictureSVG;
