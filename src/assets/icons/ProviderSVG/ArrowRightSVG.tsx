import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const ArrowRight: React.FC<Props> = props => {
//   const size = props.size ? props.size : 12;
//   const color = props.color ? props.color : 'white';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="6" height="12" viewBox="0 0 6 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M0.199555 11.8246C0.420417 12.0585 0.777976 12.0585 0.998273 11.8246L5.66913 6.86648C6.11029 6.39818 6.11029 5.63848 5.66913 5.17019L0.964381 0.175476C0.745778 -0.0559717 0.392739 -0.0589697 0.171312 0.16948C-0.0546335 0.402727 -0.0574578 0.787074 0.165099 1.02392L4.47106 5.59411C4.69192 5.82856 4.69192 6.20811 4.47106 6.44255L0.199555 10.9768C-0.0213065 11.2106 -0.0213065 11.5908 0.199555 11.8246Z" fill="#323643"/>
      </svg>
  `}
    />
  );
};
export default ArrowRight;
