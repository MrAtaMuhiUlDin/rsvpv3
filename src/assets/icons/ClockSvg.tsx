import React from 'react';
import {SvgXml} from 'react-native-svg';

interface Props {
  size?: number;
  color?: string;
}

const ClockSvg: React.FC<Props> = props => {
  const size = props.size ? props.size : 12;
  const color = props.color ? props.color : '#FF5959';
  return (
    <SvgXml
    //   width={size}
    //   height={size}
      xml={`<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
	  <path fill-rule="evenodd" clip-rule="evenodd" d="M22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2C17.5228 2 22 6.47715 22 12ZM4 12C4 16.4183 7.58172 20 12 20C16.4183 20 20 16.4183 20 12C20 7.58172 16.4183 4 12 4C7.58172 4 4 7.58172 4 12ZM11 7C11 6.44772 11.4477 6 12 6C12.5523 6 13 6.44772 13 7V11.636L15.98 15.1874C16.335 15.6105 16.2798 16.2413 15.8567 16.5963C15.4337 16.9513 14.8029 16.8961 14.4479 16.473L11.238 12.6477C11.1933 12.5951 11.1539 12.5378 11.1207 12.4767C11.0661 12.3761 11.0282 12.2652 11.0108 12.1476C11.0032 12.0965 10.9996 12.045 11 11.9937M11 7V11.9937V7Z" fill="${color}"/>
	  </svg>
	  
`}
    />
  );
};
export default ClockSvg;
