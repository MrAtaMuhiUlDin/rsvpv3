export const USER_TYPES = [
  {
    _id: '123456789',
    source: {uri: 'https://picsum.photos/200/300?grayscale'},
    title: 'Clients',
    selected: false,
  },
  {
    _id: '234567890',
    source: {uri: 'https://picsum.photos/200/300?grayscale'},
    title: 'Proveedor',
    selected: true,
  },
];
