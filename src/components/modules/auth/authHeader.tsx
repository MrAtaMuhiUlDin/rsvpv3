import React from 'react';
import {StyleSheet, View} from 'react-native';
import ChevronLeftSVG from '../../../assets/icons/chevronLeftSVG';
import {goBack} from '../../../navigation/authNavigator';
import {Colors} from '../../../theme/colors';
import AbstractIconButton from '../../abstract/abstractIconButton';
import ContentContainer from '../../abstract/contentContainer';
import MyText from '../../abstract/myText';

interface Props {
  title?: string;
}

const AuthHeader: React.FC<Props> = ({title}) => {
  return (
    <View style={[{backgroundColor: Colors.SECONDRY}]}>
      <ContentContainer>
        <View style={[styles.container]}>
          <AbstractIconButton
            onPress={() => goBack()}
            size={45}
            Icon={props => <ChevronLeftSVG {...props} />}
          />
          <MyText style={styles.textStyle}>{title}</MyText>
        </View>
      </ContentContainer>
    </View>
  );
};
export default AuthHeader;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 200,
    backgroundColor: Colors.SECONDRY,
    paddingTop: 20,
    paddingBottom: 20,
    justifyContent: 'space-between',
  },
  textStyle: {
    color: Colors.FONT_SECONDRY,
    fontWeight: '700',
    fontSize: 30,
    width: 170,
  },
});
