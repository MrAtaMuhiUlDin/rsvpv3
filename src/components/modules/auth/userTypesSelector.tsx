import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {navigate} from '../../../navigation/authNavigator';
import UserTypeSelectItem from './userTypeSelectItem';

interface ISingleUserType {
  _id: string;
  source: {uri: string};
  title: string;
  selected: boolean;
}

interface Props {
  userTypes: ISingleUserType[];
  onChange?: (typeId: string) => void;
}

const UserTypesSelector: React.FC<Props> = props => {
  const [uTypes, setUserTyps] = useState<ISingleUserType[]>([]);

  useEffect(() => {
    setUserTyps(props.userTypes);
  }, []);

  const handleOnChange = (itemId: string) => {
    let items: ISingleUserType[] = uTypes.map(i => ({
      ...i,
      selected: i._id === itemId,
    }));
    setUserTyps(items);
    if (props.onChange) props.onChange(itemId);
  };

  return (
    <View style={[styles.container]}>
      {uTypes.map(ut => (
        <UserTypeSelectItem
          key={ut._id}
          _id={ut._id}
          title={ut.title}
          source={ut.source}
          selected={ut.selected}
          onChange={itemId => handleOnChange(itemId)}
        />
      ))}
    </View>
  );
};
export default UserTypesSelector;

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
});
