import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {Colors} from '../../../theme/colors';
import AbstractRadio from '../../abstract/abstractRadio';
import MyText from '../../abstract/myText';

interface Props {
  _id: string;
  source: {uri: string};
  title: string;
  selected?: boolean;
  onChange?: (_id: string) => void;
}

const UserTypeSelectItem: React.FC<Props> = ({
  _id,
  source,
  selected,
  onChange,
  title,
}) => {
  return (
    <ImageBackground
      source={source}
      resizeMode="cover"
      style={[styles.container]}>
      <TouchableOpacity
        onPress={() => onChange?.(_id)}
        style={[styles.touchStyle]}>
        <View style={[styles.innerContainer]}>
          <MyText style={styles.textStyle}>{title}</MyText>
          <AbstractRadio onPress={() => onChange?.(_id)} selected={selected} />
        </View>
      </TouchableOpacity>
    </ImageBackground>
  );
};
export default UserTypeSelectItem;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignSelf: 'center',
    borderRadius: 5,
    height: 130,
    backgroundColor: Colors.SECONDRY,
    alignItems: 'center',
    overflow: 'hidden',
    marginTop: 10,
  },
  touchStyle: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  innerContainer: {
    width: '90%',
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  textStyle: {
    color: Colors.FONT_SECONDRY,
    fontSize: 20,
    fontWeight: '600',
  },
});
