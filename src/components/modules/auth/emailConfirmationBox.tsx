import React from 'react';
import {Dimensions, View} from 'react-native';
import AbstractButton from '../../abstract/abstractButton';
import AbstractTextInput from '../../abstract/abstractTextInput';
import MyText from '../../abstract/myText';
import Modal from 'react-native-modal';
import {useWindowDimensions} from 'react-native';
import {Colors} from '../../../theme/colors';
import {useState} from 'react';
import {useEffect} from 'react';

interface Props {
  visible?: boolean;
  onSubmit?: (email: string) => void;
  onCancel?: () => void;
}

const EmailConfirmationBox: React.FC<Props> = ({
  visible,
  onSubmit,
  onCancel,
}) => {
  const [isVisible, setVisible] = useState(false);
  const [email, setEmail] = useState('rsvp@rsvp.com');
  useEffect(() => {
    setVisible(visible ? true : false);
  }, [visible]);

  const handleOnCancel = () => {
    if (onCancel) onCancel();
    setVisible(false);
    setEmail('');
  };
  const handleSubmit = () => {
    if (onSubmit) onSubmit(email);
    setVisible(false);
    setEmail('');
  };

  // const {height} = useWindowDimensions()
  return (
    <Modal
      isVisible={isVisible}
      onBackButtonPress={() => setVisible(false)}
      backdropOpacity={0.5}
      backdropColor={'black'}
      animationIn={'zoomIn'}
      animationOut={'zoomOut'}
      coverScreen={false}
      backdropTransitionOutTiming={0}>
      <View
        style={{
          width: '100%',
          alignSelf: 'center',
          backgroundColor: Colors.PRIMARY,
          height: 250,
          padding: 10,
          paddingLeft: 20,
          paddingRight: 20,
          borderRadius: 10,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <MyText style={{fontWeight: 'bold', fontSize: 18, textAlign: 'center'}}>
          Olvidaste tu contraseña?
        </MyText>
        <View style={{height: 10}} />
        <MyText style={{fontSize: 18, textAlign: 'center'}}>
          Entra el correo a donde enviaremos un link para cambiar tu contraseña
        </MyText>
        <AbstractTextInput
          value={email}
          onChangeText={text => setEmail(text)}
          label="EMAIL"
        />
        <View style={{height: 10}} />
        <AbstractButton onPress={() => handleSubmit()} title="ENVIAR" />
      </View>
    </Modal>
  );
};
export default EmailConfirmationBox;
