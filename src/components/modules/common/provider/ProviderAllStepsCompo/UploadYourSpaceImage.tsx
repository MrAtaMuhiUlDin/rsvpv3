import React from 'react'
import { StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AddPictureSVG from '../../../../../assets/icons/addPictureSVG'

interface Props {
    
}

const UploadYourSpaceImage = (props: Props) => {
    return (
        <TouchableOpacity style={styles.mainContainer}>
           <View style={styles.viewOne}> 

          <AddPictureSVG size={30} />   
          <Text style={{fontFamily:'OpenSans',fontWeight:'600',fontSize:moderateScale(11,0.1),color:'white',marginTop:5}} >Upload Your Space Image</Text>
        
           </View>
        </TouchableOpacity>
    )
}

export default UploadYourSpaceImage

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(205,0.1),
        backgroundColor:'#FF5959',
        borderRadius:9,
        justifyContent:'center',
        alignItems:'center',

    },
    viewOne:{
        width:'50%',
        height:'50%',
        // backgroundColor:'pink',
        justifyContent:'center',
        alignItems:'center'
        
    }
})
