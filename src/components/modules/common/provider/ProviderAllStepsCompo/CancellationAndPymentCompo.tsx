import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'






const SimpleFieldOne = () => {
    return (
        <View style={{width:'100%',height:'20%',flexDirection:'row'}}>
        <View style={{width:'60%',height:'100%',paddingLeft:22}}>
            <Text style={styles.textOne}>Reservation Expiry</Text>
        </View>
        <View style={{width:'40%',height:'100%'}}>
            <Text style={styles.textTwo}>After  3 Days</Text>
        </View>
        </View>
    )
}




const SimpleFieldTwo = () => {
    return (
        <View style={{width:'100%',height:'60%',flexDirection:'row'}}>
        <View style={{width:'60%',height:'100%',paddingLeft:22}}>
        <Text style={styles.textOne}>Reservation Expiry</Text>
        </View>
        <View style={{width:'40%',height:'100%'}}>
        <Text numberOfLines={6} style={styles.textTwo}>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sunt praesentium sapiente, sit ea quae harum recusandae, cum repudiandae eaque excepturi tempore iure! Nam facere dicta recusandae, itaque dolor quae amet?</Text>
        </View>
        </View>
    )
}




const CancellationAndPymentCompo = () => {
    return (
        <View style={styles.mainContainer}>
        
        <SimpleFieldOne />
        <SimpleFieldTwo />
        <SimpleFieldOne />


        </View>
    )
}

export default CancellationAndPymentCompo

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(145,0.1),
        // backgroundColor:'red'
    },
    textOne:{
        color:'rgba(134, 142, 150, 1)',
        fontFamily:'OpenSans',
        fontSize:moderateScale(11,0.1),
        fontWeight:'600'
    },
    textTwo:{
        color:'rgba(51, 51, 51, 1)',
        fontFamily:'OpenSans',
        fontSize:moderateScale(11,0.1),
        fontWeight:'400'
    }
})
