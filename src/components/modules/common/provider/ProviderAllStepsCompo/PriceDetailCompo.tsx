import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import DotSvg from '../../../../../assets/icons/DotSvg'




const FieldsCompo = () => {

    return (
        <View style={{ width: '100%', height: '25%', flexDirection: 'row' }}>
            <View style={{ width: '40%', height: '100%', justifyContent: 'center', flexDirection: 'row', alignItems: 'center' }}>
                <DotSvg color={'rgba(134, 142, 150, 1)'} />
                <Text style={styles.textOne}> Package 1 </Text>
            </View>

            <View style={{ width: '60%', height: '100%', justifyContent: 'center',paddingLeft:10  }}>
                <Text style={styles.textTwo}>71 ABCD Street, NYC, Arizona</Text>
            </View>
        </View>
    )
}



const PriceDetailCompo: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={{ width: '100%', height: '25%',justifyContent: 'center',paddingLeft:18 }}>
                    <Text style={styles.textOne}>Prices</Text>
                </View>
                <FieldsCompo />
                <FieldsCompo />
                <View style={{ width: '100%', height: '25%',flexDirection:'row' }}>
                    <View style={{ width: '40%', height: '100%', flexDirection: 'row', alignItems: 'center',paddingLeft:18 }}>
                        <Text style={styles.textOne}> Available Days</Text>
                    </View>

                    <View style={{ width: '60%', height: '100%', justifyContent: 'center',paddingLeft:10 }}>
                        <Text style={styles.textTwo}>71 ABCD Street, NYC, Arizona</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default PriceDetailCompo

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(127, 0.1),
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        width: '100%',
        height: '75%',
        // backgroundColor: 'green'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '600',
        fontSize: moderateScale(11, 0.1),
        color: 'rgba(134, 142, 150, 1)',
        paddingLeft:5
    },
    textTwo:{
        color:'rgba(255, 89, 89, 1)',
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize:moderateScale(11,0.1)
    }
})
