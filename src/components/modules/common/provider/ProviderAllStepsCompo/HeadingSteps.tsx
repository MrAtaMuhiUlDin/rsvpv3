import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    heading:string,
    innerViewWidth?:string|number,
    step:number
}

const HeadingSteps:React.FC<Props> = ({heading,innerViewWidth,step}) => {

   const defInnerWidth = innerViewWidth ? innerViewWidth : '50%'
     

    return (
        <React.Fragment>        
        <View style={{marginBottom:5}} ><Text style={styles.textTwo}>STEP {step} of 6</Text></View>
        <View  style={styles.mainContainer}>
           <View style={{width:defInnerWidth,height:'100%',}}>
               <Text style={styles.textOne}>{heading}</Text>
           </View>
        </View>
        </React.Fragment>

    )
}

export default HeadingSteps

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(63,0.1),
        // backgroundColor:'green'
    },
    textOne:{
        color:'rgba(19, 32, 77, 1)',
        fontFamily:'OpenSans',
        fontWeight:'700',
        fontSize:moderateScale(26,0.1)
    },
    textTwo:{
        color:'rgba(122, 122, 122, 1)',
        fontFamily:'OpenSans',
        fontWeight:'600',
        fontSize:moderateScale(12,0.1)
    }
})
