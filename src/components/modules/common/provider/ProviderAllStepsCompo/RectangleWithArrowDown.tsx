import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ArrowDownSVG from '../../../../../assets/icons/ArrowDownSVG'

interface Props {
    text:string
}

const RectangleWithArrowDown:React.FC<Props> = ({text}) => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
               <Text style={styles.textOne}>{text}</Text>
               <ArrowDownSVG size={12} />
            </View>
           
        </View>
    )
}

export default RectangleWithArrowDown

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(42,0.1),
        backgroundColor:'white',
        borderWidth:1,
        borderColor:'#FF5959',
        justifyContent:'center',
        alignItems:'center'
    },
    viewOne:{
        width:'90%',
        height:'80%',
        // backgroundColor:'pink',
        justifyContent:'space-between',
        flexDirection:'row',
        alignItems:'center'
    },
    textOne:{
        fontFamily:'OpenSans',
        fontSize:moderateScale(13,0.1),
        color:'rgba(19, 32, 77, 1)',
        fontWeight:'600'
    }

})
