import React from 'react'
import { StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AddPictureSVG from '../../../../../assets/icons/addPictureSVG'

interface Props {
  onpress?:()=>void  
}

const UploadAnImageSteps:React.FC<Props> = ({onpress}) => {
    return (
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={onpress}
        style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <AddPictureSVG size={moderateScale(29,0.1)} color={'#FF5959'}  />
                <View style={{width:'50%',height:50,justifyContent:'center',alignItems:'center',marginLeft:8}}>
                <Text style={styles.textOne}>Upload An Image</Text>

                </View>
            </View>
           
        </TouchableOpacity>
    )
}

export default UploadAnImageSteps

const styles = StyleSheet.create({
    mainContainer:{
        height:moderateScale(119,0.1),
        width:'100%',
        borderRadius:10,
        backgroundColor:'rgba(255, 99, 89, 0.1)',
        justifyContent:'center',
        alignItems:'center'
    },
    viewOne:{
        width:'28%',
        height:'40%',
        // backgroundColor:'green',
        justifyContent:'center',
        alignItems:'center',
        flexDirection:'row'
    },
    textOne:{
      color:'#FF5959',
      fontSize:moderateScale(11,0.1),
      fontFamily:'OpenSans',
      fontWeight:'700',
    }
})
