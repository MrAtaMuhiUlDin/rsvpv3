import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Dot from 'react-native-calendars/src/calendar/day/dot'
import { moderateScale } from 'react-native-size-matters'
import DollarSvg from '../../../../../assets/icons/DollarSvg'
import DotSvg from '../../../../../assets/icons/DotSvg'
import PlusSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/PlusSvg'
import DustBinSVg from '../../../../../assets/icons/ProviderSVG/DustBinSVg'
import SimplePlus from '../../../../../assets/icons/SimplePlus'
import ContentContainer from '../../../../abstract/contentContainer'

interface Props {

}




const ExtraServicesBtn = () => {
    return (
        <TouchableOpacity style={styles.viewTwo}>

            <View style={{ width: '90%', height: '90%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={styles.textTwo} >+ Add Extra Service</Text>
            </View>

        </TouchableOpacity>
    )
}


const ExtraServicesItem = () => {
    return (

        <View style={{ width: '100%', flexDirection: 'row', height: moderateScale(26, 0.1) }}>
            <View style={{ width: '5%', justifyContent: 'center', alignItems: 'center' }}>
                <DotSvg />
            </View>
            <View style={{ width: '57%', justifyContent: 'center', alignItems: 'center', borderBottomColor: 'lightgrey', borderBottomWidth: 1 }}>
                <TextInput style={{ padding: 0, paddingLeft: 10, width: '100%' }} />
            </View>
            <View style={{ width: '8%', justifyContent: 'center', alignItems: 'center' }}>
                <DollarSvg color={'#13204D'} size={12} />
            </View>
            <View style={{ width: '22%', justifyContent: 'center', alignItems: 'center', borderBottomColor: 'lightgrey', borderBottomWidth: 1 }}>
                <TextInput style={{ padding: 0, paddingLeft: 5, width: '100%' }} />

            </View>
            <View style={{ width: '8%', justifyContent: 'center', alignItems: 'flex-end' }}>
                <DustBinSVg color={'rgba(0, 0, 0, 0.4)'} />
            </View>
        </View>
    )
}








const ExtraSevicesSimple: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={{ width: '90%', height: '85%' }}>

                <View style={styles.viewOne}>
                    <View style={{ width: '100%', paddingTop: 5, paddingBottom: 10 }}>
                        <Text style={styles.textOne}>Extra Services</Text>
                    </View>


                    <ExtraServicesItem />


                    <View style={{ width: '100%', height: 100, justifyContent: 'flex-end', alignItems: 'center' }}>
                        <ExtraServicesBtn />
                    </View>




                    <View>

                    </View>


                </View>

            </View>
        </View>
    )
}

export default ExtraSevicesSimple

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(186, 0.1),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        color: 'black',
        fontSize: moderateScale(12, 0.1),
        fontFamily: 'OpenSans',
        fontWeight: '600'
    },
    viewOne: {
        width: '100%'
    },
    viewTwo: {
        width: moderateScale(164, 0.1),
        height: moderateScale(45, 0.1),
        backgroundColor: 'rgba(255, 99, 89, 0.1)',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
    },
    textTwo: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: '#FF5959'
    }
})
