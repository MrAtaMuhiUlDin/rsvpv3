import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'




const TripToIslaCompo: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={styles.viewTwo}>
                    <Image source={require('../../../../../assets/images/supplierImageSize.png')} style={{ width: '100%', height: '100%' }} resizeMode={'cover'} />
                </View>
                <View style={styles.viewThree}>
                    <View style={styles.viewFour} >
                        <Text style={styles.textOne}>Trip to Isla Contadora</Text>
                        <Text style={styles.textTwo}>$200</Text>

                    </View>

                    <View style={styles.viewFive} >
                        <Text style={styles.textThree} numberOfLines={4}  >Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore, quasi ab aperiam consequuntur suscipit amet ratione atque deleniti officia sunt, eaque dolorum mollitia harum aspernatur praesentium animi error hic fugit!</Text>
                    </View>

                </View>

            </View>
        </View>
    )
}

export default TripToIslaCompo

const styles = StyleSheet.create({
    mainContainer: {
        width: moderateScale(340,0.1),
        height: moderateScale(122, 0.1),
        backgroundColor: 'white',
        // backgroundColor:'red',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 2,
        marginVertical:5
    },
    viewOne: {
        width: '90%',
        height: '75%',
        // backgroundColor: 'green',
        flexDirection: 'row'
    },
    viewTwo: {
        width: moderateScale(87, 0.1),
        height: '100%',
        // backgroundColor: 'pink',

    },
    viewThree: {
        width: moderateScale(223, 0.1),
        height: '100%',
        // backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFour: {
        width: '100%',
        height: '25%',
        // backgroundColor: 'pink',
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingLeft: 10
    },
    viewFive: {
        width: '100%',
        height: '75%',
        // backgroundColor: 'red',
        paddingLeft: 10
    },
    textOne: {
        fontFamily: 'Poppins',
        fontSize: moderateScale(13, 0.1),
        fontWeight: '600',
        color: '#263238'
    },
    textTwo: {
        fontFamily: 'Poppins',
        fontSize: moderateScale(13, 0.1),
        fontWeight: '400',
        color: 'rgba(50, 54, 67, 0.7)'
    },
    textThree: {
        fontFamily: 'Poppins',
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        color: 'rgba(50, 54, 67, 0.7)',
        lineHeight: 15
    }
})
