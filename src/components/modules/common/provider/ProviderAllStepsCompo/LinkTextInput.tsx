import React from 'react'
import { StyleSheet, Text, Touchable, TouchableOpacity, View,TextInput } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AddPictureSVG from '../../../../../assets/icons/addPictureSVG'
import NuestroMenuSvg from '../../../../../assets/icons/SupplierScreenSvgs/NuestroMenuSvg'
import AbstractTextInput from '../../../../abstract/abstractTextInput'

interface Props {
    label?:string,
    placeHolder?:any
}

const LinkTextInput:React.FC<Props> = ({placeHolder,label}) => {

    const defPlaceHolder = placeHolder ? placeHolder : undefined
    const defLabel = label ? label : undefined

    return (
        <View style={styles.mainContainer}>
        <View style={{width:'90%',height:'100%'}}>
            <Text style={styles.textOne}>{defLabel}</Text>
            <TextInput placeholder={defPlaceHolder}  style={{height:moderateScale(36,0.1),padding:0}} />
       
        </View>
        <View style={styles.viewOne}>
          <NuestroMenuSvg />
        </View>
    </View>
    )
}

export default LinkTextInput

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(55,0.1),
        // backgroundColor:'blue',
        flexDirection:'row',
        borderBottomColor:'lightgrey',
        borderBottomWidth:1,
    },
    textOne:{
        fontSize:moderateScale(11,0.1),
        fontFamily:'OpenSans',
        color:'#868E96',
        fontWeight:'600',
        height:moderateScale(14,0.1)
    },
    viewOne:{
        width:'10%',height:'100%',
        paddingTop:moderateScale(12,0.1),
        justifyContent:'center',
        alignItems:'flex-end',
        paddingRight:10,
        // backgroundColor:'purple'
    }

})
