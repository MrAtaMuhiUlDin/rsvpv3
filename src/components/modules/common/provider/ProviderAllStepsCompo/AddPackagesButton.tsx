import React from 'react'
import { StyleSheet, Text, Touchable, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    onpress?: () => void
}

const AddPackagesButton: React.FC<Props> = ({ onpress }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.8}
            onPress={onpress}
            style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <Text style={styles.textOne}>ADD PACKAGE</Text>
            </View>

        </TouchableOpacity>
    )
}

export default AddPackagesButton

const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(100, 0.1),
        width: moderateScale(340, 0.1),
        borderRadius: 15,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 2,
    },
    viewOne: {
        width: moderateScale(313, 0.1),
        height: moderateScale(60, 0.1),
        backgroundColor: 'rgba(255, 99, 89, 0.1)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        color: '#FF5959',
        fontSize: moderateScale(11, 0.1),
        fontFamily: 'OpenSans',
        fontWeight: '700',
    }
})
