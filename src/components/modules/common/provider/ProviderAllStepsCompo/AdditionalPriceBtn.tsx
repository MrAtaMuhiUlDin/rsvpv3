import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'


interface Props {
    label?:string
}




const AdditionalPriceBtn:React.FC<Props> = ({label}) => {
   
     
    const deflabel = label ? label  : '+ Add additional price'

    return (
        <View style={styles.mainContainer}>
           <View style={styles.viewOne}>
               <Text style={styles.textOne} >{deflabel}</Text>

           </View>
        </View>
    )
}

export default AdditionalPriceBtn

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(45,0.1),
        backgroundColor:'white',
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        borderColor:'#FF5959',
        borderWidth:1
    },
    viewOne:{
        height:'90%',
        backgroundColor:'white',
        justifyContent:'center',
        alignItems:'center'
    },
    textOne:{
        fontSize:moderateScale(14,0.1),
        fontWeight:'700',
        fontFamily:'OpenSans',
        color:'#FF5959',
        textTransform:'uppercase'
    }
})
