import React from 'react'
import { ImageBackground, StyleSheet, Text, View } from 'react-native'
import { moderateScale, moderateVerticalScale } from 'react-native-size-matters'



const Banner:React.FC = () => {
    return (
        <View style={styles.mainConatiner}>
            <ImageBackground source={require('../../../../../assets/images/Banner.png')} style={{width:'100%',height:'100%',borderRadius:9}}>
               <View style={styles.viewOne}>
               <Text style={styles.textOne}>Arizona Club</Text>

              <Text style={styles.textTwo}>The perfect Space to hangout in the town and organize private parties.</Text>

               </View>
            </ImageBackground>
        </View>
    )
}

export default Banner

const styles = StyleSheet.create({
    mainConatiner:{
        width:'100%',
        height:moderateScale(309,0.1),
        backgroundColor:'pink',
        borderRadius:9

    },
    viewOne:{
        width:'85%',
        height:70,
        // backgroundColor:'green',
        alignSelf:'center',
        position:'absolute',
        bottom:10,
    },
    textOne:{
        fontFamily:'OpenSans',
        fontSize:moderateScale(16,0.1),
        fontWeight:'700',
        color:'white',
        paddingBottom:5
    },
    textTwo:{
        fontFamily:'OpenSans',
        fontSize:moderateScale(12,0.1),
        fontWeight:'600',
        color:'white',
        lineHeight:20
    }
})
