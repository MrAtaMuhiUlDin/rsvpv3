import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {CheckBoxCompo} from '../../client/BottomSheetsClient/SplitBillBottomSheet'

const SelectCategory: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={styles.viewtwo}>
                    <CheckBoxCompo label={'By Guest'} labelFontWeight={'600'} labelColor={'#13204D'} />
                </View>
            </View>
            <View style={styles.viewOne}>
                <View style={styles.viewtwo}>
                <CheckBoxCompo label={'By Package'} labelFontWeight={'600'} labelColor={'#13204D'} />
                </View>

            </View>
        </View>
    )
}

export default SelectCategory

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: 80,
        backgroundColor: 'white',
        flexDirection: 'row'
    },
    viewOne: {
        width: '50%',
        height: '100%',
        // backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewtwo: {
        width: '60%',
        height: '40%',
        // backgroundColor: 'yellow',
    }
})
