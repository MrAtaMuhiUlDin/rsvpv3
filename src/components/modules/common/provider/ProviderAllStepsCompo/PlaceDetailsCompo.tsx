import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'



const FieldsCompo = () => {

    return (
        <View style={{ width: '100%', height: '14.2%', flexDirection: 'row' }}>
            <View style={{ width: '40%', height: '100%', justifyContent: 'center', paddingLeft: 22 }}>
                <Text style={styles.textOne}>Location</Text>
            </View>

            <View style={{ width: '60%', height: '100%', justifyContent: 'center', paddingLeft: 10 }}>
                <Text style={styles.textTwo}>71 ABCD Street, NYC, Arizona</Text>
            </View>
        </View>
    )
}


const PlaceDetailsCompo: React.FC = () => {
    return (
        <React.Fragment>
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <FieldsCompo />
                <FieldsCompo />
                <FieldsCompo />
                <FieldsCompo />
                <FieldsCompo />
                <FieldsCompo />
                <FieldsCompo />
            </View>
        
        </View>
            <View style={{ height: 1, width: '100%', borderRadius: 1, borderWidth: 1, borderColor: 'black', borderStyle: 'dashed', zIndex: 0, }}>
            <View style={{ position: 'absolute', left: 0, bottom: 0, width: '100%', height: 1, backgroundColor: 'white', zIndex: 1 }} />
        </View>
        </React.Fragment>
    )
}

export default PlaceDetailsCompo

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(230, 0.1),
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
    
    },
    viewOne: {
        width: '100%',
        height: '85%',
        // backgroundColor: 'red'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '600',
        color: 'rgba(134, 142, 150, 1)',
        fontSize: moderateScale(11, 0.1)
    },
    textTwo: {
        color: 'rgba(51, 51, 51, 1)',
        fontFamily: 'OpenSans',
        fontWeight: '400',
        fontSize: moderateScale(11, 0.1)

    }

})
