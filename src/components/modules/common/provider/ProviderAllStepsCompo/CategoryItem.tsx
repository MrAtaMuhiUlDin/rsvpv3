import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    label?:string,
    pink?:boolean,
    width?:string|number
}

const CategoryItem:React.FC<Props> = ({label,pink,width}) => {
 
    const defLabel = label ? label : 'text'
    const defWidth = width ? width : moderateScale(108,0.1)

    return (
        <View style={[styles.mainContainer,{backgroundColor:pink?'#FDF1DB':'#CFECFF',width:defWidth}]}>
            <Text style={{fontFamily:'OpenSans',fontSize:moderateScale(12,0.1),fontWeight:'600',paddingVertical:5,paddingHorizontal:10}}>
                {defLabel}
                </Text>
        </View>
    )
}

export default CategoryItem

const styles = StyleSheet.create({
    mainContainer:{
        marginBottom:10,
        marginRight:5,
        minWidth:moderateScale(108,0.1),
        height:moderateScale(28,0.1),
        borderRadius:15,
        justifyContent:'center',
        alignItems:'center'
    }
})
