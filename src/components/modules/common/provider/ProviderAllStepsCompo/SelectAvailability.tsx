import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import TimeBtnWithDownArrow from '../../client/TimeBtnWithDownArrow'



interface Props {
    disabled?:boolean
}




const SelectAvailability: React.FC<Props> = ({disabled}) => {

    const [select, setSelect] = useState<boolean>(false)

    return (
        <View style={styles.mainConatiner}>



            <TouchableOpacity
                onPress={() => setSelect((prev) => !prev)}
                style={{ width: '10%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <View style={{ width: 20, height: 20, borderRadius: 10, borderWidth: 1, borderColor: '#EF4339', justifyContent: 'center', alignItems: 'center' }}>
                    {select ?
                        <View style={{ width: 15, height: 15, borderRadius: 7.5, backgroundColor: '#EF4339' }} />
                        : null}
                </View>
            </TouchableOpacity>

            <View style={{ width: '30%', height: '100%', justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 15 }}>
                <Text style={styles.textOne}>Sun</Text>
            </View>


            <View style={{ width: '30%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <TimeBtnWithDownArrow disabled={disabled} />
            </View>

            <View style={{ width: '30%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <TimeBtnWithDownArrow disabled={disabled} />
            </View>






        </View>
    )
}

export default SelectAvailability

const styles = StyleSheet.create({
    mainConatiner: {
        width: '100%',
        height: 50,
        // backgroundColor: 'pink',
        flexDirection: 'row',
        marginVertical:8
      
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(13, 0.1),
        fontWeight: '400',
        color: 'rgba(0, 0, 0, 0.5)'
    }
})
