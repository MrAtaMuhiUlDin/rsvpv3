import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'



interface Props {
    today?: boolean,
    onPress?:()=>void
}

const NotificationsModule: React.FC<Props> = ({ today,onPress }) => {

    const arrayColor: string[] = [
        'rgba(255, 99, 89, 0.1)', ''
    ]


    return (
        <React.Fragment>

            <TouchableOpacity
            onPress={onPress}
            style={[{ backgroundColor: today ? 'rgba(255, 99, 89, 0.1)' : '' }, styles.mainConatiner]}>
                <View style={styles.viewOne}>

                    <View style={styles.viewTwo}>
                        <View style={{ width: 50, height: 50, borderRadius: 25 }}>
                            <Image source={require('../../../../assets/images/iconUser.png')} style={{ width: '100%', height: '100%', borderRadius: 25 }} />
                        </View>
                    </View>


                    <View style={styles.viewThree}>
                        <Text style={styles.textOne}>You have recieved an <Text style={{ fontWeight: 'bold' }}>Quote</Text> from client <Text style={{ color: '#FF5959' }}>Jose Maneul</Text> click to see.</Text>
                    </View>

                </View>
            </TouchableOpacity>

        </React.Fragment>
    )
}

export default NotificationsModule

const styles = StyleSheet.create({
    mainConatiner: {
        height: moderateScale(76, 0.1),
        width: '100%',
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        width: '95%',
        height: moderateScale(50, 0.1),
        flexDirection: 'row'
    },
    viewTwo: {
        width: '20%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        borderRightColor: '#FF5959',
        borderRightWidth: 2
    },
    viewThree: {
        width: '75%',
        height: '100%',
        marginLeft: 15,
        justifyContent: 'center'
    },
    textOne: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'black'
    },

})
