import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    
}

const CompletedJobsCompo = (props: Props) => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
             <Text style={styles.textOne}>Job Name</Text>
             <Text style={styles.textOne}>Amount USD</Text>
            </View>
            <View style={styles.viewTwo}>
            <Text style={styles.textOne}>Party Arrangements</Text>
             <Text style={styles.textOne}>$100</Text>
            </View>
            <View style={styles.viewTwo}>
            <Text style={[styles.textOne,{color:'#FF5959'}]}>RSVP Fee</Text>
             <Text style={[styles.textOne,{color:'#FF5959'}]}>-$20</Text>
            </View>
            <View style={styles.viewTwo}>
            <Text style={styles.textOne}>Expected Earnings</Text>
             <Text style={styles.textOne}>$80</Text>
            </View>
        
        </View>
    )
}

export default CompletedJobsCompo

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        // height:moderateScale(102,0.1),
        marginTop:10,
        backgroundColor:'white',
        flexDirection:'column',
    },
    viewOne:{
        width:'100%',
        height:moderateScale(30,0.1),
        // backgroundColor:'orange',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'flex-start'
    },
    viewTwo:{
        width:'100%',
        height:moderateScale(25,0.1),
        // backgroundColor:'pink',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
     textOne:{
         color:'#323232',
         fontSize:moderateScale(13,0.1),
         fontWeight:'400',
         fontFamily:'OpenSans'
     }

})
