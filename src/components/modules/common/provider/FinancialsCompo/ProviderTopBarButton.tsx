import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    label?:string,
    isFocused?: Boolean,
    isFirstIndex: Boolean,
    isLastIndex: Boolean,
    onPress?: () => void
}

const ProviderTopBarButton:React.FC<Props> = ({label,isFocused,isFirstIndex,isLastIndex,onPress}) => {
    return (
        <TouchableOpacity
        onPress={onPress}
         style={styles.viewFour}>
        <Text style={[styles.textOne, { color: isFocused ? '#FF5959' : 'black' }]}>{label}</Text>
        {isFocused
            ?
            <View style={styles.viewFive} />
            :
            <View style={[styles.viewFive, { backgroundColor: isFocused ? '#FF5959' : '#EFEFEF' }]} />
        }
    </TouchableOpacity>
    )
}

export default ProviderTopBarButton

const styles = StyleSheet.create({
    viewFour: {
        width: '33.4%',
        height: '100%',
        // backgroundColor:'pink',
        justifyContent: 'center',
        alignItems: 'center'

    },
    viewFive: {
        width: 26,
        height: 5,
        borderRadius: 30,
        backgroundColor: '#FF5959',
        marginTop: 5
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(13, 0.1),
        color: '#FF5959',
        fontWeight: '400'
    }
})
