import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {View} from 'react-native'


interface Props {
    color?:any,
    Startsize?:number 
    rating?:number,
    actionTypes?: "one" | "two" 
}

let RatingDefaultProps = {
    rating : 5,
    color:'red',
    Startsize:10
}



const StarRating:React.FC<Props> = ({color,Startsize,rating}) =>{

    const ArrayColor:string[] = ['white','white','white','white','white',]

    const RatingCalculator = (rnumber:any) => {

        for (let i = 0; i < rnumber; i++) {
            ArrayColor[i] = color
          }

         }

       RatingCalculator(rating)

    return (
        <View style={{flexDirection:'row'}}>
                
            <AntDesign name='star' size={Startsize} color={ArrayColor[0]} />
            <AntDesign name='star' size={Startsize} color={ArrayColor[1]} />
            <AntDesign name='star' size={Startsize} color={ArrayColor[2]} />
            <AntDesign name='star' size={Startsize} color={ArrayColor[3]} />
            <AntDesign name='star' size={Startsize} color={ArrayColor[4]} />
    
        </View>
    )
}

StarRating.defaultProps = RatingDefaultProps
export default StarRating