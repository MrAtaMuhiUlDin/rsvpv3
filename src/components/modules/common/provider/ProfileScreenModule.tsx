import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import BusinessSVG from '../../../../assets/icons/ProviderSVG/businessSVG'

interface Props {
    image?: boolean,
    imgUrl?: any
}

const ProfileScreenModule: React.FC<Props> = ({ image, imgUrl }) => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={{ height: moderateScale(59, 0.1), width: moderateScale(59, 0.1) }}>
                    {!image ?
                        <View style={styles.viewtwo}>
                            <BusinessSVG />
                        </View>
                        :
                        <View style={styles.viewThree}>
                            <Image source={imgUrl} style={{ width: '100%', height: '100%' }} />
                        </View>}
                </View>

                <Text style={styles.textOne}>Business</Text>
                <Text style={styles.textTwo}>Panama City, Panama</Text>
            </View>
        </View>
    )
}

export default ProfileScreenModule

const styles = StyleSheet.create({
    mainContainer: {
        width: '55%',
        height: moderateScale(134, 0.1),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        height: '85%',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewtwo: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderColor: '#FF5959',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: 60,
        height: 60,
        borderRadius: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(16, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'black'
    },
    textTwo: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#A2A2A2'
    },

})

