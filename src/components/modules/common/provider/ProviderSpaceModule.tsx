import React from 'react'
import { ImageBackground, StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import LinearGradient from 'react-native-linear-gradient';

interface Props {
  onPress?:()=>void  
}

const ProviderSpaceModule:React.FC<Props> = ({onPress}) => {
    return (
        <LinearGradient colors={['#EF4339', '#FF5959']}   style={styles.viewOne}>
            <Image source={require('../../../../assets/images/image2.png')}  style={styles.imageStyle} />
            <View style={[StyleSheet.absoluteFillObject,{justifyContent:'center',marginLeft:15}]}>
                <View style={{width:'62%',height:'80%',justifyContent:'space-between'}}>
                <Text style={styles.textOne}>{`List Your Space today & Host Events of Your \nChoice`}</Text>
                
                <TouchableOpacity
                onPress={onPress}
                activeOpacity={1}
                style={styles.viewThree}>
                <Text style={styles.textTwo}>List Space Now</Text>
                </TouchableOpacity>

                </View>

            
        </View>
        </LinearGradient>
    )
}

export default ProviderSpaceModule

const styles = StyleSheet.create({
viewOne:{
    width:'100%',
    height:moderateScale(134,0.1),
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:'#EF4339',
    borderRadius:8
},
imageStyle:{
    width:'100%',
    height:'100%',opacity:0.5,
    borderRadius:8
},
viewThree:{
    width:moderateScale(93,0.1),
    height:moderateScale(24,0.1),
    justifyContent:'center',
    borderRadius:6,
    alignItems:'center',
    backgroundColor:'white'
},
textOne:{
    fontSize:moderateScale(15,0.1),
    fontWeight:'700',
    fontFamily:'OpenSans',
    color:'white'
},
textTwo:{
    fontSize:moderateScale(8,0.1),
    fontWeight:'700',
    fontFamily:'OpenSans',
    color:'#FF5959'
}
})


