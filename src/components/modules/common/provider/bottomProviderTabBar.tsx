import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import AlarmSVG from '../../../../assets/icons/alarmSVG';
import BriefcaseSVG from '../../../../assets/icons/briefcaseSVG';
import CalendarSVG from '../../../../assets/icons/calendarSVG';
import CheckCircleSVG from '../../../../assets/icons/checkCircleSVG';
import ProfileSVG from '../../../../assets/icons/profileSVG';
import {CLIENT_TABS} from '../../../../navigation/providerNavigator';
import AbstractFooterButtonContainer from '../../../abstract/abstractFooterButtonContainer';

interface Props {
  state: {
    routes: any[];
    index: number;
  };
  descriptors: any;
  navigation: any;
}

const BottomProviderTabBar: React.FC<Props> = ({
  state,
  descriptors,
  navigation,
}) => {
  return (
    <View style={{flexDirection: 'row'}}>
      {state.routes.map((route: any, index: any) => {
        const {options} = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;
        const isFirstIndex = index === 0;
        const isLastIndex = index === state.routes.length - 1;
        const onPress = () => {
          // console.log(`Pressed....`);
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // console.log(`Navigating`);
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: route.name, merge: true});
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        // console.log(route.name);
        switch (route.name) {
          case CLIENT_TABS.HOME:
            return (
              <AbstractFooterButtonContainer
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}>
                <CheckCircleSVG />
              </AbstractFooterButtonContainer>
            );
          case CLIENT_TABS.CALENDAR:
            return (
              <AbstractFooterButtonContainer
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}>
                <CalendarSVG />
              </AbstractFooterButtonContainer>
            );
          case CLIENT_TABS.Business:
            return (
              <AbstractFooterButtonContainer
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}>
                <BriefcaseSVG />
              </AbstractFooterButtonContainer>
            );
          case CLIENT_TABS.NOTIFICATIONS:
            return (
              <AbstractFooterButtonContainer
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}>
                <AlarmSVG />
              </AbstractFooterButtonContainer>
            );
          case CLIENT_TABS.PROFILE:
            return (
              <AbstractFooterButtonContainer
                isFirstIndex={isFirstIndex}
                isLastIndex={isLastIndex}
                onPress={onPress}
                key={route.key}
                isFocused={isFocused}>
                <ProfileSVG />
              </AbstractFooterButtonContainer>
            );
          default:
            return false;
        }
      })}
    </View>
  );
};
export default BottomProviderTabBar;
