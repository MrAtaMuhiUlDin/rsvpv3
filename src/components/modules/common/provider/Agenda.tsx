import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { moderateScale } from "react-native-size-matters";
import CircleCheckSvg from "../../../../assets/icons/ProviderSVG/CircleCheckSvg";


interface Props {
    pending?: boolean,
    money?: boolean,
    onPress?:()=>void
}




const Agenda: React.FC<Props> = ({ pending, money,onPress }) => {
    return (
        <TouchableOpacity
        onPress={onPress}
        style={styles.mainContainer} >
            <View style={styles.containerOne}>
                <View style={{ width: '100%', height: '50%', flexDirection: 'row' }} >
                    <View style={{ width: '10%' }}>
                        <CircleCheckSvg />
                    </View>
                    <View style={{ width: '55%' }}>
                        <Text style={styles.textOne}>Viaje Isla Contadora</Text>
                        <Text style={styles.textTwo}>Viaje para 7 Personas</Text>
                    </View>
                    <View style={{ width: '35%', alignItems: 'flex-end', paddingRight: 2 }}>
                        {pending ?
                            <Text style={styles.textThree}>PENDING CONFIRMATION</Text>
                            :
                            <Text style={styles.textFour}>YK875765</Text>
                        }
                    </View>
                </View>
                <View style={{ width: '100%', height: '55%', flexDirection: 'row' }} >
                    <View style={{ width: '55%', height: '100%' }}>
                        <View style={{ height: '45%', width: '100%' }}>
                            <Text style={{ fontSize: moderateScale(11, 0.1), paddingTop: 5, fontWeight: '400', fontFamily: 'OpenSans', color: 'rgba(50,54,67,0.5)' }}>Arrival Details</Text>
                        </View>

                        <View style={{ width: '100%', height: '55%', flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View>
                                <Text style={styles.textFive}>Name</Text>
                                <Text style={styles.textSix}>Jose</Text>
                            </View>
                            <View>
                                <Text style={styles.textFive}>Date</Text>
                                <Text style={styles.textSix}>11/13/2020</Text>
                            </View>
                            <View>
                                <Text style={styles.textFive}>Guests</Text>
                                <Text style={styles.textSix}>2 Adults</Text>
                            </View>

                        </View>

                    </View>
                    <View style={{ width: '45%', height: '100%', justifyContent: 'center', alignItems: 'flex-end' }}>
                        {money ?
                            <TouchableOpacity style={styles.moneyButton}>
                                <Text style={styles.moneyButtonText}>$950.00</Text>
                            </TouchableOpacity>
                            : null}
                    </View>


                </View>
            </View>
        </TouchableOpacity>
    )
}


export default Agenda


const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        marginVertical: 10,
        height: moderateScale(134, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    containerOne: {
        width: '85%',
        height: '75%',
        alignSelf: 'center'
    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans'
    },
    textTwo: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingTop: 5
    },
    textThree: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textFour: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'

    },
    textFive: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'

    },
    textSix: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    moneyButton: {
        width: moderateScale(72, 0.1),
        height: moderateScale(30, 0.1),
        justifyContent: 'center',
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    moneyButtonText: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'white'
    }
})