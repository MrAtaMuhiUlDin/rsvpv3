import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, ViewStyle } from 'react-native'
import { moderateScale } from "react-native-size-matters";
import StarRating from "./StarRating";


interface Props {
    tag?: boolean,
    tagtext?: string,
    imageUrl?: any,
    title?: string,
    style?: ViewStyle,
    onPress?:()=>void
}


const YourServicesCard: React.FC<Props> = ({ tag, tagtext, title, imageUrl, style,onPress }) => {

    const defTagText = tagtext ? tagtext : 'tagHere'
    const defTitleText = title ? title : 'titlHere'


    return (
        <TouchableOpacity
        activeOpacity={0.9}
        onPress={onPress}
            style={[styles.mainContainer, style]} >
            <View style={styles.viewOne}>
                <View style={styles.viewThree}>
                    <View style={styles.viewFour}>
                        <View style={{ width: moderateScale(24, 0.1), height: moderateScale(24, 0.1) }}>
                            <Image source={require('../../../../assets/images/logo.png')} style={{ width: '100%', height: '100%' }} />
                        </View>
                    </View>
                    <View style={{ width: '90%', paddingLeft: 8, justifyContent: 'center' }}>
                        <Text style={styles.textOne}>Company</Text>
                        <Text style={styles.textTwo}>San Cristobal, Panama  </Text>
                    </View>
                </View>
            </View>
            <View style={styles.viewTwo}>
                <Image source={imageUrl} style={{ width: '100%', height: '100%' }} resizeMode={'stretch'} />
                {tag ?
                    <TouchableOpacity style={styles.viewFive}>
                        <Text style={styles.textThree}>{defTagText}</Text>
                    </TouchableOpacity>
                    : null}
            </View>
            <View style={styles.viewSix}>
                <View style={styles.viewSeven}>
                    <Text style={styles.textFour}>{defTitleText}</Text>
                    <View style={styles.textFive}>
                        <View style={{ width: '65%', justifyContent: 'center', alignItems: 'center' }}>

                            <StarRating rating={5} color={'#FF7373'} />

                        </View>
                        <View style={{ width: '35%', justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={styles.textSix}>(222)</Text>
                        </View>
                    </View>
                </View>


            </View>
        </TouchableOpacity>
    )
}


export default YourServicesCard


const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        marginVertical: 10,
        height: moderateScale(275, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10.5,
        flexDirection: 'column',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,

    },
    viewOne: {
        width: '100%',
        height: '20%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: '58%',
        backgroundColor: 'grey',
        position: 'relative'
    },
    viewThree: {
        width: '95%',
        height: '100%',
        flexDirection: 'row'
    },
    viewFour: {
        width: '10%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFive: {
        width: moderateScale(75, 0.1),
        height: moderateScale(18, 0.1),
        justifyContent: 'center',
        position: 'absolute',
        bottom: 10,
        right: 14,
        borderRadius: 33,
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    viewSix: {
        width: '100%',
        height: '22%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSeven: {
        width: '85%',
        height: '100%',
        justifyContent: 'center'
    },
    textOne: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textTwo: {
        fontSize: moderateScale(7.5, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50,54,67,0.5)'
    },
    textThree: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'white'
    },
    textFour: {
        fontSize: moderateScale(12.5, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#000000'
    },
    textFive: {
        width: '30%',
        height: '40%',
        flexDirection: 'row'
    },
    textSix: {
        fontSize: moderateScale(8.5, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50,54,67,0.5)'
    }
})