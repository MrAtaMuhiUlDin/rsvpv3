import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Check from '../../../../assets/icons/ProviderSVG/CheckSVG'
import CheckMark from '../../../../assets/icons/ProviderSVG/CheckMarkSVG'


interface Props {
    active?: boolean,
    notseen?: boolean,
    notseenmorethanMonth?: boolean,
    onPress?:()=>void
}

const ChatModule: React.FC<Props> = ({ active, notseen, notseenmorethanMonth,onPress }) => {
    return (
        <TouchableOpacity
        onPress={onPress}
        style={styles.mainContainer}>
            <View style={styles.conatainerOne}>
                <View style={styles.containerOneView}>
                    <View style={styles.conatinerImageView}>
                        <Image source={require('../../../../assets/images/10.png')} style={{ width: '100%', height: '100%', borderRadius: 20 }} />
                        {active ?
                            <View style={styles.greenActiveConatiner}>
                                <View style={styles.greenActiveView}></View>
                            </View>
                            : null}
                    </View>
                </View>
            </View>
            <View style={styles.conatinerTwo}>
                <Text style={styles.textstyleOne}>Empresa, S.A.</Text>
                <Text style={styles.textStyleTwo} numberOfLines={1}>The weather will be pesasasasrfect for the st...</Text>
            </View>
            <View style={styles.containerThree}>

                <View style={{ width: '90%', height: '50%' }}>
                    <View style={{ width: '100%', height: '50%', alignItems: 'flex-end' }}>
                        <View style={styles.numberView}>
                            <Text style={styles.numberText}>1</Text>
                        </View>
                    </View>
                    <View style={styles.viewsContainer}>
                        {notseen ?
                            <View style={styles.viewContainerOne}>
                                <CheckMark />
                                <Text style={styles.viewText}>Friday</Text>
                            </View>
                            : notseenmorethanMonth ?
                                <View style={styles.viewContainerOne}>
                                    <CheckMark />
                                    <Text style={styles.viewText}>1/11/2015</Text>
                                </View>
                                :
                                <View style={styles.viewContainerOne}>
                                    <Check />
                                    <Text style={styles.viewText}>2:14 PM</Text>
                                </View>
                        }

                    </View>
                </View>



            </View>
        </TouchableOpacity>
    )
}

export default ChatModule

const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(63, 0.1),
        width: '100%',
        backgroundColor: '#FCFCFC',
        flexDirection: 'row'
    },
    conatainerOne: {
        width: '15%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerOneView: {
        width: moderateScale(39, 0.1),
        height: moderateScale(39, 0.1),
        justifyContent: 'center',
        alignItems: 'center'
    },
    conatinerImageView: {
        width: moderateScale(39, 0.1),
        height: moderateScale(39, 0.1),
        borderRadius: 20
    },
    greenActiveConatiner: {
        width: 15,
        height: 15,
        borderRadius: 7.5,
        backgroundColor: 'white',
        position: 'absolute',
        top: -1,
        right: -1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    greenActiveView: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: '#20E070',
    },
    conatinerTwo: {
        width: '63%',
        height: '100%',
        justifyContent: 'center',
        paddingLeft: 5
    },
    textstyleOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'SfProText',
        color: 'black'
    },
    textStyleTwo: {
        fontSize: moderateScale(12, 0.1),
        fontWeight: '400',
        fontFamily: 'SfProText',
        color: '#7A7A7A',
        width: '90%'
    },
    containerThree: {
        width: '22%',
        height: '100%',
        justifyContent: 'center'
    },
    numberView: {
        maxWidth: moderateScale(40, 0.1),
        height: moderateScale(15, 0.1),
        backgroundColor: '#FF3742',
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    numberText: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '700',
        fontFamily: 'SfProText',
        color: 'white',
        paddingHorizontal: 5
    },
    viewsContainer: {
        width: '100%',
        height: '50%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    viewContainerOne: {
        height: '100%',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    viewText: {
        fontSize: moderateScale(12, 0.1),
        fontWeight: '400',
        fontFamily: 'SfProText',
        color: '#7A7A7A'
    }

})
