import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { moderateScale } from "react-native-size-matters";
import OptionsSvg from "../../../../assets/icons/OptionsSvg";
import CircleCheckSvg from "../../../../assets/icons/ProviderSVG/CircleCheckSvg";
import StarRating from "../provider/StarRating";


interface Props {
    Person?: boolean,
    onPressOptions?:()=>void,
    onPress?:()=>void
}




const Bookings: React.FC<Props> = ({ Person,onPressOptions,onPress }) => {
    return (
        <TouchableOpacity 
        activeOpacity={0.8}
        onPress={onPress}
        style={styles.mainContainer} >
            <View style={styles.viewOne}>

                {Person ?
                    <View style={styles.viewTwo} >
                        <View style={styles.viewThree}>
                            <Text style={styles.textOne}>Viaje Isla Contadora</Text>

                            <Text style={styles.textTwo}>YK875765</Text>
                        </View>


                        <View style={styles.viewFour}>
                            <View style={{ width: 20, height: 20, borderRadius: 10 }}>
                                <Image source={require('../../../../assets/images/ppic.png')} style={{ width: '100%', height: '100%' }} />
                            </View>
                            <View style={{ flexDirection: 'column', marginLeft: 10 }}>
                                <Text style={styles.textThree}>Viaje para 7 Personas</Text>
                                <StarRating Startsize={8} />
                            </View>
                        </View>
                        <View style={styles.viewFive}>
                        </View>
                    </View>

                    : <View style={{ width: '100%', height: '50%', flexDirection: 'column' }} >
                        <View style={styles.viewSix}>
                            <CircleCheckSvg />
                            <Text style={styles.textFour}>YK875765</Text>

                        </View>
                        <View style={{ width: '55%' }}>
                            <Text style={styles.textFive}>Viaje Isla Contadora</Text>
                            <Text style={styles.textSix}>Viaje para 7 Personas</Text>
                            <StarRating Startsize={8} />
                        </View>
                        <View style={{ width: '35%', alignItems: 'flex-end', paddingRight: 2 }}>
                        </View>
                    </View>
                }

                <View style={{ width: '100%', height: '52%', flexDirection: 'row', }} >
                    <View style={{ width: '55%', height: '100%' }}>
                        <View style={{ height: '45%', width: '100%' }}>
                            {Person ? null
                                : <Text style={styles.textSeven}>Arrival Details</Text>
                            }
                        </View>

                        <View style={styles.viewSeven}>

                            <View style={{ marginRight: 25 }}>
                                <Text style={styles.textEight}>Date</Text>
                                <Text style={styles.textNine}>11/13/2020</Text>
                            </View>
                            <View>
                                <Text style={styles.textEight}>Guests</Text>
                                <Text style={styles.textNine}>2 Adults</Text>
                            </View>

                        </View>

                    </View>
                    <View style={styles.viewNine}>
                     <TouchableOpacity
                     onPress={onPressOptions}
                     style={{width:'20%',height:20,justifyContent:'center',alignItems:'center'}}>
                      <OptionsSvg color={'#FF5959'} />
                     </TouchableOpacity>
                    </View>


                </View>
            </View>
        </TouchableOpacity>
    )
}


export default Bookings



const styles = StyleSheet.create({
    mainContainer: {
        width: '100%', marginVertical: 10, height: moderateScale(168, 0.1), justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderRadius: 10, borderWidth: 1, borderColor: 'lightgrey'
    },
    viewOne: {
        width: '85%',
        height: '75%',
        alignSelf: 'center'
    },
    viewTwo: {
        width: '100%',
        height: '50%',
        flexDirection: 'column'
    },
    viewThree: {
        width: '100%',
        height: '25%',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewFour: {
        width: '55%',
        height: '50%',
        flexDirection: 'row',
        marginVertical: 30
    },
    viewFive: {
        width: '35%',
        alignItems: 'flex-end',
        paddingRight: 2
    },
    viewSix: {
        width: '100%',
        height: '25%',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewSeven: {
        width: '60%',
        height: '55%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewNine: {
        width: '45%',
        height: '100%',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        // backgroundColor:'red'
    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans'
    },
    textTwo: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textThree: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingBottom: 2
    },
    textFour: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textFive: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        paddingTop: 3
    },
    textSix: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingBottom: 2
    },
    textSeven: {
        fontSize: moderateScale(11, 0.1),
        paddingTop: 10,
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50,54,67,0.5)'
    },
    textEight: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textNine: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#323643'
    }
})