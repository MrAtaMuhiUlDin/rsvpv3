import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChatSvg from '../../../../../assets/icons/ConfirmBookingScreenSvgs/ChatSvg'
import AbstractButton from '../../../../abstract/abstractButton'

const ClientBookingConfirmation:React.FC = () => {
    return (
            <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <Image source={require('../../../../../assets/images/booking.png')} style={{ width: '100%', height: '100%', borderRadius: 15 }} />
            </View>

            <View style={styles.viewTwo}>
                <View style={styles.viewThree}>
                    <View style={styles.viewSix}>
                        <Text style={styles.textTwo}>Viaje Isla Contadora</Text>
                        <Text style={styles.textThree}>Viaje para 7 Personas</Text>
                    </View>

                    <View style={styles.viewSeven}>
                        <View style={styles.viewFive}>
                            <Text style={styles.textOne}>ESPACIO</Text>
                        </View>
                    </View>

                </View>

                <View style={styles.viewFour}>
                    <View style={styles.viewSix}>
                        <Text style={styles.textFour}>Check In</Text>
                        <Text style={styles.textFive}>26/11/2016</Text>
                    </View>

                    <View style={styles.viewSeven}>
                        <Text style={styles.textFour}>Guests</Text>
                        <Text style={styles.textFive}>7 Adults</Text>
                    </View>
                </View>


                <View style={styles.viewFour}>
                    <View style={styles.viewSix}>
                        <Text style={styles.textFour}>Booking Confirmation</Text>
                        <Text style={styles.textFive}>YK875765</Text>
                    </View>

                    <View style={styles.viewSeven}>
                        <Text style={styles.textFour}>Price</Text>
                        <Text style={styles.textFive}>$689</Text>
                    </View>
                </View>

            </View>

            <View style={styles.viewEight}>
                <View style={styles.viewNine}>
                    <Image source={require('../../../../../assets/images/Shape.png')} style={{ width: '100%', height: '100%' }} />
                </View>
            </View>

            <View style={styles.viewTen}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.btnContainer}>
                    <Text style={{ color: 'white', fontSize: moderateScale(14, 0.1), fontWeight: '700', paddingRight: 10 }}>
                        CHAT
                    </Text>
                    <ChatSvg />
                </TouchableOpacity>

            </View>
        </View>
   

    )
}

export default ClientBookingConfirmation

const styles = StyleSheet.create({
   
    mainContainer: {
        width:'95%',
        height: moderateScale(640, 0.1),
        // backgroundColor: 'red',
        borderRadius: 10,
        // alignItems: 'center',
        borderWidth:1,
        borderColor:'lightgrey'
       
    },
    viewOne: {
        width: moderateScale(276, 0.1),
        height: moderateScale(175, 0.1),
        // backgroundColor:'green',
        marginTop: 18,
        borderRadius: 15,
        alignSelf: 'center'

    },
    viewTwo: {
        width: '80%',
        height: moderateScale(162, 0.1),
        // backgroundColor: 'green',
        alignSelf: 'center'
    },
    viewThree: {
        height: '40%',
        // backgroundColor: 'purple',
        width: '100%',
        flexDirection: 'row'
    },
    viewFour: {
        height: '30%',
        // backgroundColor: 'pink',
        width: '100%',
        flexDirection: 'row'

    },
    viewFive: {
        width: moderateScale(75, 0.1),
        height: moderateScale(19, 0.1),
        backgroundColor: '#FF5959',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSix: {
        width: '70%',
        height: '100%',
        justifyContent: 'center'
    },
    viewSeven: {
        width: '30%',
        // backgroundColor:'black',
        height: '100%',
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    viewEight: {
        width: '100%',
        height: moderateScale(199, 0.1),
        backgroundColor: 'rgba(50, 54, 67, 0.05)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewNine: {
        width: moderateScale(100, 0.1),
        height: moderateScale(100, 0.1),
    },
    viewTen: {
        width: '100%',
        // backgroundColor: 'orange',
        height: '13%',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf:'center'
    },
    btnContainer: {
        width: moderateScale(246, 0.1),
        height: moderateScale(43, 0.1),
        backgroundColor: '#FF5959',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // backgroundColor:'blue'
    },
    textOne: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'white'
    },
    textTwo: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#273D52',
    },
    textThree: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingTop: 5
    },
    textFour: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643',
    },
    textFive: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#323643',
    },
})
