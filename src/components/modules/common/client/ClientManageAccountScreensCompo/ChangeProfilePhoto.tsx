import React, { useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import BusinessSVG from '../../../../../assets/icons/ProviderSVG/businessSVG'


interface Props {
    realImage?:boolean
}




const ChangeProfilePhoto: React.FC<Props> = ({realImage}) => {

    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewThree}>
                 {realImage ?  
                <Image source={require('../../../../../assets/images/ppic.png')} style={{ height: '100%', width: '100%' }} />
                     : 
                     <View style={styles.viewtwo}>
                     <BusinessSVG />
                 </View>
                 }
                </View>
            <Text style={styles.textOne}>
                Change Profile Photo
            </Text>

        </View>
    )
}

export default ChangeProfilePhoto

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderColor:'rgba(255, 89, 89, 1)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewtwo: {
        width: 60,
        height: 60,
        borderRadius: 30,
        borderColor: '#FF5959',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne:{
        fontFamily:'Montserrat',
        fontSize:moderateScale(13,0.1),
        fontWeight:'500',
        color:'#0E85C6',
        marginTop:10
    }
})
