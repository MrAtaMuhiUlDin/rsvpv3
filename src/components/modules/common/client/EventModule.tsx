import React from 'react'
import { Image, StyleSheet, Text, Touchable, TouchableOpacity, View, ViewStyle } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Accept from '../../../../assets/icons/ProviderSVG/AcceptSVG'
import DenySVG from '../../../../assets/icons/ProviderSVG/DenySVG'
import DustBinSVg from '../../../../assets/icons/ProviderSVG/DustBinSVg'
import OptionsSvg from '../../../../assets/icons/ProviderSVG/OptionsSvg'
import PencilSvg from '../../../../assets/icons/ProviderSVG/PencilSvg'

interface Props {
    type?: 'Options' | 'Request' | 'editOrDel'
    style?: ViewStyle,
    bookings?: boolean,
    ProfilePic?: boolean,
    time?: boolean,
    onPress?:()=>void,
    onPressOptions?:()=>void
}



interface ActionProps {
    actionType?:string,
    OnOptions?:()=>void
}


const Actions = (props:ActionProps) => {

    switch (props.actionType) {
        case 'Options':
            return (
                <TouchableOpacity
                onPress={props.OnOptions}
                style={styles.viewSeven}>
                    <OptionsSvg />
                </TouchableOpacity>
            )

        case 'Request':
            return (
                <View style={styles.viewEight}>
                    <Accept />
                    <DenySVG />
                </View>
            )

        case 'editOrDel':
            return (
                <View style={styles.viewSix}>
                    <PencilSvg />
                    <DustBinSVg />
                </View>
            )

        default: return (
            <View style={styles.viewNine}>
                <PencilSvg />
                <DustBinSVg />
            </View>
        )
    }

}


const EventModule: React.FC<Props> = ({ type, bookings, ProfilePic, time,onPress,onPressOptions }) => {

    return (
        <TouchableOpacity 
        activeOpacity={0.8}
        onPress={onPress}
        style={styles.mainContainer}>

            <View style={styles.viewOne}>

                <Text style={styles.textOne}>18</Text>
                <Text style={styles.textTwo}>June</Text>

            </View>

            <View style={styles.viewTwo}>
                <View style={{ width: '100%', flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.textThree}>Evento</Text>

                    {bookings ?

                        <View style={styles.viewThree}>
                            <Text style={styles.textFour}>2 Bookings</Text>
                        </View>
                        : null}

                    {ProfilePic ?

                        <View style={styles.viewFour}>
                            <Image source={require('../../../../assets/images/Oval.png')} style={{ width: '100%', height: '100%' }} />
                        </View>
                        : null}

                </View>
                <Text style={styles.textFive}>Ubicación, Ubicacion</Text>
                {time ?
                    <Text style={styles.textSix}>8:00 AM</Text>
                    : null}
            </View>

            <View style={styles.viewFive}>
                
                <Actions actionType={type} OnOptions={onPressOptions} />

            </View>

        </TouchableOpacity>
    )
}

export default EventModule


const styles = StyleSheet.create({
    mainContainer: {
        marginVertical: 10,
        backgroundColor: 'white',
        height: moderateScale(69, 0.1),
        flexDirection: 'row',
        width: '100%',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'lightgrey'
    },
    viewOne: {
        height: '100%',
        width: moderateScale(58, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 99, 89, 0.1)',
        borderColor: 'lightgrey',
        borderRightWidth: 1
    },
    viewTwo: {
        height: '100%',
        width: '55%',
        justifyContent: 'center',
        paddingLeft: 10
    },
    viewThree: {
        width: moderateScale(78, 0.1),
        height: moderateScale(19, 0.1),
        backgroundColor: 'rgba(255, 99, 89, 0.2)',
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10
    },
    viewFour: {
        width: moderateScale(15, 0.1),
        height: moderateScale(15, 0.1),
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5
    },
    viewFive: {
        height: '100%',
        width: '27%',
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor:'red'
    },
    viewSix: {
        width: '60%',
        height: '45%',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewSeven: {
        width: '70%',
        height: '50%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewEight: {
        width: '70%',
        height: '45%',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewNine: {
        width: '60%',
        height: '45%',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },

    textOne: {
        fontSize: moderateScale(23, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textTwo: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textThree: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#273D52'
    },
    textFour: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '600',
        fontFamily: 'Poppins',
        color: '#FF5959'
    },
    textFive: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'grey'
    },
    textSix: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'lightgrey'
    }
})