import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ArrowRightWhite from '../../../../assets/icons/ProviderSVG/ArrowRightWhite'
import Event1SVG from '../../../../assets/icons/ProviderSVG/Event1SVG'


const EventsDismisserModule:React.FC = () => {
    return (
        <View style={styles.mainContainer}>

            <View style={styles.viewOne}>
                <View style={styles.viewTwo}>
                    <Event1SVG />
                </View>
                <View style={styles.viewThree}>
                    <Text style={styles.textOne}>Crear tu primer evento</Text>
                </View>
            </View>
            <View style={styles.viewFour}>
                <View style={styles.viewFive}>

                    <TouchableOpacity
                        activeOpacity={0.9}
                        style={styles.viewSix}>
                        <Text style={styles.textTwo}>Dismiss</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        activeOpacity={0.9}
                        style={styles.viewSeven}>
                        <View style={styles.viewEight}>
                            <Text style={styles.textTwo}>Clear</Text>
                            <ArrowRightWhite />
                        </View>

                    </TouchableOpacity>

                </View>
            </View>





        </View>
    )
}

export default EventsDismisserModule


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#FF5959',
        borderRadius: 4,
        height: moderateScale(103, 0.1),
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    viewOne: {
        width: '90%',
        height: '40%',
        flexDirection: 'row'
    },
    viewTwo: {
        width: '18%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: 15
    },
    viewThree: {
        width: '83%',
        height: '100%',
        justifyContent: 'center'
    },
    viewFour: {
        width: '90%',
        height: '35%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    viewFive: {
        width: '55%',
        height: '90%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewSix: {
        height: '100%',
        width: '38%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewSeven: {
        backgroundColor: 'rgba(255, 255, 255, 0.2)',
        borderRadius: 4,
        height: '100%',
        width: '58%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewEight: {
        width: '60%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginRight: 10
    },
    textOne: {
        fontSize: moderateScale(16, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'white'
    },
    textTwo: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'white'
    },

})
