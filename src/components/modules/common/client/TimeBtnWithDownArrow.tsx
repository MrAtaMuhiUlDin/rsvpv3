import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ArrowDownSVG from '../../../../assets/icons/ArrowDownSVG'


interface Props {
    onPress?: () =>void,
    hide?:boolean,
    disabled?:boolean
}


const TimeBtnWithDownArrow:React.FC<Props> = ({onPress,hide,disabled}) => {
    return (
        <TouchableOpacity 
        disabled={disabled}
        onPress={onPress}
        activeOpacity={0.8}
        style={[disabled ?  styles.withoutShadow  :styles.mainContainer,
        {backgroundColor:disabled ? 'transparent': 'white',}
        ]}>
        <View style={styles.viewOne} />
        <View style={styles.viewTwo}>
            <Text style={[styles.textOne,{color :disabled ? 'lightgrey' : '#4D4D4D'}]}>0:00 AM</Text>
        </View>
        {hide?null:
        <View style={styles.viewThree} >
            <ArrowDownSVG color={disabled? 'lightgrey' : 'black'} />
        </View>
          }
    </TouchableOpacity>
    )
}

export default TimeBtnWithDownArrow


const styles = StyleSheet.create({
    mainContainer:{
        width:moderateScale(84,0.1),
        height:moderateScale(36,0.1),
        backgroundColor:'white',
        flexDirection:'row',
        borderRadius:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 1,
      
    },
    viewOne:{
        width:'3%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    viewTwo:{
        width:'80%',
        height:'100%',
        justifyContent:'center',
        alignItems:'center'
    },
    viewThree:{
        width:'17%',
        height:'100%',
        justifyContent:'center',
        // alignItems:'center'
    },
    textOne:{
        fontFamily:'OpenSans',
        fontWeight:'400',
        fontSize:moderateScale(14,0.1),
    },
    withoutShadow:{
        width:moderateScale(84,0.1),
        height:moderateScale(36,0.1),
        backgroundColor:'white',
        flexDirection:'row',
        borderRadius:5,
   
    }
})