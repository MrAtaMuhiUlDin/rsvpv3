import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'


interface Props {
    onPress?:()=>void
}



const CheckListButton:React.FC<Props> = ({onPress}) => {
    return (
        <TouchableOpacity 
        activeOpacity={0.8}
        onPress={onPress}
        style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <Text style={styles.textOne}>CHECKLIST</Text>

                <View style={styles.viewTwo}>
                <Text style={[styles.textOne,{color:'#FF5959',fontSize:moderateScale(14,0.1)}]}>4/9</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default CheckListButton

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(49, 0.1),
        // backgroundColor: 'red',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FF5959',
    },
    viewOne: {
        width: '40%',
        height: '75%',
        // backgroundColor: 'red',
        justifyContent: 'space-between',
        alignItems:'center',
        flexDirection:'row'
    },
    viewTwo: {
        height: moderateScale(31, 0.1),
        width: moderateScale(45, .1),
        backgroundColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '700',
        color: 'white',
        fontFamily: 'OpenSans',

    }
})
