import React from 'react'
import { StyleSheet, Switch, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import SettingSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/SettingSvg'



const CheckListModule:React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>

                <View style={{ width: '100%', height: '40%', flexDirection: 'row' }}>
                    <View style={{ width: '75%',justifyContent:'center' }}>
                      <Text style={styles.textOne}>Split Bill</Text>
                    </View>
                    <View style={styles.viewTwo}>
                      <Switch />
                      <SettingSvg />
                    </View>
                </View>

                <View style={styles.viewThree}>
                      <Text style={styles.textTwo}>Total</Text>
                      <Text style={styles.textThree}>$200</Text>
                </View>

                <View style={styles.viewFour}>
                      <Text style={styles.textFour}>Total Paid </Text>
                      <Text style={styles.textFive}>$130</Text>
                </View>


            </View>
        </View>
    )
}

export default CheckListModule

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(149, 0.1),
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:5
    },
    viewOne: {
        marginLeft: 10,
        width: '88%',
        height: '90%',
        // backgroundColor: 'green',
    },
    viewTwo:{
        width: '25%', 
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        // backgroundColor: 'pink' 
    },
    viewThree:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        height:'30%',
        borderBottomColor:'lightgrey',
        borderBottomWidth:1,
    },
    viewFour:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        height:'30%',
    },
    textOne:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'600',
        fontFamily:'OpenSans',
        color:'#13204D'
    },
    textTwo:{
        fontSize:moderateScale(19,0.1),
        fontWeight:'400',
        fontFamily:'OpenSans',
        color:'#868686'
    },
    textThree:{
        fontSize:moderateScale(21,0.1),
        fontWeight:'700',
        fontFamily:'OpenSans',
        color:'#868686'
    },
    textFour:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'400',
        fontFamily:'OpenSans',
        color:'#FF5959'
    },
    textFive:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'700',
        fontFamily:'OpenSans',
        color:'#FF5959'
    },
})
