import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import EventAddButton from './EventAddButton'
import PlusSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/PlusSvg'



const Servicious: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={{ width: '90%', height: '90%' }}>
                <View style={styles.viewHai}>
                    <View style={styles.viewOne}>
                        <View style={styles.viewThree}>
                            <Text style={styles.textOne}>Paquetes</Text>
                        </View>


                        <View style={styles.viewFive} >
                            <View style={styles.viewSix}>
                                <View style={styles.viewPlusBtn}>
                                    <PlusSvg />
                                </View>

                                <View style={styles.priceBtn}>
                                    <Text style={[styles.textOne, { color: 'white' }]}>$18</Text>
                                </View>
                            </View>
                        </View>

                    </View>

                </View>



                <View style={{ width: '100%', height: '70%' }}>

                    <View style={styles.viewEight}>

                        <View style={{ width: '30%', height: '100%' }}>
                            <Image resizeMode={'cover'} source={require('../../../../../assets/images/yacht.png')}
                                style={{ width: '100%', height: '100%', borderTopLeftRadius: 11, borderBottomLeftRadius: 11 }} />

                        </View>

                        <View style={styles.viewNine}>
                            <View style={styles.viewTen}>
                                <View style={{ justifyContent: 'center' }} >
                                    <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(12, 0.1), fontWeight: '700' }}>Luxury Yacht Service</Text>
                                    <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(12, 0.1), fontWeight: '700', color: '#FF5959' }}>Company Name </Text>
                                </View>

                                <View style={{ justifyContent: 'center' }} >
                                    <View style={styles.priceBtn}>
                                        <Text style={[styles.textOne, { color: 'white' }]}>$18</Text>
                                    </View>

                                </View>



                            </View>
                        </View>



                    </View>



                </View>


            </View>
        </View>
    )
}

export default Servicious

const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(138, 0.1),
        width: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 1.5,
        borderRadius:5
    },
    viewHai: {
        width: '100%',
        height: '30%',
        // backgroundColor:'blue'
    },
    viewOne: {
        width: '90%',
        height: '90%',
        // backgroundColor: 'yellow',
        flexDirection: 'row'
    },
    viewTow: {
        width: '90%',
        height: '90%',
        // backgroundColor: 'yellow',
        flexDirection: 'row'
    },
    viewThree: {
        width: '70%',
        height: '100%',
        // backgroundColor: 'green',
        justifyContent: 'center'
    },
    viewFour: {
        width: '30%',
        height: '100%',
        backgroundColor: 'purple',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    viewFive: {
        width: '42%',
        height: '100%',
        // backgroundColor: 'purple',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewSix: {
        width: '70%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewEight: {
        marginTop: 10,
        width: '100%',
        flexDirection: 'row',
        height: moderateScale(56, 0.1),
        backgroundColor: 'white',
        borderRadius: 11,
        borderWidth: 0.5,
        borderColor: 'lightgrey'
    },
    viewNine: {
        width: '70%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTen: {
        width: '90%',
        height: '90%',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewPlusBtn: {
        width: 20,
        height: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    priceBtn: {
        width: 35,
        height: 35,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(14, 0.1),
        color: '#273D52'
    },

})


