import React, { useState } from 'react'
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CombinedShapeSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/CombinedShape'
import GuestTick from '../../../../../assets/icons/EventDetailsScreenSvgs/GuestTick'
import Loading from '../../../../../assets/icons/EventDetailsScreenSvgs/Loading'
import BLueTick from '../../../../../assets/icons/SearchHeaderSvgs/BLueTick'



interface Props {
    Simple: boolean
}



const Guest: React.FC<Props> = ({ Simple }) => {



    const [disable, setDisable] = useState(false)



    return (
        <View style={styles.mainConatiner}>
            <View style={styles.viewOne}>

                {Simple ? null :
                    <View style={styles.registeredView}>
                        <BLueTick height={6} width={7} color={'white'} />
                    </View>

                }
                <View style={styles.picCircle}>

                    <Image source={require('../../../../../assets/images/OvalPic.png')} style={{ width: '100%', height: '100%' }} />
                </View>

            </View>
            <View style={styles.viewTwo}>
                <Text style={[styles.textOne]}>
                    Esteban Lemus
                </Text>
            </View>


            {Simple ? null :
                <View style={styles.viewThree}>
                    <TouchableOpacity
                        onPress={() => setDisable((prev) => !prev)}
                        activeOpacity={0.8}
                        style={[styles.viewFour, { borderColor: disable ? '#D8DAE3' : '#FF5959' }]}>
                        <View style={styles.viewFive}>
                            <Text style={[styles.textTwo, { color: disable ? '#D8DAE3' : '#FF5959' }]}>
                                $30
                            </Text>
                            <View style={[styles.viewSix, { backgroundColor: disable ? '#D8DAE3' : '#FF5959' }]}>
                                {disable ?
                                    <Loading />
                                    :
                                    <GuestTick />
                                }
                            </View>

                        </View>
                    </TouchableOpacity>
                </View>
            }
        </View>
    )
}

export default Guest

const styles = StyleSheet.create({
    mainConatiner: {
        width: '100%',
        height: moderateScale(61, 0.1),
        backgroundColor: 'white',
        flexDirection: 'row',
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    },
    viewOne: {
        width: '20%',
        height: '100%',
        // backgroundColor:'red',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },
    picCircle: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#FFEBEB',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        width: '55%',
        height: '100%',
        // backgroundColor:'grey',
        justifyContent: 'center'
    },
    viewThree: {
        width: '25%',
        height: '100%',
        // backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',

    },
    textOne: {
        paddingLeft: 5,
        fontSize: moderateScale(15, 0.1),
        fontFamily: 'OpenSans',
        color: '#333333',
        fontWeight: '600'
    },
    textTwo: {
        fontWeight: '600',
        fontSize: moderateScale(15, 0.1),
        fontFamily: 'OpenSans'
    },
    registeredView: {
        width: 14,
        height: 14,
        borderRadius: 7,
        backgroundColor: '#FF5959',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        right: 15,
        bottom: 8,
        zIndex: 1
    },
    viewFour: {
        height: moderateScale(33, 0.1),
        width: moderateScale(78, 0.1),
        backgroundColor: 'white',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    },
    viewFive: {
        width: '80%',
        height: '90%',
        // backgroundColor: 'red',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewSix: {
        width: 28,
        height: 25,
        backgroundColor: '#FF5959',
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5
    }

})
