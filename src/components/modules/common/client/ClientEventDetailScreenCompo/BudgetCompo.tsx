import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale, s } from 'react-native-size-matters'

interface Props {
    height?: string | number,
    bgColor?:string,
    financial?:boolean
}

const BudgetCompo: React.FC<Props> = ({ height,bgColor,financial }) => {

    const defHeight = height ? height : moderateScale(99, 0.1)
    const defBgColor = bgColor ? bgColor : undefined


    return (
        <View style={[styles.mainContainer, { height: defHeight,backgroundColor:defBgColor }]}>
            <View style={[styles.viewOne,{borderRightColor:financial?'white':'rgba(0, 0, 0, 0.3)'}]}>
                <Text style={[styles.textOne,{color:financial?'white':'rgba(50, 50, 50, 0.79)'}]}>
                    Initital Budget
                </Text>

                <Text style={[styles.textTwo,{color:financial?'white':'#263238'}]}>
                    $1000
                </Text>
            </View>

            <View style={[styles.viewOne,{borderRightColor:financial?'white':'rgba(0, 0, 0, 0.3)'}]}>
            <Text style={[styles.textOne,{color:financial?'white':'rgba(50, 50, 50, 0.79)'}]}>
                    Spending on Space
                </Text>

                <Text style={[styles.textTwo,{color:financial?'white':'#263238'}]}>
                    $1000
                </Text>

            </View>

            <View style={styles.viewTwo}>
            <Text style={[styles.textOne,{color:financial?'white':'rgba(50, 50, 50, 0.79)'}]}>
                    Spendings on Place
                </Text>

                <Text style={[styles.textTwo,{color:financial?'white':'#263238'}]}>
                    $1000
                </Text>

            </View>

        </View>
    )
}

export default BudgetCompo

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        // backgroundColor: 'red',
        flexDirection: 'row',
    },
    viewOne: {
        width: '33.3%',
        height: '100%',
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        borderRightColor: 'rgba(0, 0, 0, 0.3)',
        borderRightWidth: 0.5
    },
    viewTwo: {
        width: '33.4%',
        height: '100%',
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(9.5, 0.1),
        fontWeight: '400',
    },
    textTwo: {
        fontFamily: 'Rubik',
        fontSize: moderateScale(20.5, 0.1),
        fontWeight: '700',
        color: '#263238'
    }
})
