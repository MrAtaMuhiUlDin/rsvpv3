import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import PlusSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/PlusSvg'
import DustBinSVg from '../../../../../assets/icons/ProviderSVG/DustBinSVg'
import Accept from '../../../../../assets/icons/ProviderSVG/AcceptSVG'
import DenySVG from '../../../../../assets/icons/ProviderSVG/DenySVG'

interface Props {
    text?: string | any,
    price?: string | number,
    AcceptAndDeny?: boolean
}



const PriceAndDeleteFields: React.FC<Props> = ({ text, price, AcceptAndDeny }) => {
    return (

        <React.Fragment>
            {AcceptAndDeny ?

                <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', height: '24%' }}>

                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.textTwo}>{text}  <Text style={styles.textThree}>{`${(price)}`}</Text></Text>
                        <TouchableOpacity>
                            <View style={styles.viewSix}>
                                <Image source={require('../../../../../assets/images/Oval.png')} style={{ width: '100%', height: '100%' }} />
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <View style={styles.viewEight}>
                            <Accept />
                            <DenySVG />
                        </View>

                    </View>
                </View>

                :

                <View style={{ width: '100%', justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center', height: '24%', borderBottomColor: 'lightgrey', borderBottomWidth: 0.5 }}>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={styles.textTwo}>{text}</Text>
                        <TouchableOpacity>
                            <DustBinSVg color={'#BCBCBC'} />
                        </TouchableOpacity>
                    </View>

                    <View>
                        <Text style={styles.textThree}>{price}</Text>
                    </View>
                </View>

            }


        </React.Fragment>

    )
}

interface PropsOtros {
    onPressModal?:()=>void
}



const Otros: React.FC<PropsOtros> = ({onPressModal}) => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={styles.viewTwo}>
                    <View style={styles.viewThree}>
                        <Text style={styles.textOne}>Otros</Text>
                    </View>
                    <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={onPressModal}
                    style={styles.viewPlusBtn}>
                        <PlusSvg />
                    </TouchableOpacity>
                    <View style={styles.priceBtn}>
                        <Text style={[styles.textOne, { color: 'white' }]}>$18</Text>
                    </View>
                </View>
            </View>



            <View style={styles.viewFour}>
                <View style={{ width: '90%', height: '90%', }}>

                    <PriceAndDeleteFields text={'Cervezas'} price={'$4'} />
                    <PriceAndDeleteFields text={'Cervezas'} price={'$4'} />
                    <PriceAndDeleteFields text={'Cervezas'} price={'$4'} />
                    <PriceAndDeleteFields text={'Cervezas'} price={'$4'} AcceptAndDeny />

                </View>
            </View>



        </View>
    )
}

export default Otros

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(263, 0.1),
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 1.5,
        borderRadius:5

    },
    viewOne: {
        width: '100%',
        height: '25%',
        // backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        width: '90%',
        height: '90%',
        alignItems: 'center',
        //    backgroundColor: 'blue',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewThree: {
        width: '70%',
        height: '100%',
        // backgroundColor: 'green',
        justifyContent: 'center'
    },
    viewSix: {
        width: moderateScale(15, 0.1),
        height: moderateScale(15, 0.1),
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center',

    },
    priceBtn: {
        width: 35,
        height: 35,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    viewPlusBtn: {
        width: 20,
        height: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(14, 0.1),
        color: '#273D52'
    },
    viewFour: {
        width: '100%',
        height: '75%',
        // backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewEight: {
        width: '45%',
        height: '45%',
        // backgroundColor:'green',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    textTwo: {
        fontWeight: '400',
        color: '#273D52',
        fontFamily: 'OpenSans',
        fontSize: moderateScale(13, 0.1),
        paddingRight: 10
    },
    textThree: {
        color: '#273D52',
        fontWeight: '700',
        fontSize: moderateScale(13, 0.1),
    }
})
