import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import PlusSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/PlusSvg'


interface Props {
    onlyAdd?: boolean
}



const EventAddButton: React.FC<Props> = ({ onlyAdd }) => {
    return (
        <View style={styles.mainContainer}>

            <View style={styles.viewOne}>
                <View style={styles.viewThree}>
                    <Text style={styles.textOne}>Paquetes</Text>
                </View>


                {onlyAdd ?
                    <View style={styles.viewFour} >
                        <View style={styles.viewPlusBtn}>
                            <PlusSvg />
                        </View>
                    </View>
                    :
                    <View style={styles.viewFive} >
                        <View style={{ width: '80%', height: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <View style={styles.viewPlusBtn}>
                                <PlusSvg />
                            </View>

                            <View style={styles.priceBtn}>
                                <Text style={[styles.textOne, { color: 'white' }]}>$18</Text>
                            </View>
                        </View>
                    </View>
                }
            </View>

        </View>
    )
}

export default EventAddButton

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(61, 0.1),
        // backgroundColor: 'red',
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderColor: '#E5ECED',
        borderWidth: 0.5
    },
    viewOne: {
        width: '90%',
        height: '90%',
        // backgroundColor: 'yellow',
        flexDirection: 'row'
    },
    viewThree: {
        width: '70%',
        height: '100%',
        // backgroundColor: 'green',
        justifyContent: 'center'
    },
    viewFour: {
        width: '30%',
        height: '100%',
        // backgroundColor: 'purple',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    viewFive: {
        width: '30%',
        height: '100%',
        // backgroundColor: 'purple',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        alignItems: 'center'
    },
    viewPlusBtn: {
        width: 20,
        height: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    priceBtn: {
        width: 35,
        height: 35,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(14, 0.1),
        color: '#273D52'
    },

})
