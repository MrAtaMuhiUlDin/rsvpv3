import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CombinedShapeSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/CombinedShape'



const AddContact:React.FC = () => {
    return (
        <View style={styles.mainConatiner}>
            <View style={styles.viewOne}>

                <View style={styles.picCircle}>
                 <CombinedShapeSvg />
                </View>

            </View>
            <View style={styles.viewTwo}>
                <Text style={styles.textOne}>
                  Add Conatct +
                </Text>
            </View>
          
        </View>
    )
}

export default AddContact

const styles = StyleSheet.create({
    mainConatiner:{
        marginTop:'2%',
        width:'100%',
        height:moderateScale(70,0.1),
        backgroundColor:'white',
        flexDirection:'row'
    },
    viewOne:{
        width:'20%',
        height:'100%',
        // backgroundColor:'red',
        justifyContent:'center',
        alignItems:'center'
    },
    picCircle:{
        width:40,
        height:40,
        borderRadius:20,
        backgroundColor:'#FFEBEB',
        justifyContent:'center',
        alignItems:'center'
    },
    viewTwo:{
        width:'80%',
        height:'100%',
        // backgroundColor:'grey',
        justifyContent:'center'
    },
    textOne:{
        paddingLeft:5,
        fontSize:moderateScale(15,0.1),
        fontFamily:'OpenSans',
        color:'#8D92A6',
        fontWeight:'600'
    }
})
