import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
text?:string,
Svg?:JSX.Element
}



const ClientEventDetailsCompo:React.FC<Props> = ({text,Svg}) => {


    const defaultText = text ? text : 'textHere'


    return (
        <View style={styles.mainContainer}> 
           <View style={styles.viewOne}>
              {Svg}
           </View>

           <View style={styles.viewTwo}>
             <Text style={styles.textOne}>
             {defaultText}
             </Text>
           </View>
        </View>
    )
}

export default ClientEventDetailsCompo

const styles = StyleSheet.create({
mainContainer:{
    width:'100%',
    height:50,
    // backgroundColor:'green',
    flexDirection:'row'
},
viewOne:{
    width:'5%',
    height:'100%',
    // backgroundColor:'grey',
    justifyContent:'center',
    alignItems:'center'
},
viewTwo:{
    width:'95%',
    height:'100%',
    // backgroundColor:'pink',
    justifyContent:'center',
    paddingLeft:'2%'
},
textOne:{
    fontFamily:'OpenSans',
    fontWeight:'600',
    fontSize:moderateScale(15,0.1),
    color:'#8D92A6',
    paddingLeft:5
}
})
