import React, { useRef, useState } from 'react'
import { Button, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import { moderateScale } from 'react-native-size-matters'
import CalendarSVG from '../../../../../assets/icons/calendarSVG';
import DeleteSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/DeleteSvg';
import EditSvgs from '../../../../../assets/icons/EventDetailsScreenSvgs/EditSvgs';
import ShareSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/ShareSvg';
import CalendarClientScreen from '../../../../../screens/Client/calendarClientScreen';



interface Props {
    onPress?:()=>void
}

const CancelBookingBottomSheet:React.FC<Props>  = ({ onPress }) => {
    return (


            <View style={{width:'100%',height:'100%'}}>

                <View style={{width:'100%',height:'30%',flexDirection:'row',alignItems:'center',justifyContent:'space-between',borderBottomColor:'lightgrey',borderBottomWidth:0.5}}>
                <TouchableOpacity
                     activeOpacity={0.8}
                     onPress={onPress}
                     >
                     <Text style={[styles.textTwo,{color:'black'}]}>Cancel</Text>
                </TouchableOpacity>


                <TouchableOpacity
                     activeOpacity={0.8}
                     onPress={onPress}
                     >
                     <Text style={[styles.textTwo]}>Done</Text>
                </TouchableOpacity>
                </View>    



                <View style={{width:'100%',height:'30%',flexDirection:'row',paddingRight:5,alignItems:'center',justifyContent:'space-between',borderBottomColor:'black',borderBottomWidth:0.5}}>
                     <Text style={[styles.textTwo,{color:'black'}]}>Add to Event</Text>
                 <CalendarSVG color={'black'} size={16} />
                </View>  



                <TouchableOpacity style={{width:'100%',height:'36%',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                     <Text style={[styles.textTwo,{color:'#FF0000'}]}>Cancel Booking</Text>
                </TouchableOpacity>  

                

            </View>

    )
}


export default CancelBookingBottomSheet

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#13204D'
    },
    viewTwo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },
    viewThree: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },

    textTwo: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },

})
