import React, { useRef, useState } from 'react'
import { Button, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import { moderateScale } from 'react-native-size-matters'
import DeleteSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/DeleteSvg';
import EditSvgs from '../../../../../assets/icons/EventDetailsScreenSvgs/EditSvgs';
import ShareSvg from '../../../../../assets/icons/EventDetailsScreenSvgs/ShareSvg';


interface Props {
    onPress?: () => void
}


const EventOptionsBottomSheet:React.FC<Props> = ({ onPress }) => {
    return (
        <View style={{ width: '90%', height: 200,alignSelf:'center',paddingTop:20 }}>

            <View style={{ width: '100%', height: '25%', alignItems: 'flex-end' }}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={onPress}
                    style={{ height: '100%' }}>
                    <Text style={styles.textTwo}>Done</Text>
                </TouchableOpacity>
            </View>


            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.viewTwo}>
                <Text>
                    Edit Event
                </Text>
                <EditSvgs />
            </TouchableOpacity>

            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.viewTwo}>
                <Text>
                    Share
                </Text>
                <ShareSvg />
            </TouchableOpacity>


            <TouchableOpacity
                activeOpacity={0.8}
                style={styles.viewThree}>
                <Text style={{ color: '#FF2424' }}>
                    Delete Event
                </Text>
                <DeleteSvg />
            </TouchableOpacity>

        </View>
    )
}


export default EventOptionsBottomSheet

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#13204D'
    },
    viewTwo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },
    viewThree: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },

    textTwo: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },

})
