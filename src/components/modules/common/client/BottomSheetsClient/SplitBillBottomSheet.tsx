import React,{useRef, useState} from 'react'
import { Button, StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import { moderateScale } from 'react-native-size-matters'
import ContentContainer from '../../../../abstract/contentContainer';



interface Props {
onPress?:()=>void,
}




const CheckBoxCompo = ({label,labelFontWeight,labelColor}:{label?:string,labelFontWeight?:any,labelColor?:string}) => {

const [select,setSelect] = useState<boolean>(false)
const defText = label ? label : 'textHere'
const deflabelFontWeight = labelFontWeight ? labelFontWeight : '400'
const deflabelColor = labelColor  ?labelColor : '#4C4C4C'

    return (
        <TouchableOpacity
        activeOpacity={0.8}
        onPress={()=>setSelect((prev)=>!prev)}
        style={{height:30,flexDirection:'row',alignItems:'center'}}>
        <View style={{width:20,height:20,borderRadius:10,borderWidth:1,borderColor:'#EF4339',justifyContent:'center',alignItems:'center'}}>
        {select ? 
         <View style={{width:15,height:15,borderRadius:7.5,backgroundColor:'#EF4339'}} />
        :null}
        </View>


      <Text style={{paddingLeft:5,fontWeight:deflabelFontWeight,fontSize:moderateScale(14,0.1),color:deflabelColor}}>
          {defText}
      </Text>

        </TouchableOpacity>
    )
}


const SplitBillBottomSheet:React.FC<Props> = ({onPress}) => {
    return(
        <View style={{width:'100%',height:170,marginTop:30}}>
            <ContentContainer>
            <View style={{width:'100%',height:60,flexDirection:'row'}}>
              <View style={{width:'80%',height:'100%'}}>
                  <Text style={styles.textOne}>Split Bill Settings</Text>
                  <Text style={styles.textThree}>Choose what you want to split</Text>

              </View>
              <TouchableOpacity
              activeOpacity={0.8}
            onPress={onPress}
              style={{width:'20%',height:'100%',alignItems:'flex-end'}}>
               <Text style={styles.textTwo}>Done</Text>
              </TouchableOpacity>
            </View>



            <View style={{width:'90%',height:75,justifyContent:'space-between',alignSelf:'center',marginVertical:12}}>
                <CheckBoxCompo label={'Plans'} />
                <CheckBoxCompo label={'Others'} />
            </View>
            </ContentContainer>
        </View>
    )
}






export default SplitBillBottomSheet
export {CheckBoxCompo}

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    viewOne:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'600',
        fontFamily:'OpenSans',
        color:'#13204D'
    },
    viewTwo:{

    },
    textOne:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'600',
        fontFamily:'OpenSans',
        color:'#13204D'
    },
    textTwo:{
        fontSize:moderateScale(15,0.1),
        fontWeight:'400',
        fontFamily:'OpenSans',
        color:'#FF5959'
    },
    textThree:{
        fontSize:moderateScale(13,0.1),
        fontWeight:'400',
        fontFamily:'OpenSans',
        color:'#868686'
    }
})
