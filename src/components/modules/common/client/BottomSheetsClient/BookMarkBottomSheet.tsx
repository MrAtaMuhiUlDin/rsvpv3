import React, { useRef, useState } from 'react'
import { Button, StyleSheet, Text, View, TouchableOpacity,ScrollView } from 'react-native'
import RBSheet from "react-native-raw-bottom-sheet";
import { moderateScale } from 'react-native-size-matters'
import CalendarSVG from '../../../../../assets/icons/calendarSVG';
import ContentContainer from '../../../../abstract/contentContainer';
import YourServicesCard from '../../provider/YourServicesCard';


interface Props {
    onPress?:()=>void
}

const BookMarkBottomSheet:React.FC<Props>  = ({ onPress }) => {
    return (

             <ContentContainer>
            <View style={{width:'100%'}}>

                <View style={{width:'100%',height:'12%',flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
              
                <TouchableOpacity
                     activeOpacity={0.8}
                     onPress={onPress}
                     style={{position:'absolute',left:0}}
                     >
                     <Text style={[styles.textTwo,{color:'black'}]}>Close</Text>
                </TouchableOpacity>


                <Text style={{fontSize:moderateScale(19,0.1),fontWeight:'700',color:'#273D52',fontFamily:'OpenSans'}}>Bookmarks</Text>
                </View>    

                <View>
                    <ScrollView showsVerticalScrollIndicator={false}>
                        
                    <YourServicesCard  title={'Chef Privado'} tag tagtext={'SERVICIO'} imageUrl={require('../../../../../assets/images/chef.png')} />
                        <YourServicesCard  title={'Luxury Yacht Rental '} imageUrl={require('../../../../../assets/images/yacht.png')} />
                   

                   <View style={{width:'100%',height:100}}></View>



                    </ScrollView>
                </View>


                



            </View>
            </ContentContainer>

    )
}


export default BookMarkBottomSheet

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: '#13204D'
    },
    viewTwo: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.5,
        borderBottomColor: 'lightgrey',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },
    viewThree: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '25%',
        // backgroundColor: 'pink'
    },

    textTwo: {
        fontSize: moderateScale(15, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },

})
