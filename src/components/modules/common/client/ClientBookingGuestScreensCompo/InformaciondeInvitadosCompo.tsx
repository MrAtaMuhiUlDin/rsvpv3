import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale, moderateVerticalScale } from "react-native-size-matters";
import BLueTick from '../../../../../assets/icons/SearchHeaderSvgs/BLueTick';



const Checks = ({ text }: { text?: string }) => {
    return (
        <View style={{ width: '50%', flexDirection: 'row', height: '20%', alignItems: 'center' }}>
            <BLueTick width={13} height={13} color={'lightgreen'} />
            <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(9, 0.1), fontWeight: '400', paddingLeft: 10, color: '#273D52' }}>
                {text}
            </Text>
        </View>
    )
}








const InformaciondeInvitadosCompo: React.FC = () => {
    return (
        <React.Fragment>
            <View style={styles.shadowView}>
                <View style={styles.viewOne}>
                    <View style={{ width: '90%', height: '70%', flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '55%' }}>
                            <Text style={styles.textOne}>Viaje Isla Contadora</Text>
                            <Text style={styles.textTwo}>Viaje para 7 Personas</Text>
                        </View>
                        <TouchableOpacity style={styles.moneyButton}>
                            <Text style={styles.moneyButtonText}>$950.00</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={styles.viewTwo}>
                    <View style={{ width: '90%', height: '80%', flexDirection: 'row', flexWrap: 'wrap' }}>

                        <Checks text={'Heio'} />
                        <Checks text={'Asador'} />
                        <Checks text={'Gastos de Muelle'} />
                        <Checks text={'Seguro Pasajeros'} />
                        <Checks text={'Asador'} />
                        <Checks text={'Gastos de Muelle'} />
                        <Checks text={'Seguro Pasajeros'} />
                        <Checks text={'Asador'} />
                        <Checks text={'Asador'} />
                        <Checks text={'Asador'} />

                    </View>
                </View>




            </View>

        </React.Fragment>

    )
}

export default InformaciondeInvitadosCompo

const styles = StyleSheet.create({
    shadowView: {
        width: '100%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 1.5,
        borderRadius:2
    },

    viewOne: {
        width: '100%',
        height: moderateVerticalScale(60, 0.1),
        backgroundColor: 'white',
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        justifyContent: 'flex-end',
        alignItems: 'center',

    },
    viewTwo: {
        width: '100%',
        height: moderateScale(167, 0.1),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,

    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans'
    },
    textTwo: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingTop: 5
    },
    moneyButton: {
        width: moderateScale(72, 0.1),
        height: moderateScale(30, 0.1),
        justifyContent: 'center',
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    moneyButtonText: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'white'
    }
})
