import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale, moderateVerticalScale } from "react-native-size-matters";
import BLueTick from '../../../../../assets/icons/SearchHeaderSvgs/BLueTick';





const Checks = ({ text }: { text: string }) => {
    return (
        <View style={{ width: '50%', flexDirection: 'row', height: '20%', alignItems: 'center' }}>
            <BLueTick width={13} height={13} color={'lightgreen'} />
            <Text style={{ fontFamily: 'OpenSans', fontSize: moderateScale(9, 0.1), fontWeight: '400', paddingLeft: 10, color: '#273D52' }}>
                {text}
            </Text>
        </View>
    )
}


const Details = ({heading,detail}:{heading?:string,detail?:string}) => {
    return (
        <View style={{ width: '50%', marginVertical:3 }}>
            <Text style={styles.tetxFour}>{heading}</Text>
            <Text style={styles.textFive}>{detail}</Text>
        </View>
    )
}




const RevisarBookingPagoCompo: React.FC = () => {
    return (
        <React.Fragment>
            <View style={styles.shadowView} >
            <View style={styles.viewOne}>
                <View style={{ width: '90%', height: '70%',flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ width: '55%' }}>
                        <Text style={styles.textOne}>Viaje Isla Contadora</Text>
                        <Text style={styles.textTwo}>Viaje para 7 Personas</Text>
                    </View>
                    <TouchableOpacity style={styles.moneyButton}>
                        <Text style={styles.moneyButtonText}>$950.00</Text>
                    </TouchableOpacity>
                </View>
            </View>


            <View style={styles.viewTwo}>
                <View style={{ width: '90%', height: '80%', flexDirection: 'row', flexWrap: 'wrap' }}>

                    <Checks text={'Heio'} />
                    <Checks text={'Asador'} />
                    <Checks text={'Gastos de Muelle'} />
                    <Checks text={'Seguro Pasajeros'} />
                    <Checks text={'Asador'} />
                    <Checks text={'Gastos de Muelle'} />
                    <Checks text={'Seguro Pasajeros'} />
                    <Checks text={'Asador'} />
                    <Checks text={'Asador'} />
                    <Checks text={'Asador'} />

                </View>
            </View>

            <View style={styles.viewThree}>

                <View style={{ width: '90%', height: '95%',}}>
                    <View>
                        <Text style={styles.textThree}>Guest Details</Text>
                    </View>

                    <View style={{ width: '100%', flexDirection: 'row', flexWrap: 'wrap' }}>
                      <Details heading={'Guest Name'} detail={'Miguel Poso'} />
                      <Details heading={'Guest Phone Number'} detail={'+34 722 111 988'} />
                      <Details heading={'Guest Email Address'} detail={'miguel.poso@gmail.com'} />
                    </View>

                    <View style={{marginTop:10}}>
                        <Text style={styles.textThree}>Payment Details</Text>
                    </View>

                    <View style={{ width: '100%', flexDirection: 'row', flexWrap: 'wrap' }}>
                      <Details heading={'Card Holder'} detail={'Miguel Poso'} />
                      <Details heading={'Card Number'} detail={'xxxx xxxx xxxx xx41'} />
                      <Details heading={'Expiration Date'} detail={'22/07'} />
                      <Details heading={'CCV'} detail={'22/11'} />
                    </View>


                    <View style={{marginTop:5}}>
                        <Text style={styles.textThree}>
                        *Se hará un abono del 50% ahora, y un abono del 50% el dia antes del evento  
                        </Text>
                    </View>

                    <View style={{marginTop:5}}>
                        <Text style={[styles.textThree,{color:'rgba(0, 0, 0, 0.75)'}]}>
                        *Cuando su booking sea confirmado, se hara el cobro a su tarjeta  
                        </Text>
                    </View>
                    

                </View>
            </View>


   </View>



        </React.Fragment>

    )
}

export default RevisarBookingPagoCompo

const styles = StyleSheet.create({
    shadowView: {
        width: '100%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 1.5,
        borderRadius:2
    },
    viewOne: {
        width: '100%',
        height: moderateVerticalScale(60, 0.1),
        backgroundColor: 'white',
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: moderateScale(173, 0.1),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'

    },
    viewThree: {
        width: '100%',
        backgroundColor: 'white',
        height: moderateScale(280, 0.1),
        // justifyContent: 'center',
        alignItems: 'center',
        borderBottomLeftRadius: 3,
        borderBottomRightRadius: 3,
    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans'
    },
    textTwo: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: '#273D52',
        paddingTop: 5
    },
    moneyButton: {
        width: moderateScale(72, 0.1),
        height: moderateScale(30, 0.1),
        justifyContent: 'center',
        borderRadius: 5,
        alignItems: 'center',
        backgroundColor: '#FF5959'
    },
    moneyButtonText: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'white'
    },
    textThree: {
        fontSize: moderateScale(12, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.5)'
    },
    tetxFour: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643'
    },
    textFive: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.5)'
    }
})
