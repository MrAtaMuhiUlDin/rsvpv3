import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity,ViewStyle } from 'react-native'
import { moderateScale } from "react-native-size-matters";
import StarRating from '../provider/StarRating'


interface Props {
    tag?: boolean,
    text?: string,
    imageUrl?: any,
    title?: string,
    style?:ViewStyle
}


const CollectionCard: React.FC<Props> = ({ tag, text, title, imageUrl,style }) => {

    const defText = text ? text : 'tagHere'
    const defTitleText = title ? title : 'titlHere'


    return (
        <View
            style={[styles.mainContainer,style]} >
            <View style={styles.viewOne}>
                <View style={styles.viewTwo}>
                    <View style={styles.viewFive}>
                        <View style={{ width: moderateScale(24, 0.1), height: moderateScale(24, 0.1) }}>
                            <Image source={require('../../../../assets/images/logo.png')} style={{ width: '100%', height: '100%' }} />
                        </View>
                    </View>
                    <View style={{ width: '90%', paddingLeft: 8, justifyContent: 'center' }}>
                        <Text style={styles.textOne}>Company</Text>
                        <Text style={styles.textTwo}>San Cristobal, Panama  </Text>
                    </View>
                </View>
            </View>
            <View style={styles.viewThree}>
                <Image source={imageUrl} style={{ width: '100%', height: '100%' }} resizeMode={'stretch'} />
             
            </View>
            <View style={styles.viewFour}>
                <View style={styles.viewSix}>
                    <Text style={styles.textThree}>{defTitleText}</Text>
                    <View style={{ width: '70%', height: '40%', flexDirection: 'row' }}>
                        <View style={{ width: '45%', justifyContent: 'center' }}>

                            <StarRating rating={5} color={'#FF7373'}  />

                        </View>
                        <View style={{ width: '60%', justifyContent: 'center' }}>
                            <Text style={styles.textFour}>(13 ratings)</Text>
                        </View>
                    </View>
                </View>


            </View>
        </View>
    )
}


export default CollectionCard


const styles =StyleSheet.create({
    mainContainer:{
        width: moderateScale(228,0.1),
        marginVertical:10,
        height: moderateScale(197, 0.1),
        marginRight:15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10.5,
        flexDirection: 'column',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,

        elevation: 2,

    },
    viewOne:{
        width: '100%',
         height: '20%',
          justifyContent: 'center',
           alignItems: 'center'
    },
    viewTwo:{
        width: '95%',
         height: '100%',
         flexDirection: 'row' 
    },
    viewThree:{
        width: '100%',
         height: '58%',
           position: 'relative'
    },
    viewFour:{
        width: '100%',
         height: '22%',
          justifyContent: 'center',
           alignItems: 'center'
    },
    viewFive:{
        width: '10%',
         justifyContent: 'center',
          alignItems: 'center'
    },
    viewSix:{
        width: '85%',
         height: '100%', 
         justifyContent: 'center'
    },
    textOne:{
        fontSize: moderateScale(9, 0.1),
         fontWeight: '600',
          fontFamily: 'OpenSans',
           color: '#323643'
    },
    textTwo:{
        fontSize: moderateScale(7.5, 0.1),
         fontWeight: '400', 
         fontFamily: 'OpenSans',
          color: 'rgba(50,54,67,0.5)' 
    },
    textThree:{
        fontSize: moderateScale(15, 0.1), 
        fontWeight: '700', 
        fontFamily: 'OpenSans',
         color: '#273D52'
    },
    textFour:{
        fontSize: moderateScale(12, 0.1),
         fontWeight: '700',
          fontFamily: 'OpenSans',
           color: '#273D52'
    },
  
})