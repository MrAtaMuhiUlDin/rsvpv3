import React, { useState } from 'react'
import { StyleSheet, Text, View, Image,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ReactionSvg from '../../../../../assets/icons/ChatSvgs/ReactionSvg'


interface Props {
    text?:any
}



const SenderCompo: React.FC<Props> = ({text}) => {

   const [thumbsUp,setThumbsUp] = useState(false)


    return (
        <View style={styles.mainContainer}>


            <View style={{ width: '15%', justifyContent: 'flex-end', alignItems: 'center' }}>
                <View style={{ width: 32, height: 32, borderRadius: 16 }}>
                    <Image source={require('../../../../../assets/images/ChatIcon.png')} style={{ width: '100%', height: '100%', borderRadius: 16 }} />
                </View>
            </View>
            <View style={{ height: '100%', minWidth: '10%', maxWidth: '85%' }}>
                {thumbsUp?
                <View style={{ width: 40, height: 30,  position: 'absolute', right: -22, top: -10,zIndex:1 }}>
                <Image resizeMode={'contain'} source={require('../../../../../assets/images/Reaction.png')} style={{width:'100%',height:'100%'}} />
                {/* <ReactionSvg /> */}
                </View>
                 :null}
                <TouchableOpacity
                activeOpacity={1}
                onLongPress={()=>setThumbsUp((prev)=>!prev)}
                style={{ minHeight: 33, backgroundColor: 'white', borderWidth:1,borderColor:'#ECEBEB',borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomRightRadius: 20 }} >
                    <Text style={{ paddingHorizontal: 18, paddingVertical: 10, fontWeight: '500', fontSize: moderateScale(13, 0.1) }}>
                        {text}
                    </Text>
                </TouchableOpacity>
                <Text style={{ fontWeight: '500', fontSize: moderateScale(13, 0.1), color: '#7A7A7A', paddingTop: 3 }}>
                    3:00 PM
                </Text>
            </View>

        </View>
    )
}

export default SenderCompo

const styles = StyleSheet.create({
    mainContainer: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        position: 'relative',
        marginVertical:10
    }
})
