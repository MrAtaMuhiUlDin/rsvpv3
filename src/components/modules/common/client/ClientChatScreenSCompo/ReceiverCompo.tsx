import React, { useState } from 'react'
import { StyleSheet, Text, View, Image,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ReactionSvg from '../../../../../assets/icons/ChatSvgs/ReactionSvg'
import SeenTick from '../../../../../assets/icons/ChatSvgs/SeenTick'

interface Props {
    text?:any
}




const ReceiverCompo: React.FC<Props> = ({text}) => {

   const [thumbsUp,setThumbsUp] = useState(false)


    return (
        <View style={styles.mainContainer}>


            <View style={{ height: '100%', minWidth: '10%', maxWidth: '100%' }}>
                {thumbsUp?
                <View style={{ width: 40, height: 30,  position: 'absolute', left: -22, top: -10,zIndex:1 }}>
                <Image resizeMode={'contain'} source={require('../../../../../assets/images/Reaction1.png')} style={{width:'100%',height:'100%'}} />
                {/* <ReactionSvg /> */}
                </View>
                 :null}
                <TouchableOpacity
                activeOpacity={1}
                onLongPress={()=>setThumbsUp((prev)=>!prev)}
                style={{ minHeight: 33, backgroundColor: '#ECEBEB', borderWidth:1,borderColor:'#ECEBEB',borderTopLeftRadius: 20, borderTopRightRadius: 20, borderBottomLeftRadius: 20 }} >
                    <Text style={{ paddingHorizontal: 18, paddingVertical: 10, fontWeight: '500', fontSize: moderateScale(13, 0.1) }}>
                        {text}
                    </Text>
                </TouchableOpacity>

                <View style={{flexDirection:'row',justifyContent:'space-between',width:70,alignSelf:'flex-end'}}>
                <SeenTick />
                <Text style={{ fontWeight: '500', alignSelf:'flex-end' ,fontSize: moderateScale(13, 0.1), color: '#7A7A7A'}}>
                    3:00 PM
                </Text>
                </View>
             
            </View>

        </View>
    )
}

export default ReceiverCompo

const styles = StyleSheet.create({
    mainContainer: {
        // backgroundColor: 'red',
        flexDirection: 'row',
        position: 'relative',
        marginVertical:10,
        justifyContent:'flex-end',
        // alignItems:'flex-end'
    }
})
