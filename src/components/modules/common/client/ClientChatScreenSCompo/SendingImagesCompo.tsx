import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Images from 'react-native-chat-images';
import { moderateScale } from 'react-native-size-matters';

interface Props {
    
}

const SendingImagesCompo:React.FC = (props: Props) => {


    const AllImages = [
       {url:'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg'},
       {url:'https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg'},
       {url:'https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg'},
       {url:'https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg'},
       {url:'https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg'},
       {url:'https://cdn.pixabay.com/photo/2021/08/25/20/42/field-6574455__480.jpg'},
       
    ]



    return (
        <View style={{width:moderateScale(253,0.1),
        height:moderateScale(254,0.1),
        backgroundColor:'red',
        alignSelf:'flex-end',
        borderTopLeftRadius:20
        }}>
        <Images
        backgroundColor={'#ECEBEB'}
        images={AllImages} />

        </View>
    )
}

export default SendingImagesCompo

const styles = StyleSheet.create({})
