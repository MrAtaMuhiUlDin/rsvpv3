import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity, TextInput } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AddMore from '../../../../../assets/icons/ChatSvgs/AddMoreSvg'
import SendBTnSVG from '../../../../../assets/icons/ChatSvgs/SendBTnSVG'
import ThunderSvg from '../../../../../assets/icons/ChatSvgs/ThunderSvg'



interface Props {
    
}

const ChatBar = (props: Props) => {
    return (
        <View style={styles.mainContainer}>
           <TouchableOpacity style={styles.viewOne}>
               <AddMore />
           </TouchableOpacity>
           <TouchableOpacity style={styles.viewTwo}>
               <ThunderSvg />
           </TouchableOpacity>
           <View style={styles.viewThree}>
               <View style={styles.viewTextInputOuter}>
               <TextInput placeholder={'Send a message'} style={{paddingLeft:15}} placeholderTextColor={'#7A7A7A'} />

               </View>
           </View>
           <TouchableOpacity style={styles.viewFour}>
               <SendBTnSVG />
           </TouchableOpacity>
        </View>
    )
}

export default ChatBar

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(55,0.1),
        backgroundColor:'white',
        flexDirection:'row'
    },
    viewOne:{
        width:'10%',
        height:'100%',
        // backgroundColor:'orange',
        justifyContent:'center',
        alignItems:'center'
    },
    viewTwo:{
        width:'10%',
        height:'100%',
        // backgroundColor:'green',
        justifyContent:'center',
        alignItems:'center'
    },
    viewTextInputOuter:{
        height:moderateScale(40,0.1),
        width:'100%',
        borderRadius:22,
        borderColor:'#ECEBEB',
        borderWidth:1
    },
    viewThree:{
        width:'65%',
        height:'100%',
        // backgroundColor:'blue',
        justifyContent:'center',
        alignItems:'center'
    },
    viewFour:{
        width:'15%',
        height:'100%',
        // backgroundColor:'yellow',
        justifyContent:'center',
        alignItems:'center'
    }
})
