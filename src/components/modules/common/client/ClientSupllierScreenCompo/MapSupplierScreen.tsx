import React from 'react'
import { ImageBackground, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import NuestroMenuSvg from '../../../../../assets/icons/SupplierScreenSvgs/NuestroMenuSvg'



const MapSupplierScreen:React.FC = () => {
    return (
        <TouchableOpacity
        activeOpacity={0.9}
        style={styles.mainContainer}>
            <ImageBackground 
            source={require('../../../../../assets/images/map.png')} 
            imageStyle={{borderRadius:10}}
            style={{width:'100%',height:'100%'}}>
            <View style={{position:'relative',height:'100%',width:'100%'}}>
                    <View style={styles.Bubble}>
                    <View style={styles.viewOne}>
                       
                    <View style={{width:'80%',height:'100%'}}>
                     <Text
                     style={styles.textOne}
                     >Moll de Barcelona WTC, 08039 Barcelona, Spain</Text>
                     </View>

                    </View>
                   
                    </View>
                    <View style={styles.arrowBorder} />
                    <View style={styles.arrow} />

                 <View style={styles.blueCircle}>
                     <View style={{width:moderateScale(9,0.1),height:moderateScale(9,0.1),borderRadius:10,backgroundColor:'white'}} />
                 </View>    

                </View>
            </ImageBackground>
           
        </TouchableOpacity>
    )
}

export default MapSupplierScreen

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(280,0.1),
        // backgroundColor:'green',
    },
    Bubble: {
        position:'absolute',
        right:40,
        bottom:80,
        flexDirection: "row",
        alignSelf: "flex-start",
        backgroundColor: "#fff",
        borderRadius: 6,
        borderColor: "#ccc",
        borderWidth: 0.5,
        // padding: 15,
        width: moderateScale(186,0.1),
        height: moderateScale(47,0.1),
        // backgroundColor: "#FC5B3F",
        marginTop: 50,
        // borderRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 1.5,
        zIndex:-1,
        justifyContent:'center',
        alignItems:'center'
    },
    arrow: {
        backgroundColor: "transparent",
        borderColor: "transparent",
        borderTopColor: "white",
        borderWidth: 5,
        alignSelf: "center",
        marginTop: -25,
        position:'absolute',
        right:118,
        bottom:68,
        // zIndex:-1
      
    },
    arrowBorder: {
        backgroundColor: "transparent",
        borderColor: "transparent",
        borderTopColor: "white",
        borderWidth: 5,
        alignSelf: "center",
        marginTop: -0.2,
        position:'absolute',
        right:118,
        bottom:68,
        zIndex:1
    },
    viewOne:{
        width:moderateScale(156,0.1),
        height:moderateScale(30,0.1),
        // backgroundColor:'red'
    },
    blueCircle:{
        width:moderateScale(32,0.1),
        height:moderateScale(32,0.1),
        borderRadius:16,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#3FB6F7',
        position:'absolute',
        bottom:'13%',
        right:'30.5%'
    },
    textOne:{
        fontSize:moderateScale(10,0.1),
        fontWeight:'400',
        fontFamily:'OpenSans',
        color:'#323643'
    }
   
})
