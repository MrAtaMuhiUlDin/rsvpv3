import React from 'react'
import { ImageBackground, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import BookmarkSvg from '../../../../../assets/icons/BookmarkSvg'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
import { goBack } from '../../../../../navigation/authNavigator'
import ContentContainer from '../../../../abstract/contentContainer'

interface Props {
    hideBookmark?: boolean,
    onPress?:()=>void
}


const HeaderSupplierScreen: React.FC<Props> = ({ hideBookmark,onPress }) => {
    return (
        <View style={styles.mainContainer}>
            <ImageBackground source={require('../../../../../assets/images/SupplierScreenImage1.png')} resizeMode={'cover'} style={{ width: '100%', height: '100%' }} >
                <ContentContainer style={{ width: '90%' }}>
                    <View style={styles.viewOne}>

                        <TouchableOpacity
                            onPress={()=>goBack()}
                            activeOpacity={0.8}
                            style={styles.btnStyle}>
                            <ChevronLeftSVG size={moderateScale(15, 0.1)} />
                        </TouchableOpacity>



                        {hideBookmark ? null :
                            <TouchableOpacity
                                activeOpacity={0.8}
                                style={styles.btnStyle}>
                                <BookmarkSvg />
                            </TouchableOpacity>
                        }
                    </View>
                </ContentContainer>
            </ImageBackground>
        </View>
    )
}

export default HeaderSupplierScreen

const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(266, 0.1),
        width: '100%',
        // backgroundColor:'green'
    },
    viewOne: {
        width: '100%',
        marginTop: 52,
        height: moderateScale(37, 0.1),
        // backgroundColor:'green',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    btnStyle: {
        width: moderateScale(38, 0.1),
        height: moderateScale(38, 0.1),
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
