import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import BLueTick from '../../../../../assets/icons/SearchHeaderSvgs/BLueTick'

interface Props {
height?:string| number,
width?:string | number,
onlyTick?:boolean,
smallText?:boolean,
onPress?:()=>void
}

const CardAddedBtn:React.FC<Props> = ({height,width,onlyTick,smallText,onPress}) => {

    const [change, setChange] = useState<boolean>(true)
    const defaultHeight = height ? height : moderateScale(31, 0.1)
    const defaultWidth = width ? width : moderateScale(151, 0.1)

    return (
        <TouchableOpacity
            activeOpacity={0.9}
            style={[styles.mainContainer,{  backgroundColor: change ? '#E5E5E5' : '#FF5959',height:defaultHeight,width:defaultWidth}]}
            onPress={onPress}
            >

            {change ?
                
                <>
                {smallText ? 
                 <View style={styles.viewTwo}>
                 <Text style={styles.textTwoSmall}>ADD +</Text>
                 </View>
                :
                <View style={styles.viewTwo}>
                    <Text style={styles.textTwo}>ADD +</Text>
                </View>
                 }

               </>
                :
                <>
                      
                    {onlyTick ? 
                      <View style={styles.viewOnlyTick}> 
                       <BLueTick color={'white'} height={11} width={14}  />
                       </View>
                       :
                       <View style={styles.viewOne}>
                    <Text style={styles.textOne}>ADDED</Text>
                    <BLueTick color={'white'} />
                    </View>
                          }
                </>
            }


        </TouchableOpacity>
    )
}

export default CardAddedBtn

const styles = StyleSheet.create({
    mainContainer: {
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        height: '70%',
        width: '45%',
        // backgroundColor: 'red',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewOnlyTick: {
        height: '70%',
        width: '45%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    },
    viewTwo: {
        height: '70%',
        width: '50%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    textOne: {
        fontFamily: 'Poppins',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
        color: 'white'
    },
    textTwo:{
        color:'#979797',
        fontFamily: 'Poppins',
        fontSize: moderateScale(14, 0.1),
        fontWeight: '700',
    },
    textTwoSmall:{
        color:'#979797',
        fontFamily: 'Poppins',
        fontSize: moderateScale(8, 0.1),
        fontWeight: '600',
    }
})
