import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
   width?:string|number,
   height?:string|number,
   imgUrl?:any,
   marVerticle?:number 
}


const galleryDefProps = {
    width:moderateScale(100,0.1),
    height:moderateScale(100,0.1),
    marginHorizontal:10
}



const GalleryModuleSupplier:React.FC<Props> = ({height,width,imgUrl,marVerticle}) => {
    return (
        <View style={{height:height,width:width,marginHorizontal:10,marginVertical:marVerticle}}>
            <Image source={imgUrl}
             style={{width:'100%',height:'100%',borderRadius:20}} />
        </View>
    )
}

GalleryModuleSupplier.defaultProps = galleryDefProps
export default GalleryModuleSupplier


