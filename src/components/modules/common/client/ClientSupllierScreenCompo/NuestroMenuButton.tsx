import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import NuestroMenuSvg from '../../../../../assets/icons/SupplierScreenSvgs/NuestroMenuSvg'



const NuestroMenuButton:React.FC = () => {
    return (
        <TouchableOpacity
        activeOpacity={0.9}
        style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <Text style={styles.textOne}>NUESTRO MENU</Text>
                <NuestroMenuSvg />
            </View>
        </TouchableOpacity>
    )
}

export default NuestroMenuButton

const styles = StyleSheet.create({
    mainContainer:{
        width:moderateScale(201,0.1),
        height:moderateScale(29,0.1),
        backgroundColor:'white',
        borderColor:'#FF5959',
        borderWidth:2,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center'
    },
    viewOne:{
        height:'100%',
        width:'70%',
        // backgroundColor:'pink',
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row'
    },
    textOne:{
        fontFamily:'OpenSans',
        fontSize:moderateScale(14,0.1),
        fontWeight:'700',
        color:'#FF5959'
    }
})
