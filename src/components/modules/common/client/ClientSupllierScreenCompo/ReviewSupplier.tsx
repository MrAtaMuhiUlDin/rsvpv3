import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import AbstractButton from '../../../../abstract/abstractButton'


const ReviewSupplier: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={{ width: '90%', height: '85%' }}>
                <View style={{ width: '100%', height: '15%' }}>
                    <Text style={styles.textOne}>Write a Review</Text>
                </View>
                <View style={{ width: '100%', height: '65%' }}>

                    <View style={styles.viewTwo}>
                        <Text style={[styles.textOne, { color: '#777777', fontSize: moderateScale(11, 0.1) }]}>
                            Title
                        </Text>
                        <TextInput style={styles.inputStyle} />
                    </View>

                    <View style={styles.viewTwo}>
                        <Text style={[styles.textOne, { color: '#777777', fontSize: moderateScale(11, 0.1) }]}>
                            Review
                        </Text>
                        <TextInput style={styles.inputStyle} />

                    </View>

                    <View style={styles.viewThree}>
                        <View style={styles.viewFour}>
                            <Ionicons name={'star-outline'} size={21} color={'grey'} />
                            <Ionicons name={'star-outline'} size={21} color={'grey'} />
                            <Ionicons name={'star-outline'} size={21} color={'grey'} />
                            <Ionicons name={'star-outline'} size={21} color={'grey'} />
                            <Ionicons name={'star-outline'} size={21} color={'grey'} />
                        </View>
                    </View>

                </View>

                <View style={{ width: '100%', height: '20%', justifyContent: 'flex-end' }}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        style={styles.btnstyle}
                    >
                        <Text style={[styles.textOne, { color: 'white', fontSize: moderateScale(15, 0.1) }]} >Post</Text>
                    </TouchableOpacity>
                </View>

             

            </View>
        </View>
    )
}

export default ReviewSupplier

const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(294, 0.1),
        width: '100%',
        backgroundColor: 'rgba(50, 54, 67, 0.05)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(15, 0.1),
        color: '#323643'
    },
    viewTwo: {
        width: '100%',
        height: '32%',
        // backgroundColor:'pink',

    },
    inputStyle: {
        width: '100%', height: 32, borderBottomWidth: 0.5, borderBottomColor: '#323643'
    },
    viewThree: {
        width: '100%',
        height: '36%',
        // backgroundColor:'grey',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFour: {
        width: '55%',
        height: '50%',
        // backgroundColor:'black',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    btnstyle: {
        width: '100%',
        borderRadius: 10,
        height: moderateScale(45, 0.1),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#323643'
    }
})
