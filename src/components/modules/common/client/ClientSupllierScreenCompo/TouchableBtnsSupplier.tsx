import React, { useState } from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import {moderateScale} from 'react-native-size-matters'

interface Props {
    text?:string,
    onPress?:()=>void
    active?:boolean
}

const TouchableBtnsSupplier:React.FC<Props> = ({text,onPress,active}) => {
  
     const defaultText = text ? text : 'Text'

    return (
        <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.8}
        style={[styles.btnContainer,{backgroundColor:active?'#323643':'rgba(50, 54, 67, 0.05)'}]}>

                <Text style={[styles.textOne,{color:active?'white':'rgba(50, 54, 67, 0.5)'}]}>
                    {defaultText}
                </Text>
             
        </TouchableOpacity>
    )
}

export default TouchableBtnsSupplier

const styles = StyleSheet.create({
    btnContainer: {
        minWidth: moderateScale(73, 0.1),
        maxWidth:moderateScale(85,0.1),
        height: moderateScale(25, 0.1),
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:10,
        paddingHorizontal:15,
    },
    viewTwo: {
        width: '60%',
        height: '70%',
        // backgroundColor: 'green',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        color: 'white',
        fontFamily: 'OpenSans',
        paddingHorizontal:2

    }
})
