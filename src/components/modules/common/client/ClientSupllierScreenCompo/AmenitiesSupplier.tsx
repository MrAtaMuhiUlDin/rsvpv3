import React from 'react'
import { BackHandler, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CameraSvg from '../../../../../assets/icons/SupplierScreenSvgs/CameraSvg'
import CateringSvg from '../../../../../assets/icons/SupplierScreenSvgs/CateringSvg'
import GiftSvg from '../../../../../assets/icons/SupplierScreenSvgs/GiftSvg'

interface Props {

}

const AmenitiesSupplier = (props: Props) => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>

                <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <GiftSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     Wifi
                     </Text>
                    </View>
                </View>
               

                <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <CameraSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     TV
                     </Text>
                    </View>
                </View>
               

                <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <CateringSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     Water
                     </Text>
                    </View>
                </View>
               

            </View>



            <View style={styles.viewOne}>

            <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <GiftSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     Gifts
                     </Text>
                    </View>
                </View>
               

                <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <CameraSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     Photography
                     </Text>
                    </View>
                </View>
               

                <View style={styles.viewTwo}>
                    <View style={styles.iconContainer}>
                    <CateringSvg />
                    </View>
                    <View style={styles.textContainer}>
                     <Text style={styles.textOne}>
                     Catering
                     </Text>
                    </View>
                </View>
               
                

            </View>


        </View>
    )
}

export default AmenitiesSupplier

const styles = StyleSheet.create({
    mainContainer: {
        width: moderateScale(254, 0.1),
        height: moderateScale(100, 0.1),
        // backgroundColor: 'green',
        flexDirection: 'row'
    },
    viewOne: {
        width: '50%',
        height: '100%',
        // backgroundColor: 'yellow',
        flexDirection: 'column'
    },
    viewTwo: {
        width: '100%',
        height: '33.3%',
        // backgroundColor: 'purple',
        flexDirection:'row'
    },
    iconContainer:{
        width:'30%',
        height:'100%',
        // backgroundColor:'grey',
        justifyContent:'center',
        alignItems:'center'
    },
    textContainer:{
        width:'70%',
        height:'100%',
        // backgroundColor:'black',
        justifyContent:'center'
    },
    textOne:{
      fontFamily:'Poppins',
      fontWeight:'400',
      fontSize:moderateScale(13,0.1),
      color:'#323643',

    }
})
