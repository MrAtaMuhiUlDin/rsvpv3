import React, { useState } from 'react'
import { ImageBackground, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractButton from '../../../../abstract/abstractButton'
import CardAddedBtn from './CardAddedBtn'



const CardSupplier: React.FC = () => {

    const [readMore, setReadMore] = useState<number | undefined>(3)
    const [hide, setHide] = useState<boolean>(false)

    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={{ width: '90%', height: moderateScale(25, 0.1) }}>
                    <Text style={styles.textOne}>
                        Trip to Isla Contadora
                    </Text>
                </View>
                <View style={{ width: '90%', height: moderateScale(16, 0.1) }}>
                    <View style={styles.viewAny}>
                        <Text style={styles.textTwo}>
                            Capacity: 7
                        </Text>

                        <View style={{ width: 1, height: '100%', backgroundColor: '#B1B1B1' }} />

                        <Text style={styles.textTwo}>
                            Booking Policy: 100 % Upfront
                        </Text>


                    </View>
                </View>

            </View>
            <View style={styles.viewTwo}>
                <ImageBackground resizeMode={'stretch'} source={require('../../../../../assets/images/supplierImage.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ position: 'absolute', bottom: 10, right: 10 }}>
                        <AbstractButton title={'$430'} width={moderateScale(87, 0.1)} height={moderateScale(23, 0.1)} textSize={moderateScale(13, 0.1)} />
                    </View>
                </ImageBackground>
            </View>
            <View style={styles.viewThree}>
                <View style={{ width: '90%', marginVertical: 15 }}>
                    <Text numberOfLines={readMore} style={styles.textThree}>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Totam nemo vero dolor quod quae repellendus deleniti delectus mollitia amet illum quaerat nulla esse quisquam, architecto dolorum similique, sit perspiciatis tempore. Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio cum quas et quo temporibus iste inventore similique possimus obcaecati iure animi ea beatae, dignissimos neque! Debitis facere tempore ipsa recusandae.
                        <Text style={{ color: 'white' }}>AAAAAAA</Text>
                    </Text>
                    <View style={{ width: '100%', alignItems: 'flex-end' }}>
                        {hide ? null :
                            <Text onPress={() => {
                                setReadMore(undefined)
                                setHide(true)
                            }} style={[styles.readMoreText,{width:70,textAlign:'center',height:16,textAlignVertical:'center',paddingTop:3,position:'absolute',top:1}]}>read more</Text>
                        }
                    </View>
                </View>
            </View>

            

            <View style={styles.viewFour}>
                <CardAddedBtn />
            </View>
        </View>
    )
}

export default CardSupplier

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        marginVertical:15,
        // height: moderateScale(361, 0.5),
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 1.5,
    },
    viewOne: {
        width: '100%',
        height: moderateScale(70, 0.1),
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewAny: {
        width: '65%',
        height: '100%',
        // backgroundColor: 'pink',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: moderateScale(162, 0.1),
        // backgroundColor: 'blue',
        position: 'relative'
    },
    viewThree: {
        width: '100%',
        // height: moderateScale(152,0.1),
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFour: {
        width: '100%',
        height: moderateScale(66, 0.1),
        // backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '600',
        fontFamily: 'Poppins',
        color: '#263238'
    },
    textTwo: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '400',
        fontFamily: 'Poppins',
        color: '#B1B1B1'
    },
    textThree: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '400',
        fontFamily: 'Poppins',
        color: '#323643',
        lineHeight: 15
    },
    readMoreText: {
        color: '#3FB6F7',
        fontWeight: '700',
        fontSize: moderateScale(11, 0.1),
        marginTop: -17,
        backgroundColor: 'white',
        paddingRight: 10
    }
})
