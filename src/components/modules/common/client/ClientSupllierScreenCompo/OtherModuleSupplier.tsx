import React, { useState } from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import Ionicons from 'react-native-vector-icons/Ionicons'
import StarRating from '../../provider/StarRating'



const OtherModuleSupplier: React.FC = () => {

    const [heart, setHeart] = useState(true)



    return (
        <TouchableOpacity
            onPress={() => setHeart((prev) => !prev)}
            activeOpacity={1}
            style={styles.mainContainer}>
            <View style={{ width: '32%', height: '100%' }}>
                <Image source={require('../../../../../assets/images/OtherImage.png')}
                    style={styles.imageStyle} />
            </View>
            <View style={styles.viewOne}>
                <View style={{ width: '90%', height: '90%', flexDirection: 'column' }}>


                    <View style={styles.viewTwo}>
                        <View style={styles.viewThree}>
                            <Text style={styles.textOne}>ESPACIO</Text>
                        </View>
                        {heart ?
                            <Ionicons name={'heart-outline'} color={'grey'} size={18} />
                            :
                            <Ionicons name={'heart'} color={'red'} size={18} />
                        }
                    </View>


                    <View style={{ width: '100%', height: '40%', justifyContent: 'space-between' }}>
                        <Text style={styles.textTwo}>Yate Crazypopper (Yates Boz)</Text>
                        <Text style={styles.textThree}>SAN CRISTOBAL</Text>

                    </View>



                    <View style={{ width: '100%', height: '30%', flexDirection: 'row' }}>
                        <View style={styles.viewFour}>

                            <StarRating color={'#FFC931'} Startsize={moderateScale(9, 0.1)} />

                            <Text style={styles.textFour}>243 Reviews</Text>

                        </View>
                    </View>






                </View>

            </View>
        </TouchableOpacity>
    )
}

export default OtherModuleSupplier

const styles = StyleSheet.create({
    mainContainer: {
        width: '99.5%',
        height: moderateScale(109, 0.1),
        backgroundColor: 'white',
        borderRadius: 11,
        flexDirection: 'row',
        marginVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 5,
            height: 2,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 1.8,
    },
    imageStyle: {
        width: '100%',
        height: '100%',
        borderTopLeftRadius: 11,
        borderBottomLeftRadius: 11
    },
    viewOne: {
        width: '68%',
        // backgroundColor: 'grey',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: '30%',
        // backgroundColor: 'pink',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewThree: {
        width: moderateScale(75, 0.1),
        height: moderateScale(19, 0.1),
        backgroundColor: '#FF5959',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewFour: {
        width: '48%',
        height: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(9, 0.1),
        fontWeight: '600',
        fontFamily: 'OpenSans',
        color: 'white'
    },
    textTwo: {
        fontSize: moderateScale(12, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#323643',
        paddingTop: 5
    },
    textThree: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '400',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.5)',
        paddingBottom: 2
    },
    textFour: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'rgba(50, 54, 67, 0.25)',
        paddingBottom: 2
    }
})
