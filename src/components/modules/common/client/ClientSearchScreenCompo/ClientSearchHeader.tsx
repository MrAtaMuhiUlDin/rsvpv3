import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import AbstractSearchBarCompo from '../../../../abstract/SearchCompo/AbstractSearchBarCompo'
import ContentContainer from '../../../../abstract/contentContainer'
import BookMarkSvg from '../../../../../assets/icons/SearchHeaderSvgs/BookMarkSvg'
import FilterBtnSpecial from '../ClientAdvancedSearcScreenCompo/FilterButtons/FilterBtnSpecial'
import FilterBtnSpecialnormal from '../ClientAdvancedSearcScreenCompo/FilterButtons/FilterBtnSpecialnormal'
import FilterBtnSpecialDate from '../ClientAdvancedSearcScreenCompo/FilterButtons/FilterBtnSpecialDate'
import { navigate } from '../../../../../navigation/authNavigator'


interface Props {
    hideFilterBtn?: boolean,
    icon1?:JSX.Element,
    icon2?:JSX.Element,
    icon3?:JSX.Element,
    icon4?:JSX.Element,
    onPressBookmark?:()=>void
}



const ClientSearchHeader:React.FC<Props> = ({hideFilterBtn,icon1,icon2,icon3,icon4,onPressBookmark}) => {


    const defaultIcon1 = icon1 ? icon1 : undefined
    const defaultIcon2 = icon2 ? icon2 : undefined
    const defaultIcon3 = icon3 ? icon3 : undefined
    const defaultIcon4 = icon4 ? icon4 : undefined




    return (
        <View style={styles.mainContainer}>
            <ContentContainer style={{ width: '90%' }}>
                <View style={{ width: '100%', flexDirection: 'row', marginTop: moderateScale(25, 0.1) }} >

                    <AbstractSearchBarCompo width={'89%'} />

                    <TouchableOpacity
                    onPress={onPressBookmark}
                    activeOpacity={0.8}
                        style={styles.bookmarkBtnContainer}>
                        <BookMarkSvg />
                    </TouchableOpacity>
                </View>


                <View style={styles.viewOne}>
                   <FilterBtnSpecial onPress={()=>navigate('AdvanceSearch')}  />
                    {defaultIcon1}
                    {defaultIcon2}
                    {defaultIcon3}
                    {defaultIcon4}
                    
                </View>

            </ContentContainer>
        </View>
    )
}

export default ClientSearchHeader

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(125, 0.1),
        backgroundColor: 'white'
    },
    bookmarkBtnContainer: {
        width: moderateScale(35, 0.1),
        height: moderateScale(35, 0.1),
        backgroundColor: '#848897',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 3
    },
    viewOne: {
        height: moderateScale(25, 0.1),
        // backgroundColor: 'green',
        width: '100%',
        marginTop: moderateScale(19, 0.1),
        flexDirection:'row-reverse',
        alignItems: 'flex-end'
    },
 
})
