import React from 'react'
import { ImageBackground, StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
 imgUrl?:string | any ,
 text?:string,
 onPress?:()=>void
}

const CelebrationCompo:React.FC<Props> = ({imgUrl,text,onPress}) => {
    return (
        <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.8}
        style={styles.mainContainer}>
            <ImageBackground source={imgUrl} imageStyle={{ borderRadius: 5}} resizeMode={'cover'} style={{width:'100%',height:'100%'}}>
             
             <View style={styles.viewOne} > 
               <Text style={{fontFamily:'Roboto',fontWeight:'500',fontSize:moderateScale(9,0.1),color:'white'}}>
               {text}
               </Text>
             </View>
            

            </ImageBackground>
            
        </TouchableOpacity>
    )
}

export default CelebrationCompo

const styles = StyleSheet.create({
    mainContainer:{
        width:moderateScale(163,0.1),
        height:moderateScale(111,0.1),
        // backgroundColor:'red',
        justifyContent:'center',
        alignItems:'center',
        margin:7.5
    },
    viewOne:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(0, 0, 0, 0.35)',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5
    }
})
