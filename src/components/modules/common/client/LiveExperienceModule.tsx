import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {
    imgUrl?: any,
    text?: string
}

const LiveExperienceModule: React.FC<Props> = ({ imgUrl, text }) => {
    return (
        <View style={styles.viewOne}>
            <Image source={imgUrl} style={{ width: '100%', height: '100%' }} />

            <Text style={styles.textOne}>{text}</Text>

        </View>
    )
}

export default LiveExperienceModule


const styles = StyleSheet.create({
    viewOne: {
        marginHorizontal: 5,
        width: moderateScale(147, 0.1),
        height: moderateScale(150, 0.1),
        backgroundColor: 'rgba(0, 0, 0, 0.35)',
        position: 'relative',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(10, 0.1),
        fontWeight: '500',
        fontFamily: 'OpenSans',
        color: 'white',
        position: 'absolute',
        bottom: 10
    }
})