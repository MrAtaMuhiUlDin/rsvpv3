import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
import { goBack } from '../../../../../navigation/authNavigator'


interface Props {
    onCancel?:()=>void,
    onDone?:()=>void
}


const HeaderAdvanceFilter:React.FC<Props> = ({onDone,onCancel}) => {
    return (
        <View style={styles.mainContainer}>

            <View style={styles.viewOne}>
                <TouchableOpacity
                onPress={onCancel}
                activeOpacity={0.8}
                style={styles.backBtnContainer} >
                       <Text style={styles.textTwo}>Cancel</Text>
                </TouchableOpacity>

                <TouchableOpacity
                onPress={onDone}
                activeOpacity={0.8}
                style={styles.searchBtn} >
                    <Text style={styles.textOne}>Done</Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

export default HeaderAdvanceFilter

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(60, 0.1),
        // backgroundColor: 'green',
        justifyContent: 'flex-end',
    },
    viewOne: {
        width: '100%',
        height: moderateScale(40),
        // backgroundColor: 'grey',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    backBtnContainer: {
        width: 70,
        height: '100%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    searchBtn: {
        width: 70,
        height: '100%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    textOne: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '100',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    },
    textTwo: {
        fontSize: moderateScale(14, 0.1),
        fontWeight: '100',
        fontFamily: 'OpenSans',
        color: 'black'
    }
})
