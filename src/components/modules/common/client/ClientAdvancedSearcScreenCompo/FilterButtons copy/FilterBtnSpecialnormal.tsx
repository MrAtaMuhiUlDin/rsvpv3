import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import TwoArrows from '../../../../../../assets/icons/SearchHeaderSvgs/TwoArrows'
import {moderateScale} from 'react-native-size-matters'

interface Props {
    text?:string
}

const FilterBtnSpecialnormal:React.FC<Props> = ({text}) => {
  
     const defaultText = text ? text : 'Text'


    return (
        <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnContainer}>
                <Text style={styles.textOne}>
                    {defaultText}
                </Text>
             
        </TouchableOpacity>
    )
}

export default FilterBtnSpecialnormal

const styles = StyleSheet.create({
    btnContainer: {
        minWidth: moderateScale(65, 0.1),
        height: moderateScale(25, 0.1),
        backgroundColor: '#323643',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:3,
        paddingHorizontal:15
    },
    viewTwo: {
        width: '60%',
        height: '70%',
        // backgroundColor: 'green',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '700',
        color: 'white',
        fontFamily: 'OpenSans'
    }
})
