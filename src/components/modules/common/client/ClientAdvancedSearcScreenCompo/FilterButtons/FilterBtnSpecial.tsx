import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import TwoArrows from '../../../../../../assets/icons/SearchHeaderSvgs/TwoArrows'
import {moderateScale} from 'react-native-size-matters'

interface Props {
    onPress?:()=>void
}

const FilterBtnSpecial:React.FC<Props> = ({onPress}) => {
    return (
        <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.8}
        style={styles.btnContainer}>
            <View style={styles.viewTwo}>
                <Text style={styles.textOne}>
                    Filtros
                </Text>
                <TwoArrows />
            </View>

        </TouchableOpacity>
    )
}

export default FilterBtnSpecial

const styles = StyleSheet.create({
    btnContainer: {
        width: moderateScale(81, 0.1),
        height: moderateScale(25, 0.1),
        backgroundColor: '#FF5959',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft:2.5
    },
    viewTwo: {
        width: '60%',
        height: '70%',
        // backgroundColor: 'green',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '700',
        color: 'white',
        fontFamily: 'OpenSans'
    }
})
