import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'
import TwoArrows from '../../../../../../assets/icons/SearchHeaderSvgs/TwoArrows'
import {moderateScale} from 'react-native-size-matters'
import CalendarSVG from '../../../../../../assets/icons/calendarSVG'

interface Props {
    text?:string
}

const FilterBtnSpecialDate:React.FC<Props> = ({text}) => {
  
     const defaultText = text ? text : 'Text'


    return (
        <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnContainer}>
               <View style={styles.viewTwo}>

               <CalendarSVG size={10} color={'white'} />

                <Text style={styles.textOne}>
                    {defaultText}
                </Text>
           
            </View>
        </TouchableOpacity>
    )
}

export default FilterBtnSpecialDate

const styles = StyleSheet.create({
    btnContainer: {
        minWidth: moderateScale(65, 0.1),
        height: moderateScale(25, 0.1),
        backgroundColor: '#323643',
        borderRadius: 33,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal:3,
        paddingHorizontal:15
    },
    viewTwo: {
        minWidth: 40,
        height: '70%',
        // backgroundColor: 'green',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(8, 0.1),
        fontWeight: '700',
        color: 'white',
        fontFamily: 'OpenSans',
        paddingHorizontal:5
    }
})
