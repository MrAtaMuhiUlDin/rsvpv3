import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'

interface Props {
    onCancel?:()=>void,
    onDone?:()=>void
}



const HeaderAdvanceSearch:React.FC<Props> = ({onCancel,onDone}) => {
    return (
        <View style={styles.mainContainer}>

            <View style={styles.viewOne}>
                <TouchableOpacity 
                onPress={onCancel}
                activeOpacity={0.8}
                style={styles.backBtnContainer} >
                    <ChevronLeftSVG color={'#323643'} size={16} />
                </TouchableOpacity>

                <TouchableOpacity
                activeOpacity={0.8}
                onPress={onDone}
                style={styles.searchBtn} >
                    <Text style={styles.textOne}>Search</Text>
                </TouchableOpacity>
            </View>

        </View>
    )
}

export default HeaderAdvanceSearch

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(60, 0.1),
        // backgroundColor: 'green',
        justifyContent: 'flex-end',
    },
    viewOne: {
        width: '100%',
        height: moderateScale(30),
        // backgroundColor: 'grey',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    backBtnContainer: {
        width: 50,
        height: '100%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    searchBtn: {
        width: 70,
        height: '100%',
        // backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    textOne: {
        fontSize: moderateScale(19, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: '#FF5959'
    }
})
