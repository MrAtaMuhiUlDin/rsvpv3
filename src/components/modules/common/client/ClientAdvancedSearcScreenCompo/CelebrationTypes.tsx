import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
import BLueTick from '../../../../../assets/icons/SearchHeaderSvgs/BLueTick'

interface Props {
    text?:string
}

const CelebrationTypes: React.FC<Props> = ({text}) => {

    const [change, setChange] = useState<boolean>(false)
    const defText = text ? text : 'textHere'

    return (
        <TouchableOpacity
            onPress={()=>setChange((prev)=>!prev)}
            activeOpacity={0.8}
            style={styles.mainContainer}>

            <View style={styles.viewOne}>
                <Text style={{ fontSize: moderateScale(16, 0.1), fontWeight: '400', fontFamily: 'Lato', color: change ? '#3389EE' : 'black' }}>
                    {defText}
                </Text>
            </View>

            {change ? 
            <View style={styles.viewTwo}>
                <BLueTick />
            </View>
            :null}


        </TouchableOpacity>
    )
}

export default CelebrationTypes

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(55, 0.1),
        // backgroundColor: 'white',
        flexDirection: 'row',
        borderBottomColor:'lightgrey',
        borderBottomWidth:1
    },
    viewOne: {
        width: '95%',
        height: '100%',
        // backgroundColor: 'orange',
        justifyContent: 'center'
    },
    viewTwo: {
        width: '5%',
        height: '100%',
        // backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center'
    }


})
