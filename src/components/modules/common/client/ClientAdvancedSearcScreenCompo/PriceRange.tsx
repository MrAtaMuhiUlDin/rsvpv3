import MultiSlider from '@ptomasroos/react-native-multi-slider'
import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
// import RangeSlider from 'rn-range-slider';

const PriceRange: React.FC = () => {

    const [valuesArray,setvaluesArray] = useState<number[]>([0, 10])

    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>
                <View style={styles.viewTop}>
                    <Text style={styles.headingSmall}>Price Range</Text>
                </View>
                <View style={styles.viewTwo}>
                    <View style={styles.viewThree}>
                        <MultiSlider
                            values={valuesArray}
                            onValuesChange={(values)=>setvaluesArray(values)}
                            isMarkersSeparated={true}
                            // enableLabel
                            enabledOne={true}
                            enabledTwo={true}
                            trackStyle={{ backgroundColor: 'rgba(255, 89, 89, 0.5)' }}
                            markerStyle={{ backgroundColor: '#FF5959' }}
                            selectedStyle={{ backgroundColor: '#FF5959' }}
                        />


                        <View style={{ width: 30, height: 25, position:'absolute',left:20,bottom:0,justifyContent:'center',alignItems:'center' }}>
                           <Text style={{fontFamily:'OpenSans',fontWeight:'600',fontSize:18}}>${valuesArray[0]}</Text>
                        </View>
                        <View style={{ width: 30, height: 25, position:'absolute',right:20,bottom:0,justifyContent:'center',alignItems:'center' }}>
                        <Text style={{fontFamily:'OpenSans',fontWeight:'600',fontSize:18}}>${valuesArray[1]}</Text>
                        </View>

                    </View>
                </View>




            </View>

        </View>
    )
}

export default PriceRange

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(90, 0.1),
        // backgroundColor: 'white',
        // justifyContent:'flex-end',
        position:'relative'
    },
    viewTop: {
        width: '100%',
        height: '25%',
        //    backgroundColor:'purple',
        marginBottom: 10
    },
    viewOne: {
        width: '100%',
        height: '75%',
        // backgroundColor:'purple'
    },
    viewTwo: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewThree: {
        width: '90%',
        height: '100%',
        // backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column'
    },
    headingSmall: {
        fontFamily: 'OpenSans',
        fontSize: moderateScale(11, 0.1),
        fontWeight: '700',
        color: '#273D52',

    }

})
