import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
import DropDownPicker from 'react-native-dropdown-picker';
import ArrowDownSvg from '../../../../../assets/icons/DropDownSvgs/ArrowDownSvg';


interface Props {
    width?:string|number
}



const Dropdown: React.FC<Props> = ({width}) => {


   const defWidth = width ? width : '100%'



    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(null);
    const [items, setItems] = useState([
        { label: 'Apple', value: 'apple' },
        { label: 'Banana', value: 'banana' }
    ]);

    return (
        <View style={styles.mainContainer}>
            <DropDownPicker
                style={{backgroundColor:'white',borderRadius:3,width:defWidth,height:'100%',borderColor:'grey'}}
                ArrowDownIconComponent={({style}) => <ArrowDownSvg  />}
                ArrowUpIconComponent={({style}) => <ArrowDownSvg  />}
                iconContainerStyle={{
                    marginRight: 10,
                    height:50,
                    width:50
                  }}
                showArrowIcon={true}
                placeholder="Celebration Type"
                placeholderStyle={{
                    color: "#273D52",
                    fontWeight:'700',
                    fontFamily:'OpenSans',
                    fontSize:moderateScale(14,0.1),
                    paddingLeft:20
                  }}
                  dropDownContainerStyle={{
                    backgroundColor: "white",
                    borderRadius:3,
                    borderColor:'grey'
                  }}
                open={open}
                value={value}
                items={items}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
            />

        </View>
    )
}

export default Dropdown

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(55, 0.1),
        // backgroundColor: 'green',
    },

})
