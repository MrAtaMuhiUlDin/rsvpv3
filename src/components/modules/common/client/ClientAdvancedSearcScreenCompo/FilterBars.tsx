import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'
import DropDownPicker from 'react-native-dropdown-picker';
import ArrowDownSvg from '../../../../../assets/icons/DropDownSvgs/ArrowDownSvg';


interface Props {
    width?:string|number,
    onPress?:()=>void,
    label?:string,

}



const FilterBars: React.FC<Props> = ({width,onPress,label}) => {

   const defWidth = width ? width : '100%'
   const defText = label ? label : 'textHere'
  
    return (
        <TouchableOpacity
        activeOpacity={0.8}
       onPress={onPress}
        style={styles.mainContainer}>
           <View style={{width:'90%',height:'90%',justifyContent:'space-between',flexDirection:'row',alignItems:'center'}}>
            <Text style={styles.textOne}>
           {defText}
            </Text>

           <View>
               <ArrowDownSvg />
            </View>

           </View>
    
        </TouchableOpacity>
    )
}

export default FilterBars

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(55, 0.1),
        backgroundColor: 'white',
        borderColor:'grey'
        ,borderRadius:3,
        borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },
    textOne:{
        color: "#273D52",
        fontWeight:'700',
        fontFamily:'OpenSans',
        fontSize:moderateScale(14,0.1),
        paddingLeft:10
    }

})
