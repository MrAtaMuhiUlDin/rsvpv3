import React from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../../../assets/icons/chevronLeftSVG'


const HeaderCreateEvent:React.FC = () => {
    return (
        <View style={styles.mainContainer}>

            <View style={styles.viewOne}>
              <Text style={styles.textOne}>
              Create Event
              </Text>
            </View>

        </View>
    )
}

export default HeaderCreateEvent

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(65, 0.1),
        // backgroundColor: 'green',
        justifyContent: 'flex-end',
    },
    viewOne: {
        width: '100%',
        height: moderateScale(30),
        // backgroundColor: 'grey',s
        justifyContent: 'center',
        alignItems:'center',
        flexDirection: 'row'
    },
     textOne:{
        fontFamily:'OpenSans',
        fontWeight:'700',
        fontSize:moderateScale(23,0.1),
        color:'#323643'
     }
})
