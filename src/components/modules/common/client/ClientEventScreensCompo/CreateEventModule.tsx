import React from 'react'
import { StyleSheet, Text, View,Switch } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ClockSvg from '../../../../../assets/icons/ClockSvg'
import DollarSvg from '../../../../../assets/icons/DollarSvg'
import LocationSvg from '../../../../../assets/icons/LocationSvg'



const CreateEventModule: React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <View style={styles.viewOne}>


                <View style={{ width: '100%', height: '20%',flexDirection: 'row' }}>
                    <View style={{ width: '15%', height: '100%',justifyContent:'center',alignItems:'center' }}>
                     <ClockSvg/>
                    </View>
                    <View style={{ width: '30%', height: '100%',justifyContent:'center' }}>
                    <Text style={styles.textOne}>
                    All day
                   </Text>
                    </View>
                    <View style={{ width: '55%', height: '100%',alignItems:'flex-end',justifyContent:'center' }}>
                       
                       <View style={{width:moderateScale(47,0.1),justifyContent:'center',alignItems:'center',height:moderateScale(25,0.1),borderRadius:13,backgroundColor:'white',borderWidth:1,borderColor:'#888DA6'}}>
   
                       <Switch thumbColor={'#888DA6'} trackColor={{true:'white',false:'white'}} />
                       </View>
                  
                    </View>
                </View>


                <View style={{ width: '100%', height: '20%',flexDirection: 'row' }}>
                    <View style={{ width: '15%', height: '100%', justifyContent:'center',alignItems:'center' }}>
                     {/* <ClockSvg/> */}
                    </View>
                    <View style={{ width: '30%', height: '100%', justifyContent:'center' }}>
                    <Text style={styles.textOne}>
                    Starts
                   </Text>
                    </View>
                    <View style={{ width: '55%', height: '100%',alignItems:'flex-end',justifyContent:'center' }}>
                    <Text style={[styles.textOne,{color:'#323643'}]}>
                    Wed, 12 May  7:30 PM
                   </Text>
                    </View>
                </View>



                <View style={{ width: '100%', height: '20%', flexDirection: 'row' }}>
                    <View style={{ width: '15%', height: '100%', justifyContent:'center',alignItems:'center' }}>
                     {/* <ClockSvg/> */}
                    </View>
                    <View style={{ width: '30%', height: '100%',justifyContent:'center' }}>
                    <Text style={styles.textOne}>
                    Ends
                   </Text>
                    </View>
                    <View style={{ width: '55%', height: '100%',alignItems:'flex-end',justifyContent:'center' }}>
                    <Text style={[styles.textOne,{color:'#323643'}]}>
                    9 PM
                   </Text>
                    </View>
                </View>


                <View style={{ width: '100%', height: '20%', flexDirection: 'row' }}>
                    <View style={{ width: '15%', height: '100%', justifyContent:'center',alignItems:'center' }}>
                      <LocationSvg />
                    </View>
                    <View style={{ width: '30%', height: '100%',justifyContent:'center' }}>
                    <Text style={styles.textOne}>
                    Zona
                   </Text>
                    </View>
                    <View style={{ width: '55%', height: '100%',alignItems:'flex-end',justifyContent:'center' }}>
                    <Text style={[styles.textOne,{color:'#323643'}]}>
                    {/* 9 PM */}
                   </Text>
                    </View>
                </View>


                <View style={{ width: '100%', height: '20%', flexDirection: 'row' }}>
                    <View style={{ width: '15%', height: '100%', justifyContent:'center',alignItems:'center' }}>
                      <DollarSvg size={20} />
                    </View>
                    <View style={{ width: '30%', height: '100%',justifyContent:'center' }}>
                    <Text style={styles.textOne}>
                     Budget
                   </Text>
                    </View>
                    <View style={{ width: '55%', height: '100%',alignItems:'flex-end',justifyContent:'center' }}>
                    <Text style={[styles.textOne,{color:'#323643'}]}>
                    {/* 9 PM */}
                   </Text>
                    </View>
                </View>


            </View>
        </View>
    )
}

export default CreateEventModule

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(255, 0.1),
        // backgroundColor: 'green',
        borderTopWidth: 1,
        borderTopColor: '#C4C9DF',
        alignItems: 'center',
        justifyContent: 'center'
    },
    viewOne: {
        width: '100%',
        height: '80%',
        // backgroundColor: 'red'
    },
    textOne:{
        fontFamily:'OpenSans',
        fontWeight:'600',
        fontSize:moderateScale(15,0.1),
        color:'#8D92A6'
     }
})
