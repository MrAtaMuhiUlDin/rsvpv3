import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CVCSvg from '../../../../../assets/icons/PaymentSvgs/CVCSvg'
import AbstractTextInput from '../../../../abstract/abstractTextInput'

const Expiry:React.FC = () => {
    return (
        <View style={styles.mainContainer}>
            <AbstractTextInput Fonts={'OpenSans'} label={'Expiry'} placeholderText={'11/25'} width={'30%'} />
        </View>
    )
}

export default Expiry

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(69,0.1),
        // backgroundColor:'red',
        flexDirection:'row'
    },
    viewOne:{
        width:'10%',
        // backgroundColor:'green',
        height:'70%',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'flex-end',
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    }
})
