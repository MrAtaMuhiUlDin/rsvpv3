import React from 'react'
import { StyleSheet, Text, TextInput, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import CVCSvg from '../../../../../assets/icons/PaymentSvgs/CVCSvg'
import AbstractTextInput from '../../../../abstract/abstractTextInput'

const CVC:React.FC = () => {
    return (
        <View style={styles.mainContainer}>
           
           <View style={{width:'70%',height:'100%'}}>
              <Text style={styles.textOne}>CVC</Text>
              <TextInput value={'e.g 11/25'} placeholder={'e,g 11/25'} style={{fontSize:15,fontFamily:'OpenSans',height:moderateScale(32,0.1),padding:0,width:'100%'}} />
            </View>

            <View style={{width:'30%',height:'100%',justifyContent:'center',alignItems:'flex-end'}}>
            <View style={{marginRight:-20,paddingTop:10}}>
            <CVCSvg />
            </View>
            </View>

        </View>
    )
}

export default CVC

const styles = StyleSheet.create({
    mainContainer:{
        width:moderateScale(95,0.1),
        height:moderateScale(51,0.1),
        // backgroundColor:'red',
        flexDirection:'row',
        borderBottomColor:'#C4C9DF',
        borderBottomWidth:1,
        marginRight:30
        
    },
    viewOne:{
        width:'20%',
        // backgroundColor:'green',
        height:'70%',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'flex-end',
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    },
    textOne:{
         fontFamily:'OpenSans',
         fontWeight:'600',
         fontSize:moderateScale(11,0.1),
         color:'#868E96'
    }
})
