import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import VisaTextSvg from '../../../../../assets/icons/PaymentSvgs/VisaTextSvg'



const PaymentModuleCard: React.FC = () => {
    return (
        <View style={styles.mainConatiner}>
           <ImageBackground source={require('../../../../../assets/images/CreditCardBackground.png')} style={{width:'100%',height:'100%'}}>
            <ImageBackground source={require('../../../../../assets/images/Effect.png')}
                style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
 

                <View style={styles.viewOne}>
                    <View style={styles.viewTwo}>
                        <Text style={styles.textOne}>
                            Credit
                        </Text>
                        <VisaTextSvg />
                    </View>

                    <View style={styles.viewThree}>
                        <Text style={styles.textTwo}>
                            Card Holder
                        </Text>

                        <Text style={styles.textTwo}>
                            4556 - 5642 - 0695 - 5168
                        </Text>
                    </View>

                </View>

           </ImageBackground>
            </ImageBackground>

        </View>
    )
}

export default PaymentModuleCard

const styles = StyleSheet.create({
    mainConatiner: {
        width: moderateScale(241, 0.1),
        height: moderateScale(132, 0.1),
        // backgroundColor: 'green',
        borderRadius: 14,
        justifyContent: 'center',
        alignItems: 'center',
        position:'relative'
    },
    viewOne: {
        width: '85%',
        height: '85%',
        // backgroundColor: 'green',
        justifyContent: 'space-between'

    },
    viewTwo: {
        width: '100%',
        height: '25%',
        // backgroundColor: 'blue',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewThree: {
        width: '100%',
        height: '30%',
        // backgroundColor: 'red',
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginBottom:10
    },
    textOne: {
        fontFamily: 'Ubuntu',
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        color: 'white'
    },
    textTwo: {
        fontFamily: 'IBMPlexMono',
        fontSize: moderateScale(11, 0.1),
        fontWeight: '400',
        color: 'white'
    }
})
