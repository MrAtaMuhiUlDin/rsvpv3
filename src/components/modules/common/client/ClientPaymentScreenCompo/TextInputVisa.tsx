import React from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import VisaCardSvg from '../../../../../assets/icons/PaymentSvgs/VisaCardSvg'
import AbstractTextInput from '../../../../abstract/abstractTextInput'

const TextInputVisa:React.FC = () => {
    return (
        <View style={styles.mainContainer}>
                
                <View style={{width:'85%',height:'90%'}}>
                <Text style={styles.textOne}>Card Number</Text>
              <TextInput value={'12336552 22222 55'} placeholder={'1234567899'} style={{fontSize:15,fontFamily:'OpenSans',height:moderateScale(50,0.1),padding:0,width:'100%'}} />
                </View>
                <View style={{width:'15%',height:'100%',justifyContent:'center',alignItems:'center'}}>
                
                
                <View style={{paddingTop:15,paddingLeft:5}}>
                <VisaCardSvg  height={19} width={26} />
                    
                </View>

                </View>






            {/* <AbstractTextInput Fonts={'OpenSans'} label={'Card Number'} width={'85%'} placeholderText={'3435 5565 6546 7543'} />
           <View style={styles.viewOne}>
           </View> */}
        </View>
    )
}

export default TextInputVisa

const styles = StyleSheet.create({
    mainContainer:{
        width:'100%',
        height:moderateScale(62,0.1),
        // backgroundColor:'red',
        flexDirection:'row',
        borderBottomColor:'#C4C9DF',
        borderBottomWidth:1,
    },
    viewOne:{
        width:'15%',
        // backgroundColor:'green',
        height:'70%',
        justifyContent:'center',
        alignItems:'center',
        alignSelf:'flex-end',
        borderBottomWidth:1,
        borderBottomColor:'lightgrey'
    },

    textOne:{
        fontFamily:'OpenSans',
        fontWeight:'600',
        fontSize:moderateScale(11,0.1),
        color:'#868E96'
   }
})
