import React from 'react'
import { BackHandler, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import MasterCardSvg from '../../../../../assets/icons/PaymentSvgs/MasterCardSvg'
import VisaCardSvg from '../../../../../assets/icons/PaymentSvgs/VisaCardSvg'

interface Props {
    
}

const AvailableOptions:React.FC = (props: Props) => {
    return (
        <View style={styles.mainContainer}>

            <MasterCardSvg />
            <VisaCardSvg />
           
        </View>
    )
}

export default AvailableOptions

const styles = StyleSheet.create({
    mainContainer:{
        width:'25%',
        height:moderateScale(25,0.1),
        backgroundColor:'red',
        flexDirection:'row',
        justifyContent:'space-between'
    }
})
