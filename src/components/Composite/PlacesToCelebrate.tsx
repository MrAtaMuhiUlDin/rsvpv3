import React from 'react'
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'

interface Props {

}

const PlacesToCelebrate: React.FC = () => {
  return (
    <View style={styles.mainContainer}>

      <View style={styles.viewOne}>
        <View style={styles.viewTwo}>
          <ImageBackground source={require('../../assets/images/Hotels.png')} style={styles.viewThree} >
            <Text style={styles.textOne}>Hotel Loungess</Text>
          </ImageBackground>
        </View>


        <View style={styles.viewFour}>
          <ImageBackground source={require('../../assets/images/Disco.png')} style={styles.viewFive} >
            <Text style={styles.textOne}>Disco</Text>
          </ImageBackground>
        </View>
      </View>

      <View style={styles.viewSix}>

        <View style={styles.viewSeven}>
          <ImageBackground source={require('../../assets/images/Yatch.png')} style={styles.viewEight} >
            <Text style={styles.textOne}>Yatch</Text>
          </ImageBackground>
        </View>

        <View style={styles.viewNine}>
          <ImageBackground source={require('../../assets/images/RoofTop.png')} style={styles.viewEight} >
            <Text style={styles.textOne}>Rooftop</Text>
          </ImageBackground>
        </View>
      </View>


      <View style={styles.viewTen}>

        <View style={styles.viewNine}>
          <ImageBackground source={require('../../assets/images/Bars.png')} style={styles.viewEight} >
            <Text style={styles.textOne}>Bars</Text>
          </ImageBackground>
        </View>

        <View style={styles.viewNine}>
          <ImageBackground source={require('../../assets/images/OutDoor.png')} style={styles.viewEight} >
            <Text style={styles.textOne}>Outdoor</Text>
          </ImageBackground>
        </View>
      </View>


    </View>
  )
}

export default PlacesToCelebrate

const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: moderateScale(267, 0.1),
    backgroundColor: 'white',
    justifyContent: 'space-evenly'
  },
  viewOne: {
    width: '100%',
    height: '33.4%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  viewTwo: {
    height: moderateScale(72, 0.1),
    width: '45.5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewThree: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.39)'
  },
  viewFour: {
    height: moderateScale(72, 0.1),
    width: '45.5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewFive: {
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.39)',
    alignItems: 'center',
    width: '100%', height: '100%'
  },
  viewSix: {
    width: '100%',
    height: '33.4%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  viewSeven: {
    height: moderateScale(72, 0.1),
    width: '45.5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewEight: {
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.39)',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  viewNine: {
    height: moderateScale(72, 0.1),
    width: '45.5%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewTen:{
    width: '100%',
     height: '33.4%',
      flexDirection: 'row',
       justifyContent: 'space-evenly',
        alignItems: 'center'
  },
  textOne: {
    fontSize: moderateScale(12, 0.1),
    fontWeight: '500',
    fontFamily: 'Roboto',
    color: 'white'

  },

})
