import React from 'react';
import {View, ViewStyle} from 'react-native';

interface Props {
  children?: Element;
  style?: ViewStyle;
}
const ContentContainer: React.FC<Props> = ({children, style}) => {
  return (
    <View style={[{width: '85%', alignSelf: 'center'}, style]}>{children}</View>
  );
};
export default ContentContainer;
