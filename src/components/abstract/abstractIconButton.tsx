import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Colors} from '../../theme/colors';

interface IconProps {
  size: number;
}

interface Props {
  Icon: React.FC<IconProps>;
  size?: number;
  onPress?: () => void;
}

const iconButtonDefaultProps = {
  size: 60,
};

const AbstractIconButton: React.FC<Props> = ({Icon, size, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, {width: size, height: size}]}>
      <Icon size={size ? size * 0.5 : 40} />
    </TouchableOpacity>
  );
};
AbstractIconButton.defaultProps = iconButtonDefaultProps;
export default AbstractIconButton;

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: Colors.SECONDRY,
  },
});
