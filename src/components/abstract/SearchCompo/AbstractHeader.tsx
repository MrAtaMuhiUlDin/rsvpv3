import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ChevronLeftSVG from '../../../assets/icons/chevronLeftSVG'
import OptionsSvg from '../../../assets/icons/OptionsSvg'
import BLueTick from '../../../assets/icons/SearchHeaderSvgs/BLueTick'




const HeaderForManageAccount = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={{width:'100%',height:moderateScale(60,0.1),backgroundColor:'white'}}>
            <View style={{width:'100%',height:'100%',flexDirection:'row'}}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={{width:'15%',justifyContent:'center',alignItems:'center'}}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>


                <View style={{width:'65%',justifyContent:'center',alignItems:'flex-start'}}>
                    <Text style={styles.textOne}>Manage My Account</Text>
                </View>

                <View style={{width:'20%',justifyContent:'center'}}>
                    <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <BLueTick width={17} height={13} color={'#0E85C6'} />
                        <Text style={styles.textTwo}>Save</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>

    )
}





const ProviderFinancialSetting = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={{width:'100%',height:moderateScale(60,0.1)}}>
            <View style={{width:'100%',height:'100%',flexDirection:'row'}}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={{width:'16%',justifyContent:'center',alignItems:'center'}}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>


                <View style={{alignItems:'flex-start',width:'64%',justifyContent:'center'}}>
                    <Text style={styles.textOne}>Finance Settings</Text>
                </View>

                <View style={{alignItems:'center',width:'20%',justifyContent:'center'}}>
                    <TouchableOpacity style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                        <BLueTick width={17} height={13} color={'#0E85C6'} />
                        <Text style={styles.textTwo}>Save</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>

    )
}



const ChatHeader = ({ name, onPressBackToSimple }: { name?: string, onPressBackToSimple?: () => void }) => {

    const defName = name ? name : 'Daniel Atkinson'


    return (
        <View style={styles.mainContainerForChatHeader}>


            <View style={[styles.viewOne, { height: moderateScale(50, 0.1) }]}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={[styles.viewLeft, { paddingRight: 10 }]}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>


                <View style={[styles.viewMiddle, { alignItems: 'center' }]}>
                    <Text style={styles.textThree}>{defName}</Text>
                </View>

                <View style={styles.viewRight}>
                    <View style={styles.conatinerImageView}>
                        <Image source={require('../../../assets/images/10.png')} style={{ width: '100%', height: '100%', borderRadius: 20 }} />

                        <View style={styles.greenActiveConatiner}>
                            <View style={styles.greenActiveView}></View>
                        </View>

                    </View>

                </View>
            </View>

        </View>
    )
}




const SimpleBackHeader = ({ onPressBackToSimple, changeBackgroundColor }: { onPressBackToSimple?: () => void, changeBackgroundColor?: string }) => {

    const defaultBgColor = changeBackgroundColor ? changeBackgroundColor : 'white'

    return (
        <View style={[styles.mainContainer, {height:moderateScale(55,0.12),backgroundColor:defaultBgColor }]}>
            <View style={[styles.viewOne,{height:moderateScale(55,0.1)}]}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={[styles.viewLeft, { width: '20%' }]}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>

                <View style={[styles.viewMiddle, { width: '80%' }]}>
                </View>

                <View style={[styles.viewRight, { width: '0%' }]}>
                </View>
            </View>
        </View>

    )
}



const FinancialHeader = ({ onPressBackToSimple, changeBackgroundColor }: { onPressBackToSimple?: () => void, changeBackgroundColor?: string }) => {

    const defaultBgColor = changeBackgroundColor ? changeBackgroundColor : 'white'

    return (
        <View style={[{width:'100%',height:moderateScale(60,0.1),backgroundColor:'white'}, { backgroundColor: defaultBgColor }]}>
            <View style={{width:'100%',height:'100%',flexDirection:'row'}}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={[styles.viewLeft, { width: '15%' }]}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>

                <View style={[styles.viewMiddle, { width: '85%', alignItems: 'flex-start' }]}>
                    <Text style={{ fontFamily: 'OpenSans', fontWeight: '600', fontSize: moderateScale(16, 0.1) }}>Financials</Text>
                </View>

                <View style={[styles.viewRight, { width: '0%' }]}>
                </View>
            </View>
        </View>

    )
}







const EventScreenHeader1 = ({ onPressBackToSimple, onPressOptionsToSimple }: { onPressBackToSimple?: () => void,onPressOptionsToSimple?:()=>void }) => {
    return (
        <View style={[styles.mainContainer,{height:moderateScale(55,0.1)}]}>
            <View style={[styles.viewOne,{height:moderateScale(55,0.1)}]}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={[styles.viewLeft, { width: '10%' }]}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>


                <View style={[styles.viewMiddle, { width: '80%' }]}>
                    <Text style={styles.textFour}>Boda</Text>
                </View>

                <TouchableOpacity
                onPress={onPressOptionsToSimple}
                style={[styles.viewRight, { width: '10%'}]}>
                
                        <OptionsSvg color={'black'} />
                
                </TouchableOpacity>
            </View>
        </View>

    )
}


const EventScreenHeader2 = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={[styles.mainContainer, { height: moderateScale(100, 0.1), backgroundColor: '#FF5959' }]}>
            <View style={[styles.viewOne, { height: 50 }]}>



                <View style={[styles.viewMiddle, { width: '100%', flexDirection: 'row', position: 'relative' }]}>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={onPressBackToSimple}
                        style={{ position: 'absolute', left: 10 }}>
                        <ChevronLeftSVG size={16} color={'white'} />
                    </TouchableOpacity>
                    <Text style={[styles.textFour, { color: 'white' }]}>Boda</Text>
                </View>


            </View>

            <View style={{ width: '100%', height: moderateScale(35, 0.1), alignItems: 'center' }}>
                <Text style={[styles.textFive, { paddingLeft: 2 }]}>
                    Checklist
                </Text>

            </View>

        </View>

    )
}




const CreateEventHeader = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={[styles.mainContainer, { height: moderateScale(60, 0.1) }]}>
            <View style={styles.viewOne}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={styles.viewLeft}>
                    {/* <ChevronLeftSVG size={16} color={'#323232'}  /> */}
                </TouchableOpacity>


                <View style={styles.viewMiddle}>
                    <Text style={[styles.textFour, { fontWeight: '700' }]}>Create Event</Text>
                </View>

                <View style={styles.viewRight}>
                    <TouchableOpacity>
                        {/* <OptionsSvg /> */}
                    </TouchableOpacity>

                </View>
            </View>
        </View>

    )
}



const AddGuestHeader = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={styles.mainContainerGuest}>
            <View style={styles.viewOne}>
                <TouchableOpacity
                    onPress={onPressBackToSimple}
                    style={[styles.viewLeft, { width: '10%', alignItems: 'flex-end' }]}>
                    <ChevronLeftSVG size={16} color={'#323232'} />
                </TouchableOpacity>


                <View style={[styles.viewMiddle, { width: '80%' }]}>
                    <Text style={styles.textFour}>Add Guests</Text>
                </View>

                <View style={[styles.viewRight, { width: '20%' }]}>
                    <TouchableOpacity>
                        {/* <OptionsSvg /> */}
                    </TouchableOpacity>

                </View>
            </View>
        </View>

    )
}



const ProviderStepsHeader = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={[{ height: moderateScale(60, 0.1), flexDirection: 'row', backgroundColor: 'white' }]}>

            <TouchableOpacity onPress={onPressBackToSimple} style={{ width: '10%', height: '100%', alignItems: 'flex-end', justifyContent: 'center' }}>
                <ChevronLeftSVG size={16} color={'#323232'} />
            </TouchableOpacity>
            <View style={{ width: '60%', height: '100%' }}>

            </View>
            <TouchableOpacity style={{ width: '30%', height: '100%', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ color: '#FF5959', fontFamily: 'OpenSans', fontSize: moderateScale(14, 0.1), fontWeight: '600' }}>Save as Draft</Text>
            </TouchableOpacity>
        </View>

    )
}



const SimpleRsvp = ({ onPressBackToSimple }: { onPressBackToSimple?: () => void }) => {
    return (
        <View style={[styles.mainContainer, { height: moderateScale(60, 0.1), justifyContent: 'center', alignItems: 'center' }]}>

            <View style={{ width: moderateScale(76, 0.1), height: moderateScale(29, 0.1) }}>
                <Image source={require('../../../assets/images/rsvpLogo.png')} style={{ width: '100%', height: '100%' }} />

            </View>
        </View>

    )
}


interface Props {
    type?: string,
    onPressBack?: () => void,
    bgColor?: string,
    onPressOptions?:()=> void
}



const HandlingHeaders = ({ Headertype, onPressBackToHeader, changBgColor,onPressOptionsToCheck }: { Headertype?: string, onPressBackToHeader?: () => void, changBgColor?: string,onPressOptionsToCheck?:()=>void }) => {
    switch (Headertype) {
        case 'SimpleRsvp':
            return <SimpleRsvp onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'HeaderForManageAccount':
            return <HeaderForManageAccount onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'ChatHeader':
            return <ChatHeader onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'AddGuestHeader':
            return <AddGuestHeader onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'CreateEventHeader':
            return <CreateEventHeader onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'EventScreenHeader2':
            return <EventScreenHeader2 onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'EventScreenHeader1':
            return <EventScreenHeader1 onPressBackToSimple={onPressBackToHeader} onPressOptionsToSimple={onPressOptionsToCheck} />
            break;
        case 'SimpleBackHeader':
            return <SimpleBackHeader onPressBackToSimple={onPressBackToHeader} changeBackgroundColor={changBgColor} />
            break;
        case 'FinancialHeader':
            return <FinancialHeader onPressBackToSimple={onPressBackToHeader} changeBackgroundColor={changBgColor} />
            break;
        case 'ProviderStepsHeader':
            return <ProviderStepsHeader onPressBackToSimple={onPressBackToHeader} />
            break;
        case 'ProviderFinancialSetting':
            return <ProviderFinancialSetting onPressBackToSimple={onPressBackToHeader} />
            break;
        default: <SimpleBackHeader onPressBackToSimple={onPressBackToHeader} changeBackgroundColor={changBgColor} />
    }
    return <SimpleBackHeader onPressBackToSimple={onPressBackToHeader} />
}



const AbstractHeader: React.FC<Props> = ({ type, onPressBack, bgColor,onPressOptions }) => {
    return (

        <HandlingHeaders
        Headertype={type}
        onPressBackToHeader={onPressBack}
        changBgColor={bgColor}
        onPressOptionsToCheck={onPressOptions}
        />

    )
}

export default AbstractHeader

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: moderateScale(50, 0.1),
        backgroundColor: 'white',
        justifyContent: 'flex-end',
    },
    mainContainerGuest: {
        // shadowColor: 'rgba(255, 255, 255, 0.9)',
        // shadowOffset: { width: 1, height: 2 },
        // shadowOpacity:  0.2,
        // shadowRadius: 4,
        // elevation: 5,
        width: '100%',
        height: moderateScale(50, 0.1),
        backgroundColor: 'white',
        justifyContent: 'center',
    },
    mainContainerForChatHeader: {
        width: '100%',
        height: moderateScale(50, 0.1),
        backgroundColor: 'white',

    },
    viewOne: {
        width: '100%',
        height: moderateScale(70),
        // backgroundColor: 'grey',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    viewLeft: {
        width: '20%',
        height: '100%',
        // backgroundColor: 'purple',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewMiddle: {
        width: '60%',
        height: '100%',
        // backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center'
    },
    viewRight: {
        width: '20%',
        height: '100%',
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textOne: {
        color: '#323643',
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(17, 0.1)
    },
    textTwo: {
        color: '#0E85C6',
        fontFamily: 'OpenSans',
        fontWeight: '500',
        fontSize: moderateScale(13, 0.1)
    },
    textThree: {
        color: 'black',
        fontFamily: 'OpenSans',
        fontWeight: '700',
        fontSize: moderateScale(13, 0.1)
    },
    conatinerImageView: {
        width: moderateScale(32, 0.1),
        height: moderateScale(32, 0.1),
        borderRadius: 16
    },
    greenActiveConatiner: {
        width: 12,
        height: 12,
        borderRadius: 6,
        backgroundColor: 'white',
        position: 'absolute',
        top: -1,
        right: -1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    greenActiveView: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#20E070',
    },
    textFour: {
        color: '#333333',
        fontFamily: 'OpenSans',
        fontWeight: '600',
        fontSize: moderateScale(23, 0.1)
    },
    textFive: {
        color: 'white',
        fontFamily: 'OpenSans',
        fontWeight: '400',
        fontSize: moderateScale(13, 0.1)
    }
})
