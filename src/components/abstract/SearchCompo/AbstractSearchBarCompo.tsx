import React from 'react'
import { StyleSheet, Text, View,TouchableOpacity, TextInput } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import SearchBarSvg from '../../../assets/icons/SearchHeaderSvgs/SearchBarSvg'

interface Props {
value?:string
setInput?:()=>void,
width?:number|string
}


const defPropsForSearchbar = {
    width:moderateScale(299,0.1)
}


const AbstractSearchBarCompo:React.FC<Props> = ({value,setInput,width}) => {
    return (
        <View style={[styles.mainContainer,{width:width}]}>
            <View style={styles.viewOne}>
                <TextInput 
                placeholder={'Type any keywords to search'}
                placeholderTextColor={'#324650'}
                onChangeText={setInput}
                value={value}
                style={{
                    padding:0,
                    paddingLeft:21,
                    fontFamily:'OpenSans',
                    fontWeight:'600',
                    fontSize:moderateScale(15,0.1)
                }}
                />

            </View>

            <TouchableOpacity
            activeOpacity={0.9}
             style={styles.viewTwo}>
              <SearchBarSvg />
            </TouchableOpacity>


        </View>
    )
}

AbstractSearchBarCompo.defaultProps = defPropsForSearchbar
export default AbstractSearchBarCompo


const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(36, 0.1),
        backgroundColor: '#E6E6E7',
        // backgroundColor:'red',
        borderRadius: 12,
        flexDirection: 'row',

    },
    viewOne: {
        width: '85%',
        // backgroundColor: 'red',
        height: '100%',
        justifyContent:'center',
    },
    viewTwo:{
        width: '15%',
        // backgroundColor: 'green',
        height: '100%',
        justifyContent:'center',
        alignItems:'center'
    }
})
