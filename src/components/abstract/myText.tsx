import React from 'react';
import {Text, TextStyle} from 'react-native';

interface Props {
  children?: string;
  style?: TextStyle | TextStyle[];
  onPress?: () => void;
}

const MyText: React.FC<Props> = ({children, style, onPress}) => {
  return (
    <Text onPress={onPress} style={[style, {fontFamily: 'Nunito-Medium'}]}>
      {children}
    </Text>
  );
};
export default MyText;
