import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {Colors} from '../../theme/colors';

interface Props {
  width?: number;
  height?: number;
  selected?: boolean;
  onPress?: () => void;
}

const radioDefaultProps = {
  width: 30,
  height: 30,
  selected: false,
};

const AbstractRadio: React.FC<Props> = ({width, height, selected, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.container, {width, height}]}>
      {selected ? <View style={[styles.innerContainer]} /> : false}
    </TouchableOpacity>
  );
};

AbstractRadio.defaultProps = radioDefaultProps;
export default AbstractRadio;

const styles = StyleSheet.create({
  container: {
    borderRadius: 50,
    backgroundColor: Colors.PRIMARY_TWO,
    justifyContent: 'center',
    alignItems: 'center',
  },
  innerContainer: {
    width: '70%',
    height: '70%',
    borderRadius: 50,
    backgroundColor: Colors.SECONDRY,
  },
});
