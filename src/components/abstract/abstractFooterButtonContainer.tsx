import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Colors} from '../../theme/colors';
interface Props {
  children: Element;
  isFocused?: Boolean;
  isFirstIndex: Boolean;
  isLastIndex: Boolean;
  onPress?: () => void;
}
const AbstractFooterButtonContainer: React.FC<Props> = ({
  children,
  isFocused,
  onPress,
  isFirstIndex,
  isLastIndex,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={{
        flex: 1,
        height: 60,
        borderTopLeftRadius: isFirstIndex ? 20 : 0,
        borderTopRightRadius: isLastIndex ? 20 : 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.PRIMARY,
      }}>
      <View
        style={{
          height: '100%',
          width: 25,
        }}>
        <View style={{height: 5}} />
        <View
          style={{
            height: 5,
            backgroundColor: isFocused ? Colors.SECONDRY : 'transparent',
            width: '100%',
            borderRadius: 10,
          }}
        />
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {React.Children.map(children, child => {
            if (React.isValidElement(child)) {
              return React.cloneElement(child, {
                color: isFocused ? Colors.SECONDRY : Colors.PRIMARY_THREE,
                size: 17,
              });
            }
          })}
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default AbstractFooterButtonContainer;
