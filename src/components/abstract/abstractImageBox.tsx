import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
  View,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import AddPictureSVG from '../../assets/icons/addPictureSVG';
import {Colors} from '../../theme/colors';

interface Props {
  source?: {uri: string};
  onChange?: (source: {uri: string}) => void;
  editable?: boolean;
  center?: boolean;
}

const imageDefaultProps = {
  editable: true,
};

const AbstractImageBox: React.FC<Props> = ({
  source,
  editable,
  onChange,
  center,
}) => {
  const [mSource, setSource] = useState<{uri: string | undefined}>();

  useEffect(() => {
    if (source) {
      setSource(source);
    }
  }, []);

  const handlePickImage = (): void => {
    ImagePicker.openPicker({
      cropping: true,
      mediaType: 'photo',
      multiple: false,
    })
      .then(image => {
        setSource({uri: image.path});
        if (onChange) onChange({uri: image.path});
      })
      .catch(err => {
        console.log(err);
      });
  };

  if (mSource) {
    return (
      <ImageBackground
        imageStyle={styles.imageContainerStyle}
        source={mSource}
        style={[
          styles.imageContainerStyle,
          {alignSelf: center ? 'center' : 'auto'},
        ]}>
        {editable ? (
          <View style={styles.imageContainerFillObject}>
            <TouchableOpacity
              onPress={handlePickImage}
              style={styles.iconBtnStyle}>
              <AddPictureSVG color={Colors.SECONDRY} size={25} />
            </TouchableOpacity>
          </View>
        ) : (
          false
        )}
      </ImageBackground>
    );
  } else {
    return (
      <TouchableOpacity
        onPress={handlePickImage}
        disabled={!editable}
        style={[styles.container, {alignSelf: center ? 'center' : 'auto'}]}>
        {editable ? <AddPictureSVG size={40} /> : false}
      </TouchableOpacity>
    );
  }
};

AbstractImageBox.defaultProps = imageDefaultProps;
export default AbstractImageBox;

const styles = StyleSheet.create({
  container: {
    width: 100,
    height: 100,
    backgroundColor: Colors.SECONDRY,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainerStyle: {
    width: 100,
    height: 100,
    backgroundColor: Colors.SECONDRY,
    borderRadius: 100,
  },
  imageContainerFillObject: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  iconBtnStyle: {
    width: 40,
    height: 40,
    backgroundColor: Colors.PRIMARY,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
