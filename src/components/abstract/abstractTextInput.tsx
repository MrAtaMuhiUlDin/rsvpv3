import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TextInput} from 'react-native';
import {Colors} from '../../theme/colors';
import {TextField} from 'rn-material-ui-textfield';
import { placeholder } from '@babel/types';

interface Props {
  width?:string|number;
  label: string;
  value?: string;
  onChangeText?: (text: string) => void;
  errorMessage?: string;
  secureTextEntry?: boolean;
  placeholderText?: string;
  Fonts?:string,
  height?:string|number,
  borderBColor?:string ,
  bottomHeight?:any,
  keyboardType?: 'email-address' | 'name-phone-pad' | 'number-pad' | 'default';
}

const AbstractTextInput: React.FC<Props> = ({
  width,
  label,
  value,
  onChangeText,
  errorMessage,
  keyboardType,
  secureTextEntry,
  Fonts,
  placeholderText,
  height,
  borderBColor,
  bottomHeight
}) => {
  const [mValue, setValue] = useState<string>('');

  useEffect(() => {
    setValue(value ? value : '');
  }, []);

 const defaultWidth = width ? width : '100%'
 const defaultPlaceholder = placeholderText ? placeholderText : undefined
 const defaultFontFamily = Fonts ? Fonts : 'Montserrat-Medium'
 const defHeight = height ? height : 70 
 const defbaseColor = borderBColor ? borderBColor : Colors.PRIMARY_THREE
const defbottomHeight = bottomHeight ? bottomHeight : 1


  const handleOnChangeText = (text: string): void => {
    setValue(text);
    if (onChangeText) onChangeText(text);
  };

  return (
    <View style={[styles.container,{width:defaultWidth,height:defHeight}]}>
      <TextField
        fontSize={13}
        placeholderTextColor={'#212224'}
        placeholder={defaultPlaceholder}
        value={mValue}
        style={[styles.inputField,{fontFamily:defaultFontFamily}]}
        onChangeText={handleOnChangeText}
        autoCorrect={false}
        tintColor={Colors.PRIMARY_THREE}
        baseColor={defbaseColor}
        disabledLineWidth={1}
        activeLineWidth={1}
        lineWidth={1}
        lineType="none"
        labelTextStyle={{fontFamily:defaultFontFamily}}
        enablesReturnKeyAutomatically={true}
        returnKeyType="next"
        label={label}
        keyboardType={keyboardType ? keyboardType : 'default'}
        secureTextEntry={secureTextEntry}
        labelOffset={{y1: -5}}
        error={errorMessage}
        
      />
      <View
        style={{
          width: '100%',
          height: defbottomHeight,
          backgroundColor: Colors.FONT_PRIMARY_ONE,
          marginTop: -9,
        }}
      />
    </View>
  );
};



export default AbstractTextInput;

const styles = StyleSheet.create({
  container: {
    // width: '100%',
    height: 70,
    justifyContent: 'flex-end',
  },
  inputField: {
    fontSize: 15,
    color: Colors.FONT_PRIMARY,
  },
  
});
