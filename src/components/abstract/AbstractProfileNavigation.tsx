import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { moderateScale } from 'react-native-size-matters'
import ArrowRight from '../../assets/icons/ProviderSVG/ArrowRightSVG'


interface Props {
    logout?: boolean,
    text?: string,
    onPress?: () => void
}

const AbstractProfileNavigation: React.FC<Props> = ({ logout, text, onPress }) => {


    const defText = text ? text : 'text here'

    return (
        <TouchableOpacity
            activeOpacity={0.9}
            onPress={onPress}
            style={styles.mainContainer}>
            <View style={styles.viewOne}>
                {logout ?
                    <Text style={styles.textOne}>Log out</Text>
                    :
                    <View style={styles.viewTwo}>
                        <Text style={styles.textTwo}>{defText}</Text>
                        <ArrowRight />
                    </View>
                }
            </View>
        </TouchableOpacity>
    )
}

export default AbstractProfileNavigation




const styles = StyleSheet.create({
    mainContainer: {
        height: moderateScale(60, 0.1),
        width: '100%',
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewOne: {
        width: '100%',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    viewTwo: {
        width: '100%',
        height: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textOne: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'rgba(239, 67, 57, 0.25)'
    },
    textTwo: {
        fontSize: moderateScale(13, 0.1),
        fontWeight: '700',
        fontFamily: 'OpenSans',
        color: 'black'
    },

})