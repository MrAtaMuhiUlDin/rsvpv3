import React from 'react';
import {Image, View} from 'react-native';
import AppIconSVG from '../../assets/icons/appIconSVG';
import LogoSvg from '../../assets/icons/ProviderSVG/LogoSvg';
import {Colors} from '../../theme/colors';
import MyText from './myText';
import FocusAwareStatusBar from './statusbarConfiguration';

const ScreenHeader: React.FC = props => {
  return (
    <View
      style={{
        width: '100%',
        height: 60,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.PRIMARY,
      }}>
      <FocusAwareStatusBar
        barStyle="dark-content"
        backgroundColor={Colors.PRIMARY}
      />
  
      <LogoSvg />
     
  
      {/* <MyText
        style={{
          color: Colors.SECONDRY,
          fontSize: 35,
          fontStyle: 'italic',
          letterSpacing: 3,
        }}>
        rsvp
      </MyText> */}
    </View>
  );
};
export default ScreenHeader;
