import React from 'react'
import {Text,View,TouchableOpacity,StyleSheet} from 'react-native'


interface Props {
    width?:string| number,
    height?:string| number,
    text?:string,
    isFocused?: Boolean,
    isFirstIndex: Boolean,
    isLastIndex: Boolean,
    onPress?: () => void
  
}


const btndefaultProps ={
    width:'50%',
    height:60,

}



const AbstractTopNavButton:React.FC<Props> = ({onPress,width,height,text,isFirstIndex,isLastIndex,isFocused}) => { 

    const deftext = text ? text : 'textHere'

     
    return (
      
        <TouchableOpacity 
         onPress={onPress}
        activeOpacity={0.8} style={{width:width,height:height,backgroundColor:'white',justifyContent:'center',alignItems:'center'}}>
          
            <Text 
            style={{ 
                fontWeight:'400',
                fontSize:14,
                color:isFocused ?'#FF5959' : '#BEBEBE',
                position:'absolute',
                }}>
                    {deftext}
                    </Text> 
            
            
            {isFocused ?
            <View style={{width:26,
                height:5,
                borderRadius:5,
                backgroundColor:'#FF5959',
                position:'absolute',
                bottom:10
            }} />
            :null }


       </TouchableOpacity>
        
   
    )
}

AbstractTopNavButton.defaultProps = btndefaultProps
export default AbstractTopNavButton



