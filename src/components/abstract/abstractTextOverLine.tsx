import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../../theme/colors';
import MyText from './myText';

interface Props {
  text?: string;
  width?: number | string;
}

const textOverLineDefaultProps = {
  width: '100%',
};

const AbstractTextOverLine: React.FC<Props> = ({width, text}) => {
  return (
    <View style={[styles.container, {width}]}>
      <View style={styles.lineStye} />
      <View style={[styles.overlayContainer]}>
        {text ? <MyText style={styles.textStyle}>{text}</MyText> : false}
      </View>
    </View>
  );
};
AbstractTextOverLine.defaultProps = textOverLineDefaultProps;
export default AbstractTextOverLine;
const styles = StyleSheet.create({
  container: {
    height: 20,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'center',
  },
  textStyle: {
    backgroundColor: Colors.PRIMARY,
    color: Colors.PRIMARY_ONE,
    paddingLeft: 10,
    paddingRight: 10,
  },
  lineStye: {
    width: '100%',
    height: 1.5,
    backgroundColor: Colors.PRIMARY_ONE,
  },
  overlayContainer: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
  },
});
