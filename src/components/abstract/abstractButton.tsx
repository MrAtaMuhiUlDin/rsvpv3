import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {Colors} from '../../theme/colors';
import MyText from './myText';

const getBtnStyle = (type: string | undefined) => {
  let stroke = '';
  let bgColor = '';
  let fontColor = '';

  switch (type) {
    case 'primary':
      stroke = Colors.SECONDRY_ONE;
      bgColor = Colors.SECONDRY_ONE;
      fontColor = Colors.PRIMARY;
      break;
    case 'secondry':
      stroke = Colors.SECONDRY_ONE;
      bgColor = Colors.PRIMARY;
      fontColor = Colors.SECONDRY;
      break;
    case 'transparent':
      stroke = Colors.PRIMARY;
      bgColor = 'transparent';
      fontColor = Colors.PRIMARY;
      break;
      case 'transparentTwo':
        stroke = '#FF5959';
        bgColor = 'transparent';
        fontColor = '#FF5959';
        break;
    default:
      stroke = Colors.SECONDRY_ONE;
      bgColor = Colors.SECONDRY_ONE;
      fontColor = Colors.PRIMARY;
      break;
  }
  return {stroke, bgColor, fontColor};
};

interface Props {
  title: string;
  width?: string | number;
  height?: string | number;
  btnStyle?: 'primary' | 'secondry' | 'transparent' | 'transparentTwo';
  center?: boolean;
  onPress?: () => void;
  textSize?:number
}

const buttonDefautProps = {
  width: '100%',
  height: 50,
};

const AbstractButton: React.FC<Props> = ({
  title,
  width,
  height,
  btnStyle,
  center,
  onPress,
  textSize
}) => {
  const {stroke, bgColor, fontColor} = getBtnStyle(btnStyle);
   
  const defTextSize = textSize ? textSize : 18

  return (
    <TouchableOpacity
    activeOpacity={0.8}
      onPress={onPress}
      style={[
        styles.container,
        {
          width,
          height,
          borderWidth: 1,
          borderColor: stroke,
          backgroundColor: bgColor,
          alignSelf: center ? 'center' : 'auto',
        },
      ]}>
      <MyText style={[styles.textStyle, {color: fontColor,fontSize:defTextSize}]}>{title}</MyText>
    </TouchableOpacity>
  );
};

AbstractButton.defaultProps = buttonDefautProps;
export default AbstractButton;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.SECONDRY_ONE,
    borderRadius: 11.11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontWeight: '600',
  },
});
