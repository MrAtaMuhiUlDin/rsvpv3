import React, {PureComponent} from 'react';
import {SafeAreaView, StyleProp, ViewProps, ViewStyle} from 'react-native';
import {Colors} from '../../theme/colors';

interface Props {
  upperStyle?: ViewStyle;
  bottomStyle?: ViewStyle;
  children: Element;
}

const defaultProps = {
  upperStyle: {backgroundColor: Colors.PRIMARY},
  bottomStyle: {backgroundColor: Colors.PRIMARY},
};

const ScreenContainer: React.FC<Props> = ({
  upperStyle,
  bottomStyle,
  children,
}) => {
  return (
    <>
      <SafeAreaView style={[{flex: 0}, upperStyle]} />
      <SafeAreaView style={{flex: 1, backgroundColor: Colors.PRIMARY}}>
        {children}
      </SafeAreaView>
      <SafeAreaView style={[{flex: 0}, bottomStyle]} />
    </>
  );
};
ScreenContainer.defaultProps = defaultProps;
export default ScreenContainer;
