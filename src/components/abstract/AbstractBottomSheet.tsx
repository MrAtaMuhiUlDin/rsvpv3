import React, { useEffect, useRef, useState, forwardRef } from 'react';
import {View, Text, ScrollView, Platform} from 'react-native'
import ActionSheet, {
    // addHasReachedTopListener,
    // removeHasReachedTopListener,

  } from 'react-native-actions-sheet';


interface Props  {
    isVisible?: Boolean, 
    onClose?: () => void,  
    onRequestClose?: () => void,
    containerBgColor?:any,
    title?:any,
    transculent?:boolean,
    MyHeader?:JSX.Element
}


const AbstractBottomSheet: React.FC<Props> =  (props) => {
    
    const actionSheetRef = useRef<any>();
  const scrollViewRef = useRef<any>();

    const [isVisible, setIsVisible] = useState<Boolean>(false);
    // const ref = useRef();
  
    const onHasReachedTop = (hasReachedTop:any) => {
    console.log(`OnReached Top: ${hasReachedTop}`)
    //   if (hasReachedTop){
    //       actionSheetRef.current?.setNativeProps({
    //           scrollEnabled: hasReachedTop,
    //         });
    //    }
    };
  
    // useEffect(() => {
    //   addHasReachedTopListener(onHasReachedTop);
    //   return () => {
    //     removeHasReachedTopListener(onHasReachedTop);
    //   };
    // }, []);

    useEffect(() => {
        
        if(isVisible){
            // addHasReachedTopListener(onHasReachedTop);
            actionSheetRef.current?.setModalVisible(true)
        }else{
            // addHasReachedTopListener(onHasReachedTop);
            actionSheetRef.current?.setModalVisible(false)
        }
    }, [isVisible])

    useEffect(() => {
        if(props.isVisible){
            setIsVisible(true)
        }else{
            closeModal()
        }
    }, [props.isVisible])

    const onClose = () => {
        // console.log(`On close`)
        setIsVisible(false)
        if(props.onClose){
            props.onClose()
        }
      };
    
      const onOpen = () => {
        // console.log(`On Sheet Open...`)
        if(isVisible){
            scrollViewRef.current?.setNativeProps({
                scrollEnabled: true,
            });
        }
      };

      const closeModal = () => {
        actionSheetRef.current?.setModalVisible();
        setTimeout(() => {
            if(props.onRequestClose){
                props.onRequestClose()
                setIsVisible(false)
            }
        }, 700)
      }

     

      const containerBgColor = props.containerBgColor? props.containerBgColor: 'white';
      const headerStyle:any = props.transculent ? {position:"absolute",top:0, left:0,zIndex:9999 ,width: "100%", height: 30, backgroundColor: "transparent", flexDirection:"row"}: {width: "100%", height: 30, backgroundColor: 'green', flexDirection:"row"}

     if(isVisible){
        return (    
            <ActionSheet
                ref={actionSheetRef}
                initialOffsetFromBottom={1}
                onOpen={onOpen}
                statusBarTranslucent={true}
                onPositionChanged={onHasReachedTop}
                bounceOnOpen={true}
                containerStyle={{backgroundColor:containerBgColor}}
                bounciness={2}
                gestureEnabled={true}
                onClose={onClose}
                CustomHeaderComponent={props.MyHeader}
                defaultOverlayOpacity={0.3}>
                <ScrollView 
                keyboardShouldPersistTaps="always"
                // actionSheetRef={actionSheetRef}
                ref={scrollViewRef}

                nestedScrollEnabled={true}
                onScrollEndDrag={() =>
                    actionSheetRef.current?.handleChildScrollEnd()
                }
                onScrollAnimationEnd={() =>
                    actionSheetRef.current?.handleChildScrollEnd()
                }
                onMomentumScrollEnd={() =>
                    actionSheetRef.current?.handleChildScrollEnd()
                }>
                    {/* <View style={headerStyle}> */}
                    <View>
                        {/* <View style={{width: 50, height: "100%"}}/> */}
                        {/* <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                            <Text style={{fontSize: 25, fontWeight:"bold"}}>{props.title? props.title: ""}</Text>
                        </View> */}
                        {/* <View style={{width: 50, height: "100%", justifyContent:"center", alignItems:"center"}}>
                            <RedCrossRoundedButton onPress={() => {
                                closeModal()
                            }}/>
                        </View> */}
                    </View>
                    
                    {props.children}
                   
                   {/* {
                       Platform.OS === "ios"?
                       <View style={{width:"100%", height: 50, backgroundColor:'white'}}></View>
                        :false
                   } */}
                </ScrollView>
            </ActionSheet>
        )
     }else{
         return( <>
         </>);
     }

    }
export default AbstractBottomSheet;